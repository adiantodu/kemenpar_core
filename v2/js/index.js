
var toastList = []
var newMarker = []
var layerActive = false;
var layerActiveData = false;
var layerType = 1
var geosjonsetting = {
    onEachFeature: onEachFeature,
    style: function (feature) {
        return {
            fillColor: '#102D4F',
            weight: 2,
            opacity: 0.5,
            color: '#1B3B62',
            fillOpacity: 0.7
        };
    }
}
var marker1 = L.icon({
    iconUrl: 'v2/asset/marker1.svg',
    iconSize: [38, 95],
});
var marker2 = L.icon({
    iconUrl: 'v2/asset/marker2.svg',
    iconSize: [38, 95],
});
var marker3 = L.icon({
    iconUrl: 'v2/asset/marker3.svg',
    iconSize: [38, 95],
});




var map = L.map('map-wrapper', {
    center: [-3.0238526699136, 117.09878620274043, 5.5],
    zoom: 5.5,
    zoomSnap: 0.25,
    dragging: true,
    doubleClickZoom: true,
    boxZoom: true,
    zoomControl: false
});
map.setMinZoom(4.74)
map.setMaxZoom(13)

var geojsonLayer;


function geojsongelombang() {
    geojsonLayer = [];
    for (let i = 1; i <= 21; i++) {
        geojsonLayer.push(new L.GeoJSON.AJAX(HTTP_APP_PATH + `/exec.php?action=get-gelombang&loc=${i}`, geosjonsetting))
        geojsonLayer[geojsonLayer.length - 1].addTo(map);
    }
}

function geojson1() {
    geojsonLayer = new L.GeoJSON.AJAX(HTTP_APP_PATH + "/exec.php?action=get-provinsi", geosjonsetting);
    geojsonLayer.addTo(map);

}
function geojson2() {
    geojsonLayer = new L.GeoJSON.AJAX("../json/geojson-kota.json", geosjonsetting);
    geojsonLayer.addTo(map);
}


function resetCenter() {
    map.setView(new L.LatLng(-3.0238526699136, 117.09878620274043), 5.5);
    closeCuaca()
}


function whenHover(e) {
    var layer = e.target;

    layer.setStyle({
        fillColor: '#D1AB60',
        weight: 3,
        color: '#EAC881',
        dashArray: '',
        opacity: 1,
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
    }

    // console.log(layer.feature.properties)
    // console.log(e)
}

function leaveHover(e) {
    var layer = e.target;

    if (layerType == 4 || layerType == 6) {
        if (layerActive != layer.feature.properties.ID_1) {
            geojsonLayer.resetStyle(layer);
        }
    } else if (layerType == 5) {
        $.each(geojsonLayer, (i, d) => {
            d.resetStyle(layer);
        })
    }
}

function whenClicked(e) {
    var layer = e.target;

   /*  console.log("LAYER",layer); */

    if (layerType == 4) {
        if (layerActive != layer.feature.properties.ID_1) {

            if (layerActiveData) {
                geojsonLayer.resetStyle(layerActiveData);
            }

            let prov = layer.feature.properties.NAME_1.toUpperCase();
            var params = {
                "prov": prov,
                "action": "get-cuaca-detail-v2"
            }
            $("#cuaca-content").hide();
            $("#cuaca-loading").show();

            $.getJSON(HTTP_APP_PATH + "exec.php", params, function (data) {

                $("#cuaca-loading").hide();
                $("#cuaca-content").fadeIn();

                var today = myDate(data.today[0].datetime);
                var today_1 = myDate(data.today_1[0].datetime);
                var today_2 = myDate(data.today_2[0].datetime);

                $("#today-img").attr("src", HTTP_APP_PATH + "v2/asset/weather/" + data.today[0].img_cuaca); 
                $("#today-desc").html(data.today[0].idn_cuaca);
                $("#today-day").html(`${today} <br/> <span class="t-12">${data.today[0].jam}:00</span>`);

                $("#today1-img").attr("src", HTTP_APP_PATH + "v2/asset/weather/" + data.today_1[0].img_cuaca);
                $("#today1-desc").html(data.today_1[0].idn_cuaca);
                $("#today1-day").html(`${today_1} <br/> <span class="t-12">${data.today_1[0].jam}:00</span>`);

                $("#today2-img").attr("src", HTTP_APP_PATH + "v2/asset/weather/" + data.today_2[0].img_cuaca);
                $("#today2-desc").html(data.today_2[0].idn_cuaca);
                $("#today2-day").html(`${today_2} <br/> <span class="t-12">${data.today_2[0].jam}:00</span>`);

            });
            

            $('#cuaca-location').text(layer.feature.properties.NAME_1)
            $('#cuaca-modal').show()

            map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng) + 0.21751819929076532));

            layerActive = layer.feature.properties.ID_1
            layerActiveData = layer

            layer.setStyle({
                fillColor: '#D1AB60',
                weight: 3,
                color: '#EAC881',
                dashArray: '',
                opacity: 1,
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }
        }
    } else if (layerType == 5) {
        if (layerActive != layer.feature.properties.WP_1) {

            if (layerActiveData) {
                $.each(geojsonLayer, (i, d) => {
                    d.resetStyle(layer);
                })
            }

            let kode = layer.feature.properties.WP_1 +"_"+layer.feature.properties.WP_IMM;

            var params = {
                "kode": kode,
                "action": "get-detail-gelombang-v2"
            }

            $("#result").hide();
            $("#loading").show();

            $.getJSON(HTTP_APP_PATH + "exec.php", params, function (data) {

                $("#loading").hide();
                $("#result").show();


                $("#wind-speed").html(data.data[0].wind_speed_min + " - " + data.data[0].wind_speed_max + " kts")
                $("#wave-desc").html(data.data[0].wave_cat);
                

            });


            $('#gelombang-location').text(layer.feature.properties.WP_IMM)
            $('#gelombang-modal').show()

            map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng) + 0.21751819929076532));

            layerActive = layer.feature.properties.WP_1
            layerActiveData = layer

            layer.setStyle({
                fillColor: '#D1AB60',
                weight: 3,
                color: '#EAC881',
                dashArray: '',
                opacity: 1,
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }
        }
    } else if (layerType == 6) {
        if (layerActive != layer.feature.properties.ID_1) {

            if (layerActiveData) {
                geojsonLayer.resetStyle(layerActiveData);
            }

            $('#covid-location').text(layer.feature.properties.NAME_1)
            $('#covid-modal').show()

            map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng) + 0.21751819929076532));

            layerActive = layer.feature.properties.ID_1
            layerActiveData = layer

            layer.setStyle({
                fillColor: '#D1AB60',
                weight: 3,
                color: '#EAC881',
                dashArray: '',
                opacity: 1,
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }
        }
    }

    /* console.log(layer) */
}

function myDate(tanggal) {
    var a = new Date(tanggal);
    var days = new Array(7);
    days[0] = "Minggu";
    days[1] = "Senin";
    days[2] = "Selasa";
    days[3] = "Rabu";
    days[4] = "Kamis";
    days[5] = "Jumat";
    days[6] = "Sabtu";
    var r = days[a.getDay()];
    return r;
}

function onEachFeature(feature, layer) {
    layer.on({
        click: whenClicked,
        mouseover: whenHover,
        mouseout: leaveHover
    });
}

function closeCuaca() {
    try {
        geojsonLayer.resetStyle(layerActiveData);
    } catch (e) {

    }
    layerActive = false
    layerActiveData = false
    L.Marker.stopAllBouncingMarkers();
    $('.cuaca-modal').hide()
}

// map.on('moveend', function() {

// });

function setFilter(marker_type, n = false ,t = false, callback)  {
    var params = {
        "marker_type": marker_type,
        "action": "get-marker-v2"
    }
    if(marker_type == 8 || marker_type == 9){
        var marker1 = L.icon({
            iconUrl: HTTP_APP_PATH + 'static/img/ico/'+marker_type+'.gif',
            iconSize: [35, 35],
        });
    }else{
        var marker1 = L.icon({
            iconUrl: HTTP_APP_PATH + 'static/img/ico/'+marker_type+'.png',
            iconSize: [35, 35],
        });
    }

    if (t) {
        try{
            map.removeLayer(newMarker[marker_type].marker);
            newMarker[marker_type] = null;
        }catch(e){
            console.log(e)
        }
    } else {
        var r = L.markerClusterGroup();
        $.getJSON(HTTP_APP_PATH + "/exec.php",params, function (data) {
            if(data.row < 1 ){
                if( n == false){
                    $("#map-wrapper").append("<span class='simple-notification animated pulse'>Data tidak ditemukan</span>");
                    $(".simple-notification").fadeOut(2000);
                    callback(true)
                }
            }else{
                if( marker_type == 8 || marker_type ==  9){
                }
                $.each(data.Markers, function (key, val) {
                    if(data.row == 1){
                        var m1 = new L.marker([val.Y, val.X], { icon: marker1, data: val })
                        m1.on('click', selectMarker);
                        map.addLayer(m1);
                        newMarker[marker_type] = {"marker":m1, "data":val}
                    }else{
                        var r2 = new L.marker([val.Y, val.X], { icon: marker1, data: val })
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight: 10,
                            bounceSpeed: 54,
                            exclusive: true,
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        newMarker[marker_type] = {"marker":r, "data":val}
                    }

                   
                });
           }
        });
    }
}

function defaultloadhome() {

      setFilter(8,true);
      setFilter(9, true);
      setFilter(17, true);
}



function selectMarker(e) {

    if(e.sourceTarget.options.data.type_data == "notif"){
        var notif = e.sourceTarget.options.data;
        $('#notif-modal').show();
        $("#title-notif").text(notif.comment_subject);
        $("#date-notif").text(notif.comment_date);
        $("#desc-notif").html(notif.comment_text);
        $("#link-notif").attr("href", "bencana-detil.php?id=" + notif.id_bencana);
    }else{
    var params = {
        "id": e.sourceTarget.options.data.I,
        "action": "get-marker-info-v2",
        "jns": e.sourceTarget.options.data.BB
    }
    $.getJSON(HTTP_APP_PATH + "/exec.php", params, function (data) {
            /* console.log(data); */

    $("#img-ekonomi").attr("src",'v2/asset/placeholder-img.svg')
    $("#img-wisata").attr("src",'v2/asset/placeholder-img.svg')
    $("#img-bencana").attr("src",'v2/asset/placeholder-img.svg')
    
    var layerModal =  e.sourceTarget.options.data.BB;

    if (layerModal == 1) {
        $('#wisata-modal').show();
        $("#title-wisata").text(data.Content.marker_title);
        $("#date-wisata").text(data.Content.date_add);
        $("#desc-wisata").html(data.Content.description);
        $("#link-wisata").attr("href", "objek-detil.php?id=" + data.Id);
        if (typeof (data.Content.description) != "undefined" && data.Content.description !== null) {
            $("#img-wisata").attr("src", data.Content.img_src); 
        }
       
        this.toggleBouncing();
    } else if (layerModal == 3) {
        $('#ekonomi-modal').show()
        $("#title-ekonomi").text(data.Content.marker_title);
        $("#date-ekonomi").text(data.Content.date_add);
        $("#desc-ekonomi").html(data.Content.description);
        $("#link-ekonomi").attr("href", "ekraf-detil.php?id=" + data.Id);
        if (typeof (data.Content.description) != "undefined" && data.Content.description !== null) {
            $("#img-ekonomi").attr("src", data.Content.img_src);
        }
        this.toggleBouncing();
    } else if (layerModal == 2) {
        $('#bencana-modal').show();
        $("#title-bencana").text(data.Content.marker_title);
        $("#date-bencana").text(data.Content.date_add);
        $("#desc-bencana").html(data.Content.description);
        $("#link-bencana").attr("href", "bencana-detil.php?id=" + data.Id);
        if (typeof (data.Content.description) != "undefined" && data.Content.description !== null) {
            $("#img-bencana").attr("src", data.Content.img_src);
        }
    }
    
    });

}
    // map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng) + 0.21751819929076532));
}

function zoom_map(id){

    $(event.currentTarget).removeClass('unread')
    var rr = JSON.parse(window.localStorage.getItem('notif_list'))
    rr.push(id)
    window.localStorage.setItem('notif_list',JSON.stringify(rr))
    var c = $('.notification-count').html()
    $('.notification-count').html( parseInt(c)-1 )
    if(c-1 == 0){
        $('.notification-count').css("display","none");
        bencanaNotif(true)
        $('.nav-menu-btn').removeClass('notif')
    }


    var params = {
        "id": id,
        "action": "get-detail-notif-v2"
    }

    $.getJSON(HTTP_APP_PATH + "/exec.php",params, function (data) {

        if(data.marker_type != null){ 
            if(data.marker_type == 8 || data.marker_type == 9){
                var marker1 = L.icon({
                    iconUrl: HTTP_APP_PATH + 'static/img/ico/'+data.marker_type+'.gif',
                    iconSize: [35, 35],
                });
            }else{
                var marker1 = L.icon({
                    iconUrl: HTTP_APP_PATH + 'static/img/ico/'+data.marker_type+'.png',
                    iconSize: [35, 35],
                });
            }
        }else{
            var marker1 = L.icon({
                iconUrl: 'v2/asset/marker3.svg',
                iconSize: [38, 95],
            });
        }
    
         var d = {
            'id': data.comment_id
        }

        data.type_data = "notif";

            toastList.push(d.id)
            if (layerType != 3) {
                /* bencanaNotif() */
            }
            /* $('.toast').append(toastTemplate(d)) */

            // var iii = Object.entries(newMarker).findIndex(x => x[1].data == data)
            // if(iii > -1){
                var r = new L.marker([data.lat, data.lng], { icon: marker1, data: data })
                r.on('click', selectMarker);
                map.addLayer(r);
                newMarker["notif-marker"] = { "marker": r, "data" : data }
                
                map.panTo(new L.LatLng(data.lat, data.lng));
            // }

            // setTimeout(() => {
            //     map.setZoom(8);
            // }, 50);

    });

}


function zoomIn() {
    map.zoomIn();
}
function zoomOut() {
    map.zoomOut();
}


function switchMap(t) {
    map.eachLayer(function (layer) {
        map.removeLayer(layer);
    });
    L.Marker.stopAllBouncingMarkers();
    resetCenter()

    layerType = t

    $('.legend-itm').removeClass('active')
    $(event.currentTarget).addClass('active')
    $('#floating-map').hide()

    switch (t) {
        case 1:
            $('#tab-legend-btn-2').show()
            $('#tab-legend-btn-1').removeClass('single')
            loadMap3()
            /* desaWisata() */
            break;
        case 2:
            $('#tab-legend-btn-2').show()
            $('#tab-legend-btn-1').removeClass('single')
            loadMap3()
            /* kerajinanTangan() */
            break;
        case 3:
            $('#tab-legend-btn-2').show()
            $('#tab-legend-btn-1').removeClass('single')
            loadMap3()
            /* bencana() */
            defaultloadhome()
            bencanaNotif(true)
            break;
        case 4:
            $('#tab-legend-btn-2').hide()
            $('#tab-legend-btn-1').addClass('single')
            geosjonsetting = {
                onEachFeature: onEachFeature,
                style: function (feature) {
                    return {
                        fillColor: '#102D4F',
                        weight: 2,
                        opacity: 0.5,
                        color: '#1B3B62',
                        fillOpacity: 0.7
                    };
                }
            }
            loadMap1()
            geojson1()
            $('#floating-map').show()
            $('#floating-map > .detail > #gelombang-map-detail').hide()
            $('#floating-map > .detail > #cuaca-map-detail').show()
            break;
        case 5:
            $('#tab-legend-btn-2').hide()
            $('#tab-legend-btn-1').addClass('single')
            geosjonsetting = {
                onEachFeature: onEachFeature,
                style: function (feature) {
                    return {
                        fillColor: '#102D4F',
                        weight: 2,
                        opacity: 0.5,
                        color: '#1B3B62',
                        fillOpacity: 0.7
                    };
                }
            }
            loadMap1()
            geojsongelombang()
            $('#floating-map').show()
            $('#floating-map > .detail > #gelombang-map-detail').show()
            $('#floating-map > .detail > #cuaca-map-detail').hide()
            break;
        // case 6:
        //     $('#tab-legend-btn-2').hide()
        //     $('#tab-legend-btn-1').addClass('single')
        //     geosjonsetting = {
        //         onEachFeature: onEachFeature,
        //         style: function (feature) {
        //             return {
        //                 fillColor: '#A92525',
        //                 weight: 2,
        //                 opacity: 0.5,
        //                 color: '#1B3B62',
        //                 fillOpacity: 0.7
        //             };
        //         }
        //     }
        //     loadMap4()
        //     geojson1()
        //     break;
        // case 7:
        //     $('#tab-legend-btn-2').hide()
        //     $('#tab-legend-btn-1').addClass('single')
        //     loadMap5()
        //     break;
    }
}

function loadMap1() {
    L.tileLayer(
        'https://api.mapbox.com/styles/v1/adiantodu/ckxyzklgrzvwq15luyhrbv72t/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYWRpYW50b2R1IiwiYSI6ImNreHl3dXEyYjR1MjYyd3BubnY4dTdtcWQifQ.Ls46BIK69rJM8gDBIfzv-Q', {
        tileSize: 512,
        zoomOffset: -1,
        attribution: '© <a href="https://www.mapbox.com/contribute/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
}

function loadMap2() {
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
}

function loadMap3() {
    L.tileLayer(
        'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: '&copy; <a href="http://www.esri.com/">Esri</a>'
    }).addTo(map);
    L.tileLayer(
        'https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}', {
        attribution: '&copy; <a href="http://www.esri.com/">Esri</a>'
    }).addTo(map);
}
function loadMap4() {
    L.tileLayer(
        'https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'
    }).addTo(map);
}
function loadMap5() {
    L.tileLayer(
        'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
    }).addTo(map);
}


function toggleMenu() {
    $('.nav-menu').toggleClass('active')
    $(".notification-container").removeClass('active')
}

function toggleNotif() {
    event.preventDefault();
    $(".notification-container").toggleClass('active')
}

function toastTemplate(d) {
    return `
            <div class="toast-content" id="toast-${d.id}">
                <div class="t-16 t-600">
                    GEMPA BUMI
                </div>
                <div class="t-12 line-clamp clamp-4">
                    Gempa Mag:3.8, 03-Jan-22 22:57:42 WIB, Lok:0.71 LS, 131.48 BT (Pusat gempa berada di laut 28 km timur laut Kota Sorong), Kedlmn:2 Km Dirasakan (MMI) III Sorong
                </div>
            </div>`
}

function bencanaNotif(t = false) {
    if (t) {
        $('.map-notif').hide()
    } else {
        $('.map-notif').show()
    }
}

function testToast() {
    var d = {
        'id': (Math.random() * 10).toFixed()
    }
    toastList.push(d.id)
    if (layerType != 3) {
        bencanaNotif()
    }
    $('.toast').append(toastTemplate(d))

    var r = new L.marker([0.000000, 116.303494], { icon: marker3, data: null })
    r.on('click', selectMarker);
    map.addLayer(r);
    newMarker["single-marker"] = { "marker": r }


    setTimeout(() => {
        $(`#toast-${d.id}`).remove()
    }, 2500)
}


function updateTime() {
    var dayList = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu']
    var monthList = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    var currentTime = new Date()
    var hours = currentTime.getHours()
    var minutes = currentTime.getMinutes()
    var day = currentTime.getDay()
    var date = currentTime.getDate()
    var month = currentTime.getMonth()
    var year = currentTime.getFullYear()
    if (minutes < 10) {
        minutes = "0" + minutes
    }
    var t_str = hours + " : " + minutes + " ";
    if (hours > 11) {
        t_str += "PM";
    } else {
        t_str += "AM";
    }
    $('.nav-date > span:first-child').text(t_str)
    $('.nav-date > span:last-child').text(`${dayList[day]}, ${date} ${monthList[month]} ${year}`)
}


function toggleFilter() {
    $('.filter-menu').toggle()
}


function legendTab(p) {

    $('.legend-content').removeClass('active')
    $('.tab-legend').removeClass('active')
    $('#tab-legend-btn-' + p).toggleClass('active')

    console.log(layerType)
    if (p == 1 && layerType < 4) {
        $('#tab-legend-' + p).addClass('active')
    } else {
        $('#tab-legend-' + (layerType + 1)).addClass('active')
    }

    if (p == 1) {
        $('.legend-tool').show()
    } else {
        $('.legend-tool').hide()
    }
}


function changeFilter(p) {
    var act = $(event.currentTarget).attr('class').search('active') > -1 ? true : false
    $(event.currentTarget).toggleClass('active')

    setFilter(p,false,act,(p)=>{
        if(p){
            $(event.currentTarget).removeClass('active')
        }
    });

 /*    switch (p) {
        case 1:
            if (layerType == 1) {
                desaWisata(act)
            } else if (layerType == 2) {
                kerajinanTangan(act)
            } else {
                bencana(act)
            }
            break;
        case 2:
            if (layerType == 1) {
                atraksiBuatan(act)
            } else if (layerType == 2) {
                fotografi(act)
            } else {
                banjir(act)
            }
            break;
        case 3:
            if (layerType == 1) {
                atraksiAlam(act)
            } else if (layerType == 2) {
                penerbitan(act)
            } else {
                kebakaran(act)
            }
            break;
    } */
}


function toggleDetailMap(){
    $('#floating-map > .detail').toggle()
}