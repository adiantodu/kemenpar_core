<style>
    .arrow:before{
        content:"";
        position:absolute;
        right : -20px;
        top : 10px;
        width: 0;
        height: 0;
        border-top: 10px solid transparent;
        border-left: 20px solid white;
        border-bottom: 10px solid transparent;
    }
</style>

<div class="section navbar">
        <div class="container flex-r v-center h-center">

            <div class="nav-date flex-c">
                <span class="t-18 t-600">
                </span>
                <span class="t-12">
                </span>
            </div>


            <img src="v2/asset/LOGO_KEMENPAREKRAF.png" alt="" style="
                height: 100%;
                object-fit: contain;
                margin-right: 10px;
                max-width: 50px;
            "/>
            <span class="t-700 t-20 l-120 t-center">
                Geospasial Pariwisata dan Ekonomi Kreatif<br>
                <span class=" t-16 t-400">Kementerian Pariwisata dan Ekonomi Kreatif</span>
            </span>

            <img src="static/img/visit.png" alt="" style="
                height: 100%;
                margin-left: 20px;">
            
            <div style="
                    height: 30px;
                    width: 30px;
                    position: absolute;
                    right: 30px;
                " class="cursor nav-menu-btn" onclick="toggleMenu()">
                <img src="v2/asset/icon-park_hamburger-button.svg" alt="" style="
                    width: 100%;
                    object-fit: contain;"/>
            </div>

            <div class="nav-menu flex-c h-left t-16">  
                <a href="<?php echo APP_URL ?>login.php" class="t-600">
                    <img src="" alt=""/>
                    Login
                </a>
                <a href="" class="t-600" onclick="toggleNotif()">
                    <img src="" alt=""/>
                    Notifikasi <div class="notification-count"></div>
                </a>
                <div class="divider"></div>
                <a href="<?php echo APP_URL ?>bencana-list.php">
                    <img src="" alt=""/>
                    Data Bencana
                </a>
                <a href="<?php echo APP_URL ?>ekatalog.php">
                    <img src="" alt=""/>
                    e-Katalog
                </a>
                <a href="<?php echo APP_URL ?>berita.php">
                    <img src="" alt=""/>
                    Informasi
                </a>
                <a href="<?php echo APP_URL ?>twitter.php">
                    <img src="" alt=""/>
                    Tweet
                </a>
                <a href="<?php echo APP_URL ?>data-stat.php">
                    <img src="" alt=""/>
                    Rekap Data
                </a>
                 <div class="notification-container arrow" id="notif-wrapper" >
                 </div>
            </div>
           
        </div>
    </div>
