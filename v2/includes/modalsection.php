<div class="cuaca-modal flex-c" id="cuaca-modal">
            
            <div class="flex-r v-center just-space cuaca-top-wrap">
                <div class="cuaca-location" id="cuaca-location">
                    Indonesia
                </div>
                <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
            </div>
            <div id="cuaca-loading" class="flex-c h-center">
                <div>    
               
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                </div> 
               
                <p>Loading...</p>
                </div>
            <div class="cuaca-wrap flex-r" id="cuaca-content" >
                <div class="cuaca-itm flex-c h-center">

                    <span id="today-day">
                        Senin
                    </span>
                    <img id="today-img" src="v2/asset/weather/clearday.svg" alt=""/>
                    <span id="today-desc" class="weather-desc">Cerah</span>
                </div>
                <div class="cuaca-itm flex-c  h-center t-center">
                   <span id="today1-day">
                        Senin
                    </span>
                    <img id="today1-img" src="v2/asset/weather/clearday.svg" alt=""/>
                    <span id="today1-desc" class="weather-desc" >Cerah</span>
                </div>
                <div class="cuaca-itm flex-c h-center t-center">
                    <span id="today2-day">
                        Senin
                    </span>
                    <img id="today2-img" src="v2/asset/weather/clearday.svg" alt=""/>
                    <span id="today2-desc" class="weather-desc">Cerah</span>
                </div>
            </div>

        </div>
        <div class="cuaca-modal flex-c" id="wisata-modal" style="
        max-width: 700px;
        width: 100%;">
            <div class="flex-r">
                <img src="v2/asset/placeholder-img.svg" id="img-wisata" style="
                    margin-right: 20px;
                    width: 25%;
                    height: 120px;
                    border-radius: 10px;
                    object-fit: cover;"/>

                <div class="flex-c" style="width:100%">
                    <div class="flex-r v-center just-space cuaca-top-wrap">
                        <div class="cuaca-location" id="title-wisata">
                            
                        </div>
                        <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
                    </div>
                    <div class="cuaca-wrap wrap-c" >
                        <div id="date-wisata">
                            
                        </div>
                        <div id="desc-wisata" >
                           
                        </div>
                    </div>
                    <a id="link-wisata" style="text-align:right"><button class="button-detail-info">DETAIL</button></a>
                </div>
            </div>
        </div>
        <div class="cuaca-modal flex-c" id="notif-modal" style="
        max-width: 700px;
        width: 100%;">
                <div class="flex-c" style="width:100%">
                    <div class="flex-r v-center just-space cuaca-top-wrap">
                        <div class="cuaca-location" id="title-notif">
                            
                        </div>
                        <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
                    </div>
                    <div class="cuaca-wrap wrap-c" style="display: flex;flex-direction: column;" >
                        <div id="desc-notif" >
                           
                        </div>
                        <div id="date-notif" style="font-size: 12px;color: #787878;">
                            
                        </div>
                        
                    </div>
                </div>
                 <a id="link-notif" style="text-align:right"><button class="button-detail-info">DETAIL</button></a>
        </div>
        <div class="cuaca-modal flex-c" id="bencana-modal">
            <div class="flex-r v-center just-space cuaca-top-wrap">
                <div class="cuaca-location" id="title-bencana">
                    Gempa Bumi
                </div>
                <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
            </div>
            <div class="cuaca-wrap wrap-c" style="width:100%">
                <div id="date-bencana">
                    17 January 2022
                </div>
                <div id="desc-bencana">
                    Gempa ini dirasakan untuk diteruskan pada masyarakat
                    Pusat gempa berada di laut 84 km BaratDaya Bayah
                </div>
            </div>
            <a id="link-bencana" style="text-align:right"><button class="button-detail-info">DETAIL</button></a>
        </div>
        <div class="cuaca-modal flex-c" id="ekonomi-modal" style="
            max-width: 700px;
            width: 100%;">
            <div class="flex-r">
                <img src="v2/asset/placeholder-img.svg" id="img-ekonomi" style="
                    margin-right: 20px;
                    width: 25%;
                    height: 120px;
                    border-radius: 10px;
                    object-fit: cover;"/>

                <div class="flex-c" style="width:100%">
                    <div class="flex-r v-center just-space cuaca-top-wrap">
                        <div class="cuaca-location" id="title-ekonomi">
                            Ekonomi Kreatif
                        </div>
                        <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
                    </div>
                    <div class="cuaca-wrap wrap-c">
                        <div id="date-ekonomi">
                            25 March 2021
                        </div>
                        <div id="desc-ekonomi">
                            Jl. Anggrek III No.2, RT.003/RW.005, Larangan Indah, Kec. Larangan, Kota Tangerang, Banten 15154
                        </div>
                    </div>
                     <a id="link-ekonomi" style="text-align:right"><button class="button-detail-info">DETAIL</button></a>
                </div>
            </div>

        </div>
        <div class="cuaca-modal flex-c" id="gelombang-modal">

            <div class="flex-r v-center just-space cuaca-top-wrap">
                <div class="cuaca-location" id="gelombang-location">
                    Laut ...
                </div>
                <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
            </div>
            <div class="cuaca-wrap" style="display:block !important">
                <div class="flex-r h-center" id="loading">
                        <div  >
                        <div>    
                        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                        </div> 
                        <p>Loading...</p>
                        </div>
                </div>
                
                <div id="result" class="flex-r" style="justify-content:center">
                    <div class="flex-c h-center">
                        <span>
                            Angin
                        </span>
                        <img src="v2/asset/angin-ico.svg"/>
                        <span class="t-500" id="wind-speed">
                            10-17 kts
                        </span>
                    </div>
                    <div class="flex-c h-center">
                        <span>
                            Gelombang
                        </span>
                        <img src="v2/asset/gelombang-ico.svg"/>
                        <span class="t-500" id="wave-desc">
                            Tinggi
                        </span>
                    </div>
                </div>
                
                <div>

                </div>
            </div>

        </div>
        <div class="cuaca-modal flex-c" id="covid-modal">

            <div class="flex-r v-center just-space cuaca-top-wrap">
                <div class="cuaca-location" id="covid-location">
                    Indonesia
                </div>
                <img src="v2/asset/close-ico.svg" class="close-cuaca cursor" onclick="closeCuaca()"/>
            </div>
            <div class="cuaca-wrap flex-c">
                <div class="flex-r v-center just-space">
                    <span>
                        Postif
                    </span>
                    <span class="t-600">
                        20
                    </span>
                </div>
                <div class="flex-r v-center just-space">
                    <span>
                        Sembuh
                    </span>
                    <span class="t-600">
                        2000
                    </span>
                </div>
                <div class="flex-r v-center just-space">
                    <span>
                        Meninggal Dunia
                    </span>
                    <span class="t-600">
                        2
                    </span>
                </div>
                <div class="flex-r v-center just-space">
                    <span>
                        Total Kasus
                    </span>
                    <span class="t-600">
                        2022
                    </span>
                </div>
            </div>

        </div>