<?php
include_once("includes/config.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/exec.php");
if(isset($_SESSION['role'])){
    $cur_role = $_SESSION['role'];
}
$check_settings = Helper::check_settings($default_map_settings, $map_settings);
if((!$map_settings && !is_array($map_settings)) || !$check_settings) {
    echo "You should specify <a href='" . HTTP_APP_PATH . "settings.php" . "'>Map settings</a>.";
    //exit;
}
$map_column = 9;
$num_columns = 7;
if (!$map_settings['config_show_makers_filter']) {
    $num_columns += 2;
}
if (!$map_settings['config_show_makers_search']) {
    $num_columns += 3;
    $map_column = 12;
}

?>

<div id="floating-map" onclick="toggleDetailMap()">
    <img src="v2/asset/info-ico.svg" alt="">
    <div class="detail">
        <div id="cuaca-map-detail">
            <h2>Peta Cuaca</h2>
            <p>
                Peta ini berisi informasi mengenai prakiraan cuaca pada suatu provinsi dengan jangka waktu 2 hari ke depan.
                <br/>
                Cara Kerja :
                <ul>
                    <li>
                        Pilih Menu cuaca untuk menampilkan peta Cuaca Indonesia
                    </li>
                    <li>
                        Pilih Provinsi yang akan dilihat cuacanya
                    </li>
                    <li>
                        Popup menampilkan detail cuaca pada provinsi tersebut
                    </li>
                </ul>
            </p>
        </div>
        <div id="gelombang-map-detail">
            <h2>Peta Gelombang</h2>
            <p>
                Peta ini berisi informasi yang menampilkan kecepatan angin dan tinggi gelombang disuatu wilayah.
                <br/>
                Cara Kerja :
                <ul>
                    <li>
                        Pilih menu gelombang pada menu utama
                    </li>
                    <li>
                        Pilih salah satu lautan di Indonesia
                    </li>
                    <li>
                        Popup menampilkan data gelombang dan angin pada wilayah lautan tersebut
                    </li>
                </ul>
            </p>
        </div>
        
    </div>
</div>

<div class="legend flex-r v-center h-center" style="height:auto">
            <div class="center-legend">
                <div class="tab-legend-wrap flex-r">
                    <div class="tab-legend cursor t-16 active" id="tab-legend-btn-1" onclick="legendTab(1)">
                        Tipe Peta
                    </div>
                    <div class="tab-legend cursor t-16" id="tab-legend-btn-2" onclick="legendTab(2)">
                        Filter
                    </div>
                </div>


                <div id="tab-legend-1" class="legend-content flex-r legend-map active">
                    <button onclick="switchMap(1)" class="flex-c h-center legend-itm v-center active">
                        <img src="v2/asset/map4.png" alt=""/>
                        <span>
                            Wisata
                        </span>
                    </button>
                    <button onclick="switchMap(2)" class="flex-c h-center legend-itm v-center">
                        <img src="v2/asset/map4.png" alt=""/>
                        <span>
                            Ekonomi Kreatif
                        </span>
                    </button>
                    <button onclick="switchMap(3)" class="flex-c h-center legend-itm v-center">
                        <div class="map-notif"></div>
                        <img src="v2/asset/map4.png" alt=""/>
                        <span>
                            Bencana
                        </span>
                    </button>
                    <button onclick="switchMap(4)" class="flex-c h-center legend-itm v-center">
                        <img src="v2/asset/map1.png" alt=""/>
                        <span>
                            Cuaca
                        </span>
                    </button>
                    <button onclick="switchMap(5)" class="flex-c h-center legend-itm v-center">
                        <img src="v2/asset/map2.png" alt=""/>
                        <span>
                            Gelombang
                        </span>
                    </button>
<!--                     <button onclick="switchMap(6)" class="flex-c h-center legend-itm v-center">
                        <img src="v2/asset/map3.png" alt=""/>
                        <span>
                            Covid
                        </span>
                    </button>
                    <button onclick="switchMap(7)" class="flex-c h-center legend-itm v-center">
                        <img src="v2/asset/map5.png" alt=""/>
                        <span>
                            Hazard
                        </span>
                    </button> -->
                </div>
                <div id="tab-legend-2" class="legend-content legend-filter">
                <?php foreach ($config_marker_types['pinImage2'] as $key => $marker_info){  ?>
                        <?php if( $marker_info['is_bencana'] == 0 && $marker_info['is_ekraf'] == 0  ) {?>
                        <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter('<?php echo $marker_info['marker_id'] ?>')" data-filter="1">
                            <div class="flex-r v-center h-center">
                                <img src="v2/asset/checklist-ico.svg"/>
                            </div> 
                        <span>
                            <?php echo $marker_info['type_name'] ?>
                        </span>
                    </div>
                        
                    <?php } ?>
                    <?php } ?>

<!--                     <div class="flex-r v-center legend-filter-item cursor active" onclick="changeFilter(1)" data-filter="1">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Desa Wisata
                        </span>
                    </div>
                    <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter(2)" data-filter="2">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Atraksi Buatan 
                        </span>
                    </div>
                    <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter(3)" data-filter="3">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Atraksi Alam
                        </span>
                    </div> -->
                </div>
                <div id="tab-legend-3" class="legend-content legend-filter">
                  <?php foreach ($config_marker_types['pinImage2'] as $key => $marker_info){  ?>
                        <?php if( $marker_info['is_bencana'] == 0 && $marker_info['is_ekraf'] == 1  ) {?>
                        <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter('<?php echo $marker_info['marker_id'] ?>')" data-filter="1">
                            <div class="flex-r v-center h-center">
                                <img src="v2/asset/checklist-ico.svg"/>
                            </div> 
                        <span>
                            <?php echo $marker_info['type_name'] ?>
                        </span>
                    </div>
                        
                    <?php } ?>
                    <?php } ?>
<!--                     <div class="flex-r v-center legend-filter-item cursor active" onclick="changeFilter(1)" data-filter="1">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Kerajinan Tangan
                        </span>
                    </div>
                    <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter(2)" data-filter="2">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Fotografi
                        </span>
                    </div>
                    <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter(3)" data-filter="3">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Penerbitan
                        </span>
                    </div> -->
                </div>
                <div id="tab-legend-4" class="legend-content legend-filter">
                    <?php foreach ($config_marker_types['pinImage2'] as $key => $marker_info){  ?>
                        <?php if( $marker_info['is_bencana'] == 1 && $marker_info['is_ekraf'] == 0 && $marker_info['marker_id'] != 8 && $marker_info['marker_id'] != 31 && $marker_info['marker_id'] != 39 && $marker_info['marker_id'] != 43 ) {?>
                        <div class="flex-r v-center legend-filter-item cursor <?php if($marker_info['marker_id'] == 8 || $marker_info['marker_id'] == 9 || $marker_info['marker_id'] == 17 ){ echo 'active';} ?> " onclick="changeFilter('<?php echo $marker_info['marker_id'] ?>')" data-filter="1">
                            <div class="flex-r v-center h-center">
                                <img src="v2/asset/checklist-ico.svg"/>
                            </div> 
                        <span>
                            <?php echo $marker_info['type_name'] ?>
                        </span>
                    </div>
                        
                    <?php } ?>
                    <?php } ?>
<!--                     <div class="flex-r v-center legend-filter-item cursor active" onclick="changeFilter(1)" data-filter="1">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Gempa
                        </span>
                    </div>
                    <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter(2)" data-filter="2">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Banjir
                        </span>
                    </div>
                    <div class="flex-r v-center legend-filter-item cursor" onclick="changeFilter(3)" data-filter="3">
                        <div class="flex-r v-center h-center">
                            <img src="v2/asset/checklist-ico.svg"/>
                        </div> 
                        <span>
                            Kebakaran
                        </span>
                    </div> -->
                </div>
            </div>
            <div class="legend-tool flex-c ">
                <button onclick="testToast()">
                    <img src="v2/asset/ep_zoom-in.svg" alt=""/>
                </button>
                <button onclick="zoomOut()">
                    <img src="v2/asset/ep_zoom-out.svg" alt=""/>
                </button>
                <button onclick="resetCenter()">
                    <img src="v2/asset/ep_refresh-right.svg" alt=""/>
                </button>
            </div>
        </div>