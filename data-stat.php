<?php
$sidebar = false;
$title = "Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
$bencana = new Bencana($dbh);
include_once(APP_PATH . "/includes/classes/Berita.class.php");
//waktu
$this_bulan = date('m');
$this_tahun = date('Y');
$str_title = '(Semua Data)';
if(isset($_POST['bulan'])){
    $this_bulan = (int) $_POST['bulan'];
}
if(isset($_POST['tahun'])){
    $this_tahun =  (int)$_POST['tahun'];
}
if(!empty($this_bulan)){
  $str_title = $_arr_bulan[sprintf("%02d", $this_bulan)] . ' ' ;
}
if(!empty($this_tahun )){
  if($str_title == '(Semua Data)'){
      $str_title = '';
    }
  $str_title .=  $this_tahun ;
}
$berita = new Berita($dbh);
$marker = new Marker($dbh);
$ekraf =  new Ekraf($dbh);
$marker_type = new Marker_type($dbh);
$jml_bencana = $bencana->get_jumlah('',$this_bulan,$this_tahun);
$jml_data_bnpb = $bencana->get_jumlah('BNPB',$this_bulan,$this_tahun);
$jml_data_bmkg = $bencana->get_jumlah('BMKG',$this_bulan,$this_tahun);
$jml_data_pvmbg = $bencana->get_jumlah('PVMBG',$this_bulan,$this_tahun);
$jml_berita = $berita->get_jumlah();
$jml_objek  =$marker->count_markers();
$jml_ekraf = $ekraf->count_markers();

//perjenis objek
$jns_objek = $marker_type->get(true);
$jns_objek_non_bencana = $marker_type->get('3');
$jns_objek_ekraf = $marker_type->get(0,TRUE);
// data dampak
$dt_dampak = $bencana->get_dampak($this_bulan,$this_tahun);
?>
<style type="text/css">

      body {
  font-family: "Open Sans", "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  font-size: 14px;
  line-height: 1.5em;
  font-weight: 400;
}

p, span, a, ul, li, button {
  font-family: inherit;
  font-size: inherit;
  font-weight: inherit;
  line-height: inherit;
}

strong {
  font-weight: 600;
}

h1, h2, h3, h4, h5, h6 {
  font-family: "Open Sans", "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  line-height: 1.5em;
  font-weight: 300;
}

strong {
  font-weight: 400;
}

.tile {
  width: 100%;
  display: inline-block;
  box-sizing: border-box;
  background: #fff;
  padding: 20px;
  margin-bottom: 30px;
  border-radius: 10px 10px 10px 50px;
}
.tile .title {
  margin-top: 0px;
}
.tile.purple, .tile.blue, .tile.red, .tile.orange, .tile.green, .tile.pink, .tile.yellow {
  color: #fff;
}
.tile.purple {
  background: #5133AB;
}
.tile.purple:hover {
  background: #3e2784;
}
.tile.red {
  background: #AC193D;
}
.tile.red:hover {
  background: #7f132d;
}
.tile.green {
  background: #00A600;
}
.tile.green:hover {
  background: #007300;
}
.tile.pink {
  background: #d6337c;
}
.tile.pink:hover {
  background: #f7046d;
}
.tile.blue {
  background: #2672EC;
}
.tile.blue:hover {
  background: #125acd;
}
.tile.orange {
  background: #DC572E;
}
.tile.orange:hover {
  background: #b8431f;
}

.tile.yellow {
  background: #fcba03;
}
.tile.yellow:hover {
  background: #e6990b;
}

.list-group-item{
  background: #0394fc;
}

.list-group-item-pink{
  background: #F7046D;
}

.list-group-item-yellow{
  background: #c2a502;
}
    
</style>
<div class="container" style="padding-top: 51px;">
  <div class="text-right">
 <form class="form-inline" action="" method="POST">
  <div class="form-group">
    <label class="sr-only" for="bulan">Bulan:</label>
<select name="bulan" class="form-control">
        <option>- Semua -</option>
    <?php foreach ($_arr_bulan as $key => $val){ ?>
    <option value="<?php echo $key;?>" <?php if($key==$this_bulan){ echo 'selected'; }?>><?php echo $val;?></option>
<?php } ?>
</select>
  </div>
  <div class="form-group">

    <label class="sr-only" for="tahun">Tahun:</label>
<select name="tahun" class="form-control">
      <option>- Semua -</option>
    <?php 
    $cur_tahun = date('Y');
    for ($i=$cur_tahun;$i>$cur_tahun-10;$i--){ ?>
    <option value="<?php echo $i;?>" <?php if($i==$this_tahun){ echo 'selected'; }?>><?php echo $i;?></option>
<?php } ?>
</select>
  </div>
    <button type="submit" class="btn btn-info">Tampilkan</button>
</form> 
</div>
  <div class="row">
    <div class="col-md-12">
      <h1><strong>Jumlah Data</strong></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 text-center"><a href="bencana-list.php?sumber_data=BMKG">
      <div class="tile purple">
        <h3 class="title"><?php echo number_format($jml_data_bmkg,0,'','.');?></h3>
        <p>BMKG</p>
      <em>  Badan Meteorologi, Klimatologi, dan Geofisika</em>
      </div></a>
    </div>
    <div class="col-sm-4 text-center" ><a href="bencana-list.php?sumber_data=BNPB">
      <div class="tile red">
        <h3 class="title"><?php echo number_format($jml_data_bnpb,0,'','.');?></h3>
        <p>BNPB</p>
         <em>Badan Nasional Penanggulangan Bencana</em>
      </div></a>
    </div>

    <div class="col-sm-4 text-center"><a href="bencana-list.php?sumber_data=PVMBG">
      <div class="tile orange">
        <h3 class="title"><?php echo number_format($jml_data_pvmbg,0,'','.');?></h3>
        <p>PVMBG</p>
       <em>Pusat Vulkanologi dan Mitigasi Bencana Geologi</em>
      </div></a>
    </div>
  </div>
  <div class="row">

    <div class="col-sm-4 text-center">
      <div class="tile blue">
        <h3 class="title"><?php echo number_format($jml_bencana,0,'','.');?> Bencana</h3>
        <ul class="list-group text-right">
          <?php foreach ($jns_objek as $key => $jenis) { 
            $jml_data = $bencana->get_jumlah('',$this_bulan,$this_tahun,$jenis['id']);
            $text_bencana =  array_search ($jenis['id'], $arr_bencana_bnpb);
            ?>
         <a href="bencana-list.php?search_text=&jns_bencana=<?php echo $text_bencana;?>" style="color:#fff"> <li class="list-group-item"><?php echo $jenis['type_name'];?>: <?php echo number_format($jml_data,0,'','.');?> </li></a>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="col-sm-8 text-center"  >
      <div class="tile green">
        <h3 class="title">Dampak - <?php echo $str_title;?></h3>
  <p>
  <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col" class="text-right">Korban</th>
      <th scope="col" class="text-right">Orang</th>
      <th scope="col" class="text-right">Wisman</th>
      <th scope="col" class="text-right">Wisnus</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row" >Meninggal</th>
      <td class="text-right"><?php echo number_format($dt_dampak['tot_meninggal'],0,'','.');?></td>
      <td class="text-right"><?php echo number_format($dt_dampak['wisman_meninggal'],0,'','.');?></td>
      <td class="text-right"><?php echo number_format($dt_dampak['wisnus_meninggal'],0,'','.');?></td>
    </tr>
    <tr>
      <th scope="row">Hilang</th>
      <td class="text-right"><?php echo number_format($dt_dampak['tot_hilang'],0,'','.');?></td>
      <td class="text-right"><?php echo number_format($dt_dampak['wisman_hilang'],0,'','.');?></td>
      <td class="text-right"><?php echo number_format($dt_dampak['wisnus_hilang'],0,'','.');?></td>
    </tr>
    <tr>
      <th scope="row">Luka</th>
      <td class="text-right"><?php echo number_format($dt_dampak['tot_hilang'],0,'','.');?></td>
      <td class="text-right"><?php echo number_format($dt_dampak['wisman_luka'],0,'','.');?></td>
      <td class="text-right"><?php echo number_format($dt_dampak['wisnus_luka'],0,'','.');?></td>
    </tr>
    <tr>
      <th scope="row">Menderita</th>
      <td class="text-right"><?php echo number_format($dt_dampak['tot_menderita'],0,'','.');?></td>
      <td class="text-right"> </td>
      <td class="text-right"> </td>
    </tr>

  </tbody>
   
</table></p>
 <p>
  <table class="table">
<thead class="thead-light">
    <tr>
      <th scope="col">Kerusakan</th>
      <th scope="col">Rumah</th>
      <th scope="col">Atraksi</th>
      <th scope="col">Aksesibilitas</th>
      <th scope="col">Amenitas</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Rusak Berat</th>
      <td><?php echo number_format($dt_dampak['tot_rumah_rb'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_atraksi_rb'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_akses_rb'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_amenitas_rb'],0,'','.');?></td>
    </tr>
    <tr>
      <th scope="row">Rusak Sedang</th>
      <td><?php echo number_format($dt_dampak['tot_rumah_rs'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_atraksi_rs'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_akses_rs'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_amenitas_rs'],0,'','.');?></td>
    </tr>
    <tr>
      <th scope="row">Rusak Ringan</th>
      <td><?php echo number_format($dt_dampak['tot_rumah_rr'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_atraksi_rr'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_akses_rr'],0,'','.');?></td>
      <td><?php echo number_format($dt_dampak['tot_amenitas_rr'],0,'','.');?></td>
    </tr>
  </tbody>
   
</table></p>
      </div>
    </div>    
  </div>
  <div class="row">

    <div class="col-sm-4 text-center">
      <div class="tile pink">
        <h3 class="title"><?php echo number_format($jml_objek,0,'','.');?> Wisata</h3>
        <ul class="list-group text-right">
          <?php foreach ($jns_objek_non_bencana as $key => $jenis) { 
            $jml_data = $marker->count_markers($jenis['id']);
            ?>
         <a href="manage.php?search_text=&jns_objek=<?php echo $jenis['id'];?>" style="color:#fff"> <li class="list-group-item list-group-item-pink"><?php echo $jenis['type_name'];?>: <?php echo number_format($jml_data,0,'','.');?> </li></a>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="col-sm-4 text-center">
      <div class="tile yellow">
        <h3 class="title"><?php echo number_format($jml_ekraf,0,'','.');?> Ekraf</h3>
        <ul class="list-group text-right">
          <?php foreach ($jns_objek_ekraf as $key => $jenis) { 
            $jml_data = $ekraf->count_markers($jenis['id']);
            ?>
         <a href="ekraf-list.php?search_text=&jns_objek=<?php echo $jenis['id'];?>" style="color:#fff"> <li class="list-group-item list-group-item-yellow"><?php echo $jenis['type_name'];?>: <?php echo number_format($jml_data,0,'','.');?> </li></a>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="col-sm-4 text-center"  ><a href="berita.php">
      <div class="tile green">
        <h3 class="title"><?php echo number_format($jml_berita,0,'','.');?> </h3>
        <p>Berita / RIP</p>
      </div></a>
    </div>    
  </div>
</div>