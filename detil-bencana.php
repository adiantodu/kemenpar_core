<?php

include_once("includes/config.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/exec.php");
include_once(APP_PATH . "/includes/classes/pagination/pagination.class.php");

if(isset($_POST['act'])){
    $act= $_POST['act'];
    if($act=='update-info'){
        //kirim gambar
        include_once(APP_PATH . "/includes/classes/Upload.class.php");
        $handle = new upload($_FILES['gambar_info']);
        $gambar_upload = '';
        if ($handle->uploaded) {
            $nama_baru = time();
          $handle->file_new_name_body   = $nama_baru;
          $handle->image_convert = 'jpg';
          $handle->image_resize         = true;
          $handle->image_x              = 300;
          $handle->image_ratio_y        = true;
          $handle->process(APP_PATH.'/static/img/upload/update_info/');
          if ($handle->processed) {
            $gambar_upload = $nama_baru.'.jpg';
            $handle->clean();
          } else {
            echo 'error : ' . $handle->error;
          }
        }

        $sql = "INSERT INTO bencana_informasi(id_bencana, waktu_kirim,user,judul,informasi,gambar) VALUES (:id_bencana,NOW(),:user,:title, :description,:gambar_upload) ";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id_bencana', $_SESSION['id_bencana']);
            $stmt->bindParam(':title', $_POST['judul']);
            $stmt->bindParam(':user', $_SESSION['username']);
            $stmt->bindParam(':description', $_POST['deskripsi']);
             $stmt->bindParam(':gambar_upload', $gambar_upload);
            $stmt->execute();

    } else if($act=='update_bencana'){
        $wisman_luka = (int)$_POST['wisman_luka'];
        $wisman_hilang = (int)$_POST['wisman_hilang'];
        $wisman_meninggal = $_POST['wisman_meninggal'];
        $wisnus_luka = $_POST['wisnus_luka'];
        $wisnus_hilang = (int)$_POST['wisnus_hilang'];
        $wisnus_meninggal = $_POST['wisnus_meninggal'];
        $atraksi_rr = $_POST['atraksi_rr'];
        $atraksi_rs = $_POST['atraksi_rs'];
        $atraksi_rb = $_POST['atraksi_rb'];
        $akses_rr = $_POST['akses_rr'];
        $akses_rs = $_POST['akses_rs'];
        $akses_rb = $_POST['akses_rb'];
        $amenitas_rr = $_POST['amenitas_rr'];
        $amenitas_rs = $_POST['amenitas_rs'];
        $amenitas_rb = $_POST['amenitas_rb']; 
        $deskripsi = $_POST['deskripsi_bencana']; 
        $aksi = $_POST['aksi_bencana']; 
        $catatan = $_POST['catatan_bencana'];     
        $id_bencana = $_GET['id'];
            $sql = "UPDATE bencana_bnpb SET wisman_luka = '$wisman_luka', wisman_hilang ='$wisman_hilang', wisman_meninggal ='$wisman_meninggal', wisnus_luka ='$wisnus_luka',wisnus_hilang='$wisnus_hilang',wisnus_meninggal='$wisnus_meninggal',atraksi_rr ='$atraksi_rr', atraksi_rs='$atraksi_rs', atraksi_rb='$atraksi_rb',akses_rr='$akses_rr',akses_rs='$akses_rs',akses_rb='$akses_rb',amenitas_rr='$amenitas_rr',amenitas_rs='$amenitas_rs',amenitas_rb='$amenitas_rb',deskripsi=:deskripsi,aksi=:aksi,catatan=:catatan WHERE id_bencana = '$id_bencana'";
           // echo $sql;
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':deskripsi', $deskripsi);
            $stmt->bindParam(':aksi', $aksi);
            $stmt->bindParam(':catatan', $catatan);
            $stmt->execute();

    }
}
$pagination = (new Pagination());

$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? (int) $_GET["page"] : 1;

$input_search_text = $search_text = isset($_GET['search_text']) ? trim($_GET['search_text']) : '';
$jns_objek = isset($_GET['jns_objek']) ? trim($_GET['jns_objek']) : '';
$arr_jenis = array();
if(!empty($jns_objek)){
$arr_jenis[0] = $jns_objek;
}

//record per Page($per_page)
$markers_per_page = isset($_GET['markers_per_page']) && in_array((int) $_GET['markers_per_page'], $config_markers_per_page) ? $_GET['markers_per_page'] : $config_markers_per_page[0];
$offset = $markers_per_page * ($current_page - 1);

//lokasi sekitar
$id_bencana = $_GET['id'];
if(is_numeric($id_bencana)){
    $_SESSION['id_bencana'] = $id_bencana;
} else {
    header("Location:index.php");
}
$bencana_info = $bencana->getById($id_bencana);
$lat=$bencana_info['lat'];
$lng = $bencana_info['lng'];
$markers_data = $marker->get_markers_around($lat,$lng,$markers_per_page, $offset, $search_text,$arr_jenis); 

//$history_bencana = $bencana->get_list_around($lat,$lng);
// data update informasi
$list_info = $bencana->getInformasiUpdate($id_bencana);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Data Detil Bencana</title>
        <meta charset="utf-8">
                <link rel="shortcut icon" href="static/img/favicon.png">
        <script lang="javascript">
            var SITE_DOMAIN = '<?php echo SITE_DOMAIN?>';
            var HTTP_APP_PATH = '<?php echo HTTP_APP_PATH?>';
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types) ?>;
            var ZOOM = <?php echo $map_settings['config_zoom'] ?>;
            var MAP_CENTER_LAT = <?php echo $map_settings['config_map_center_latitude'] ?>;
            var MAP_CENTER_LNG = <?php echo $map_settings['config_map_center_longitude'] ?>;
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types)?>;
            var MAP_SOURCE = <?php echo $map_settings['config_map_source_id']; ?>;
            <?php if($map_settings['config_map_source_id'] == 1): //mapbox ?>
            var MAPBOX_API_KEY = '<?php echo strlen($map_settings['config_mapbox_api_key'])?$map_settings['config_mapbox_api_key']:"";?>';
            <?php endif;?>
        </script>

        <?php include_once("blocks/scripts.php")?>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script>
                    $(document).ready(function() {
                        $("a.img_objek").fancybox();
                    });
        </script>
    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-static-top mb0" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo HTTP_APP_PATH ?>">
                    <?php echo $lang["site_title"]; ?>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php include_once("blocks/website_menu.php");?>
                        </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php include_once("blocks/user_menu.php");?>
                </ul>
            </div>

        </div>
    </nav>
<div class="container-fluid">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Tambah Informasi#<span id="marker_it_title"></span></h4>
                      </div>
                      <form class="form-horizontal" method="post" id="update-form" enctype="multipart/form-data" action="">
                          <div class="modal-body">

                              <div class="form-group">
                                  <label for="judul" class="col-sm-3 control-label">Judul *</label>
                                  <div class="col-sm-8">
                                      <input name="judul" required="" value="" type="text" class="form-control" id="judul">
                                  </div>
                              </div>
                              
                              <div class="form-group">
                                  <label for="deskripsi" class="col-sm-3 control-label">Keterangan</label>
                                  <div class="col-sm-8">
                                      <textarea name="deskripsi" id='deskripsi' class="form-control" rows="8"></textarea>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="gambar_info" class="col-sm-3 control-label">Gambar</label>
                                  <div class="col-sm-8">
                                      <span id="inputMarkerImage" style="float: left"></span>
                                      <input type="file" name="gambar_info">
                                  </div>
                              </div>
                              
                          </div>
                          <div class="modal-footer">
                              <input type='hidden' name='act' id='act' value='update-info'>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                              <button type="submit"  class="btn btn-primary">Simpan</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
            <div class="row">
                <div class="col-lg-9 col-md-9">
                        <h1 class="page-header"><?php echo $bencana_info['kejadian'];?></h1>
                        <div class="row">
                        </div><!-- /.row -->
            <div class="row">
              <div class="col-md-7">
                <div id="map-canvas" class=""></div>
              </div>
              <div class="col-md-5">
                  <div class="property-list">
                    <dl>
                    <?php 
                    $sumber = $bencana_info['sumber_data'];
                    if($sumber== 'BNPB'){ ?>                
                                    
                                      <dt>Provinsi</dt><dd><span class="label label-success"> <?php echo $bencana_info['nprop'];?> </span></dd>
                                    <dt>Kabupaten </dt><dd><span class="label label-success"> <?php echo $bencana_info['nkab'];?></span></dd>
                                      <dt>Meninggal </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['meninggal'],0,'','.');?> </span></dd>
                                    <dt>Hilang </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['hilang'],0,'','.');?> </span></dt>
                                    <dt>Terluka </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['terluka'],0,'','.');?> </span></dd>
                                    <dt>Menderita </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['menderita'],0,'','.');?> </span></dd>
                                    <dt>Rusak Ringan </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['rumah_rusak_ringan'],0,'','.');?> </span></dd>
                                    <dt>Rusak Sedang </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['rumah_rusak_sedang'],0,'','.');?> </span></dd>
                                    <dt>Rusak Berat </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['rumah_rusak_berat'],0,'','.');?> </span></dd>
                                    <dt>Keterangan</dt><dd><p class="text"><?php echo nl2br($bencana_info['keterangan']);?></p> </dd>
                    <?php } else if($sumber == 'BMKG')  { ?>
                                      <dt>Magnitude</dt><dd><span class="label label-success"> <?php echo $bencana_info['magnitude'];?> </span></dd>
                                    <dt>Kedalaman </dt><dd><span class="label label-success"> <?php echo $bencana_info['kedalaman'];?></span></dd>
                                    <dt>Lokasi </dt><dd><span class="label label-success"> <?php echo $bencana_info['lokasi'];?></span></dd>
                                    <dt>Keterangan</dt><dd><p class="text"><?php echo nl2br($bencana_info['keterangan']);?></p> </dd>

                    <?php }?>
                    </dl>
                  </div><!-- /.property-list -->
              </div>
            </div><!-- /.row -->

  <div>
    <h3 class="page-header">Data Kepariwisataan <button class="btn btn-success" id="btn_data" ><i class="glyphicon  glyphicon glyphicon-list"></i></button></h3>
  </div>
  <div id="div_data" >
    <form action="" method="post">
      <input type="hidden" id="act" name="act" value="update_bencana">
      <div class="form-row">
        <div class="form-group col-md-12">
          <label>Korban Terdampak</label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
        </div>
        <div class="form-group col-md-3">
          <label for="inputCity">Luka</label>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Hilang</label>
        </div>
        <div class="form-group col-md-3">
          <label>Meninggal</label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label>Wisman</label>
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="wisman_luka" name="wisman_luka" value="<?php echo $bencana_info['wisman_luka'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="wisman_hilang" name="wisman_hilang" value="<?php echo $bencana_info['wisman_hilang'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="wisman_meninggal" name="wisman_meninggal" value="<?php echo $bencana_info['wisman_meninggal'];?> ">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label >Wisnus</label>
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="wisnus_luka" name="wisnus_luka" value="<?php echo $bencana_info['wisnus_luka'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="wisnus_hilang" name="wisnus_hilang" value="<?php echo $bencana_info['wisnus_hilang'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="wisnus_meninggal" name="wisnus_meninggal" value="<?php echo $bencana_info['wisnus_meninggal'];?> ">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label>Infrastruktur Terdampak</label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          Kerusakan
        </div>
        <div class="form-group col-md-3">
          <label for="inputCity">Ringan</label>
        </div>
        <div class="form-group col-md-3">
          <label for="inputState">Sedang</label>
        </div>
        <div class="form-group col-md-3">
          <label>Berat</label>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label>Atraksi</label>
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="atraksi_rr" name="atraksi_rr" value="<?php echo $bencana_info['atraksi_rr'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="atraksi_rs" name="atraksi_rs" value="<?php echo $bencana_info['atraksi_rs'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="atraksi_rb" name="atraksi_rb" value="<?php echo $bencana_info['atraksi_rb'];?> ">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label >Aksesibilitas</label>
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="akses_rr" name="akses_rr" value="<?php echo $bencana_info['akses_rr'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="akses_rs" name="akses_rs" value="<?php echo $bencana_info['akses_rs'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="akses_rb" name="akses_rb" value="<?php echo $bencana_info['akses_rb'];?> ">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-3">
          <label >Amenitas</label>
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="amenitas_rr" name="amenitas_rr" value="<?php echo $bencana_info['amenitas_rr'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="amenitas_rs" name="amenitas_rs" value="<?php echo $bencana_info['amenitas_rs'];?> ">
        </div>
        <div class="form-group col-md-3">
          <input type="text" class="form-control" id="amenitas_rb" name="amenitas_rb" value="<?php echo $bencana_info['amenitas_rb'];?> ">
        </div>
      </div>
      <div class="form-group">
        <div class="form-group col-md-6">
          <label for="comment">Deskripsi:</label>
          <textarea class="form-control" rows="3" id="deskripsi_bencana" name="deskripsi_bencana"><?php echo $bencana_info['deskripsi'];?></textarea>
        </div>
        <div class="form-group col-md-6">
          <label for="comment">Aksi:</label>
          <textarea class="form-control" rows="3" id="aksi_bencana" name="aksi_bencana"><?php echo $bencana_info['aksi'];?></textarea>
        </div> 
      </div>
      <div class="form-group">
        <div class="form-group col-md-12">
          <label for="comment">Catatan/Rekomendasi:</label>
          <textarea class="form-control" rows="3" id="catatan_bencana" name="catatan_bencana"><?php echo $bencana_info['catatan'];?></textarea>
        </div>
      </div> 
      <input type="submit" class="btn btn-primary" value="Simpan Informasi"></button>
    </form>
  </div>

<h3 class="page-header">Update Informasi  <a class="btn btn-warning" id="add_information" href="#" style=""><i class="icon-star icon-white"></i> Tambah informasi <i class="load-indicator"></i></a>
    <button class="btn btn-success" id="btn_informasi" ><i class="glyphicon  glyphicon glyphicon-list"></i></button></h3>
<div id="update_informasi" style="max-height: 450px;overflow: auto;">
 <table class="table table-striped">
    <thead>
      <tr>
        <th>Judul</th>
        <th>Isi Berita</th>
        <th>Gambar</th>
        <th>Waktu</th>
        <th>Pengirim</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($list_info as $info){ 
$str_gambar = '';
$gambar = $info['gambar'];
if(!empty($gambar)){
    $str_gambar = '<img src="./static/img/upload/update_info/'.$gambar.'" width="300px" />';
}
            ?>
      <tr>
        <td><?php echo $info['judul']; ?> </td>
        <td><?php echo $info['informasi']; ?></td>
        <td><?php echo $str_gambar; ?></td>
        <td><?php echo $info['waktu_kirim']; ?></td>
        <td><?php echo $info['user']; ?></td>
      </tr>
    <?php }?>
    </tbody>
  </table>
</div>
 <br style="clear:both;">

                
</div><!-- /.content -->

<div class="col-lg-3" id="list-column">
                       <div class="panel panel-primary  ">
                            <div class="panel-heading">Objek Sekitar</div>
                            <div class="panel-body">
                                <!--<div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>manage.php">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="search" class="form-control" width="250" id="search" name="search_text" value="" placeholder="<?php echo $lang["Search_text"];?>">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>-->
                            </div>
                            <ul id="markers-list" class="list-group">

                            <?php if(isset($markers_data['rows'])) { foreach ($markers_data['rows'] as $data){?>
                            <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="nxIK.findMarker(-3.054977, 111.918358); return false;" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["title"] . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                        <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li></ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                    </div>


                </div><!-- /.sidebar -->
            </div><!-- /.row -->
        </div>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/js/update-marker-normal.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
             $("#add_information").click(function () {
                var editor = CKEDITOR.instances.deskripsi;
                if (editor) {
                    editor.destroy(true);
                }
                CKEDITOR.replace( 'deskripsi' );
                 $('#myModal').modal('show');
             }) ;  

             var mymap = L.map('map-canvas').setView([<?php echo $bencana_info['lat'];?>, <?php echo $bencana_info['lng'];?>], 13);
             var marker = L.marker([<?php echo $bencana_info['lat'];?>, <?php echo $bencana_info['lng'];?>]).addTo(mymap);
             var circle = L.circle([<?php echo $bencana_info['lat'];?>, <?php echo $bencana_info['lng'];?>], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 15,
                minZoom: 7,
                attribution: 'attribution'
            }).addTo(mymap);


            //hide show data wisata
 $("#div_data").hide();
  $("#btn_data").click(function(){
    $("#div_data").toggle();
  });

  //hide show informasi update
  update_informasi
  $("#update_informasi").hide();
  $("#btn_informasi").click(function(){
    $("#update_informasi").toggle();
  });
         });
        </script>
        <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>