<?php 
error_reporting(0);

$sidebar = false;
$title = "Log Aktivitas Pengguna - Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
$data_log =  $user_obj->get_log();
?>
<div class="container-fluid">
 <table class="table table-striped table-bordered" id="table">
                <thead>
                 <tr  class="info">
                    <td>Username</td>
                    <td>Aksi</td>
                    <td>Waktu</td>
                </tr>
            </thead>
            <tbody>
            <?php foreach($data_log as $log){ ?>
                <tr>
                    <td><?php echo $log['username'];?></td>
                    <td><?php echo $log['aksi'];?></td>
                    <td><?php echo $log['waktu'];?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
    <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
</body>
</html>