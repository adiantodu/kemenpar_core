/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 * 
 */

function saveData() {
    var options = {
        success: checkResponse  // post-submit callback 
    };
    $("#update-form").ajaxSubmit(options);
}
function checkResponse(responseText, statusText) {
    var resp = jQuery.parseJSON(responseText);

    if (resp.msg.code == 0) {
        alert(resp.msg.text);
    }
    else {
        alert("Something went wrong.");
    }
    window.location.reload();

}


$(document).ready(function () {
    $(".edit-button").click(function () {
        var marker_id = $(this).attr("data-id");
        var is_ekraf = $("#is-ekraf-" + marker_id).val();
        $('#inputIsEkraf').prop('checked', false).triggerHandler('click');
        if(is_ekraf == 1) {
            $('#inputIsEkraf').prop('checked', true).triggerHandler('click');
        }
        $("#inputMarkerTitle").val($("#marker-type-name-" + marker_id).text());
        $("#inputImageWidth").val($("#marker-image-width-" + marker_id).text());
        $("#inputImageHeght").val($("#marker-image-height-" + marker_id).text());
        $("#marker_id").val(marker_id);
        $("#marker_it_title").text(marker_id);
        $("#is_aktif").val($("#marker-aktif-" + marker_id).val());
        var image = $("#marker-image-" + marker_id).attr('src');
        $("#inputMarkerIcon").html('<img src="' + image + '">');
        $("#action").val('update-marker-type');
        $('#myModal').modal('show');
//        alert("You can't use this button in Demo mode.");
        return false;
    });

    $("#btnAdd").click(function() {
        var marker_id = 0;
        $('#inputIsEkraf').prop('checked', false).triggerHandler('click');
        $("#inputMarkerTitle").val('');
        $("#inputImageWidth").val('');
        $("#inputImageHeght").val('');
        $("#marker_id").val(marker_id);
        $("#marker_it_title").text('New');
        $("#inputMarkerIcon").html('');
        $("#action").val('add-marker-type');
        $('#myModal').modal('show');
//        alert("You can't use this button in Demo mode.");
        return false;
    });

});