

function saveData() {
    var options = {
        success: checkResponse  // post-submit callback 
    };
    CKupdate();
    $("#update-form").ajaxSubmit(options);
}
function checkResponse(responseText, statusText) {
    var resp = jQuery.parseJSON(responseText);

    if (resp.msg.code == 0) {
        $.get(HTTP_APP_PATH + "/exec.php?action=generate-cache&no_redirect=1");
        alert(resp.msg.text);
    }
    else {
        alert("Something went wrong.");
    }
    window.location.reload();
}
function deleteImage(id) {
        $.get(HTTP_APP_PATH + "/exec.php", { action: "delete-marker-image", id: id } );
        $("#inputMarkerImage").html('');
}
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

$(document).ready(function () {
    var map;
    var marker_id;
    $(".edit-button").click(function () {
        marker_id = $(this).attr("data-id");
        var params = {
                        "action": "get-marker",
                        "id": marker_id
        }
        $.getJSON(HTTP_APP_PATH +  "/exec.php",
            params,
            function (data) {
                $("#inputMarkerTitle").val(data.title);

                var marker_type = $("#marker-type-" + marker_id).attr("data-marker-type-" + marker_id);
                var dropdown_marker_type = "<select name='marker_type' id='marker_type'>";
                for (i in MAP_SETTINGS.pinImage) {
                    dropdown_marker_type += "<option " + ((i == data.marker_type) ? "selected" : "") + " value='" + i + "'>" + MAP_SETTINGS.pinImage[i].type_name + "</option>";
                }
                dropdown_marker_type += "</select>";
                var dropdown_status = "<select name='status' id='status'>";
                dropdown_status += "<option value=''>- Pilih Status -</option>";
                for (j in GET_STATUS) {
                    dropdown_status += "<option " + ((GET_STATUS[j].id_status == data.status) ? "selected" : "") + " value='" + GET_STATUS[j].id_status + "'>" + GET_STATUS[j].status + "</option>";
                }
                dropdown_status += "</select>";
                var dropdown_subkategori = "<select name='subkategori' id='subkategori'>";
                dropdown_subkategori += "<option value=''>- Pilih Subkategori -</option>";
                for (k in GET_SUBKATEGORI) {
                    dropdown_subkategori += "<option " + ((GET_SUBKATEGORI[k].id_subkategori == data.subkategori) ? "selected" : "") + " value='" + GET_SUBKATEGORI[k].id_subkategori + "'>" + GET_SUBKATEGORI[k].nama_subkategori + "</option>";
                }
                dropdown_subkategori += "</select>";
                $("#inputMarkerType").html(dropdown_marker_type);
                $("#inputMarkerStatus").html(dropdown_status);
                $("#inputMarkerSubkategori").html(dropdown_subkategori);
                $("#inputMarkerDescription").val(data.description);
                $("#inputMarkerLatitude").val(data.lat);
                $("#inputMarkerLongitude").val(data.lng);
                $("#inputMarkerProvinsi").val(data.provinsi);
                $("#inputMarkerKota").val(data.kota);
                $("#inputMarkerKecamatan").val(data.kecamatan);
                $("#inputMarkerKelurahan").val(data.kelurahan);
                $("#inputMarkerPengelola").val(data.pengelola);
                $("#marker_id").val(data.id);

                if(data.disabled == "1") {
                    $("#disable").prop('checked', true);
                }
                var editor = CKEDITOR.instances.inputMarkerDescription;
                if (editor) {
                    editor.destroy(true);
                }
                CKEDITOR.replace( 'inputMarkerDescription' );
                $('#myModal').modal('show');
            }
        );
        return false;
    });
    var map;
    var marker;
    //google.maps.event.addDomListener(window, 'load', initialize);
    //google.maps.event.addDomListener(window, "resize", resizingMap());
    function initialize() {
        //var mapProp = {
        //    zoom: 17,
        //    draggable: true,
        //    scrollwheel: false,
        //    mapTypeId:google.maps.MapTypeId.ROADMAP
        //};
        //map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);
        map = L.map('map-canvas');
        var revGeoUrl = HTTP_APP_PATH + 'revgeocode.php';
        if(MAP_SOURCE == 1) {//mapbox
            var osmUrl='https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + MAPBOX_API_KEY;
            var osmAttrib='Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
        }
        else {
            var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osmAttrib = '';
        }

        L.tileLayer(osmUrl, {
                //MAP_SETTINGS.max_zoomLevel,
                //MAP_SETTINGS.min_zoomLevel,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                osmAttrib,
                id: MAP_SETTINGS.map_type_id
        }).addTo(map);
        var marker_latlng = [$("#inputMarkerLatitude").val(), $("#inputMarkerLongitude").val()];
        map.setView(marker_latlng, 10);
        var marker = new L.marker(marker_latlng, {'draggable': true}).addTo(map);
        marker.on('moveend', function () {
            var latlng = marker.getLatLng();
            $("#inputMarkerLatitude").val(latlng.lat);
            $("#inputMarkerLongitude").val(latlng.lng);
            $.getJSON(revGeoUrl + "?lat="+ latlng.lat +"&lng="+ latlng.lng, function(data) {
                    $("#inputMarkerProvinsi").val(data.item.provinsi);
                    $("#inputMarkerKota").val(data.item.kota);
                    $("#inputMarkerKecamatan").val(data.item.kec);
                    $("#inputMarkerKelurahan").val(data.item.kel);
                });
        });
        $("#inputMarkerLongitude").on('keyup', function() {
            var lat = $("#inputMarkerLatitude").val();
            var lng = $(this).val();
            var latlng = [lat, lng];
            $.getJSON(revGeoUrl + "?lat="+ lat +"&lng="+ lng, function(data) {
                $("#inputMarkerProvinsi").val(data.item.provinsi);
                $("#inputMarkerKota").val(data.item.kota);
                $("#inputMarkerKecamatan").val(data.item.kec);
                $("#inputMarkerKelurahan").val(data.item.kel);
            });
            map.setView(latlng, 13);
            L.marker(latlng).addTo(map);
        });
    };
    //function resizeMap() {
    //    if(typeof map =="undefined") return;
    //    setTimeout( function(){resizingMap();} , 400);
    //}
    //function resizingMap() {
    //    if(typeof map =="undefined") return;
    //
    //    var center = new google.maps.LatLng($("#inputMarkerLatitude").val(), $("#inputMarkerLongitude").val());
    //    google.maps.event.trigger(map, "resize");
    //    map.setCenter(center);
    //    if(marker) {
    //        marker.setPosition(center);
    //    }
    //    else {
    //        marker = new google.maps.Marker({
    //            position:center,
    //            draggable: true,
    //            map: map
    //        });
    //    }
    //    google.maps.event.addListener(marker, "dragend", function() {
    //        var latlng = marker.getPosition();
    //        $("#inputMarkerLatitude").val(latlng.lat());
    //        $("#inputMarkerLongitude").val(latlng.lng());
    //    });
    //}
    $('#myModal').on('shown.bs.modal', function() {
        initialize();
        _.defer(map.invalidateSize.bind(map));
    });
    $('#myModal').on('hidden.bs.modal', function () {
        map.remove();
        delete map;
        delete marker;
    });
});