/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 */

var nxIK = {
    active_marker: undefined,
    markers: [],
    knnmarkers: [],
    map: undefined,
    infowindow: undefined,
    debugMarker: undefined,
    debuginfo: undefined,
    knn: false,
    knn_K: 5,
    debug: {
        showGridLines: false,
        showBoundaryMarker: false
    },
    async: {
        lastSendGetMarkers: 0,
        lastReceivedGetMarkers: 0,
        lastSendMarkerDetail: 0,
        lastReceivedMarkerDetail: 0,
        lastCache: ""
    },
    log: function (s) {
        if (console.log) {
            console.log(s)
        }
    },
    round: function (num, decimals) {
        return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals)
    },
    zoomIn: function () {
        var z = nxIK.map.getZoom();
        nxIK.map.setZoom(z + 1)
    },
    zoomOut: function () {
        var z = nxIK.map.getZoom();
        nxIK.map.setZoom(z - 1)
    },

    mymap: {
        initialize: function () {
            var marker_id = getUrlParameter('id');
            var map_center = [];
            if(marker_id) {
                ++nxIK.async.lastSendMarkerDetail;

                var getParams = "?action=get-marker-info&" + "id=" + marker_id + "&" + "sid=" + nxIK.async.lastSendMarkerDetail;
                $.ajax({
                    type: 'GET',
                    url: nxIK.mymap.settings.jsonGetMarkerInfoUrl + getParams,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        nxIK.findMarker(data.Lat, data.Lon);

                    },
                    error: function (xhr, err) {
                        var msg = "readyState: " + xhr.readyState + "\nstatus: " + xhr.status + "\nresponseText: " + xhr.responseText + "\nerr:" + err;
                        alert(nxIK.mymap.settings.textErrorMessage)
                    }
                })
            }

            map_center = [nxIK.mymap.settings.mapCenterLat, nxIK.mymap.settings.mapCenterLon];
            nxIK.map = L.map('nxIK_map',  {
                minZoom: nxIK.mymap.settings.min_zoomLevel,
                maxZoom: nxIK.mymap.settings.max_zoomLevel
            });
            nxIK.map.on('load moveend', function () {
                nxIK.mymap.events.getBounds(false);
            });

            nxIK.map.setView(map_center, nxIK.mymap.settings.zoomLevel);
             var urlOverlay = '';
            if(MAP_SOURCE == 1) {//mapbox
                var osmUrl='https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + MAPBOX_API_KEY;
                var osmAttrib='Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
            } else if(MAP_SOURCE == 2){ //esri World_Imagery
                var osmUrl='https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';
                var osmAttrib='&copy; <a href="http://www.esri.com/">Esri</a>';
                var urlOverlay = 'https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}';
            }
            else if(MAP_SOURCE == 3){ //esri  World_Physical_Map
                var osmUrl='https://server.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}';
                var osmAttrib='&copy; <a href="http://www.esri.com/">Esri</a>';
                var urlOverlay = 'http://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}';
            }
            else if(MAP_SOURCE == 4){ //Esri nat geo  MAP
                var osmUrl='https://services.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}';
                var osmAttrib='&copy; <a href="http://www.esri.com/">Esri</a>';
               // var urlOverlay = 'http://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}';
            }
            else if(MAP_SOURCE == 5){ // Gray Base
                var osmUrl='https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}';
                var osmAttrib='Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ';
                 //urlOverlay = 'http://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}';
            }
            else if(MAP_SOURCE == 6){ // OSM water color
                var osmUrl='https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg';
                var osmAttrib='Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributor';
                urlOverlay = 'http://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}';
            }
            else if(MAP_SOURCE == 7){ //CartoDB.DarkMatter
                var osmUrl='https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png';
                var osmAttrib='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>';
                //urlOverlay = 'http://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}';
            }
            else if(MAP_SOURCE == 8){ //CartoDB.DarkMatter
                var osmUrl='https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png';
                var osmAttrib='Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)';
                //urlOverlay = 'http://services.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}';
            }
            else {
                var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
                var osmAttrib = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>';
            }
            var popup = L.popup();

function onMapClick(e) {
    window.open('http://localhost/2019/kemenpar/leaflet-map-clustering/efek.php?lat='+e.latlng.lat + '&long='+ e.latlng.lng, '_blank');
}

//nxIK.map.on('click', onMapClick);
            L.tileLayer(osmUrl, {
                maxZoom: nxIK.mymap.settings.max_zoomLevel,
                minZoom: nxIK.mymap.settings.min_zoomLevel,
                attribution: osmAttrib,
                id: nxIK.mymap.settings.map_type_id
            }).addTo(nxIK.map);
            // tambah skala
            L.control.scale({imperial: false}).addTo(nxIK.map);
            if(urlOverlay!== ''){
            L.tileLayer(urlOverlay, {
                maxZoom: nxIK.mymap.settings.max_zoomLevel,
                minZoom: nxIK.mymap.settings.min_zoomLevel,
                attribution: osmAttrib,
                id: nxIK.mymap.settings.map_type_id
            }).addTo(nxIK.map);
        }

        },
        settings: MAP_SETTINGS,
        events: {
            getBounds: function (forceUpdate) {
                var bounds = nxIK.map.getBounds();
                var NE = bounds.getNorthEast();
                var SW = bounds.getSouthWest();
                var mapData = [];
                mapData.neLat = nxIK.round(NE.lat, 7);
                mapData.neLon = nxIK.round(NE.lng, 7);
                mapData.swLat = nxIK.round(SW.lat, 7);
                mapData.swLon = nxIK.round(SW.lng, 7);
                mapData.zoomLevel = nxIK.map.getZoom();
                if (nxIK.debug.showBoundaryMarker) {

                    if (nxIK.debugMarker === undefined) {
                        nxIK.debugMarker = L.marker([nxIK.mymap.settings.mapCenterLat, nxIK.mymap.settings.mapCenterLon]).addTo(nxIK.map);

                    }
                    if (nxIK.debuginfo === undefined) {
                        nxIK.debuginfo = nxIK.debugMarker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
                    }
                }
                var _ = "_";
                var cache = mapData.neLat + _ + mapData.neLon + _ + mapData.swLat + _ + mapData.swLon + _ + mapData.zoomLevel;
                if (nxIK.async.lastCache === cache && forceUpdate === false) return;
                nxIK.async.lastCache = cache;
                nxIK.mymap.events.loadMarkers(mapData)
            },
            polys: [],
            loadMarkers: function (mapData) {
                var pinImg = [];
                for (var i in nxIK.mymap.settings.pinImage) {
                    pinImg[i] = L.icon({
                        iconUrl: nxIK.mymap.settings.pinImage[i].src,
                        iconSize: [nxIK.mymap.settings.pinImage[i].width, nxIK.mymap.settings.pinImage[i].height]
                    });
                }
                ++nxIK.async.lastSendGetMarkers;
                var getParams = "?" + "action=get-markers&nelat=" + nxIK.dEscape(mapData.neLat) + "&" + "nelon=" + nxIK.dEscape(mapData.neLon) + "&" + "swlat=" + nxIK.dEscape(mapData.swLat) + "&" + "swlon=" + nxIK.dEscape(mapData.swLon) + "&" + "zoom=" + mapData.zoomLevel + nxIK.getFilterValues() + "&" + "sid=" + nxIK.async.lastSendGetMarkers;
                $.ajax({
                    type: 'GET',
                    url: nxIK.mymap.settings.jsonGetMarkerUrl + getParams,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        if (data.Ok === "0") {
                            nxIK.log(data.EMsg);
                            return
                        }
                        var lastReceivedGetMarkers = data.Rid;
                        if (lastReceivedGetMarkers <= nxIK.async.lastReceivedGetMarkers) {
                            nxIK.log('async mismatch ' + lastReceivedGetMarkers + ' ' + nxIK.async.lastReceivedGetMarkers);
                            return
                        }
                        nxIK.async.lastReceivedGetMarkers = lastReceivedGetMarkers;
                        var markersDrawTodo = nxIK.dynamicUpdateMarkers(data.Markers, nxIK.markers, nxIK.getKey, true);
                        $.each(markersDrawTodo, function () {
                            var item = this;
                            var lat = item.Y;
                            var lon = item.X;
                            var latlng = L.latLng(lat, lon);
                            var iconImg;
                            if (item.C === 1) {
                                if (pinImg[item.T]) {
                                    iconImg = pinImg[item.T];
                                } else {
                                    iconImg = pinImg.pop();
                                }
                            } else {
                                var count = item.C === undefined ? "error" : item.C;
                                i = " marker-cluster-";
                                if (count >= MARKERS_CLUSTERIZE_5) {
                                    i += "xlarge"
                                } else if (count >= MARKERS_CLUSTERIZE_4) {
                                    i += "large"
                                } else if (count >= MARKERS_CLUSTERIZE_3) {
                                    i += "xmedium"
                                } else if (count >= MARKERS_CLUSTERIZE_2) {
                                    i += "medium"
                                } else {
                                    i += "small"
                                }

                                iconImg = new L.divIcon({
                                    html: "<div><span>" + count + "</span></div>",
                                    className: "marker-cluster" + i,
                                    iconSize: new L.point(40, 40)
                                });
                            }
                            if (item.C === 1) {
                                var marker = new L.marker(latlng, {icon: iconImg}).addTo(nxIK.map);
                                var key = nxIK.getKey(item);
                                marker['key'] = key;
                                marker.on('click', function () {
                                    nxIK.mymap.events.rightSidebar(marker, item);
                                });
                                nxIK.markers.push(marker);
                            } else {
                                var text = item.C === undefined ? "error" : item.C;
                                var cluster_icon = iconImg;
                                var cluster_marker = L.marker([item.Y, item.X], {icon: cluster_icon}).addTo(nxIK.map);
                                cluster_marker.on('click', function () {
                                    var z = nxIK.map.getZoom();
                                    var n;
                                    if (z <= 8) {
                                        n = 3
                                    } else if (z <= 12) {
                                        n = 2
                                    } else {
                                        n = 1
                                    }
                                    nxIK.map.setView(
                                        [item.Y, item.X],
                                        z + n
                                    );
                                });
                                var key = nxIK.getKey(item);
                                cluster_marker['key'] = key;
                                nxIK.markers.push(cluster_marker);
                            }

                        });
                        markersDrawTodo.length = 0
                    },
                    error: function (xhr, err) {
                        var msg = "readyState: " + xhr.readyState + "\nstatus: " + xhr.status + "\nresponseText: " + xhr.responseText;
                        nxIK.log(msg);
                        alert(nxIK.mymap.settings.textErrorMessage)
                    }
                })
            },
            rightSidebar: function (marker, item) {
                console.log(item);
                nxIK.active_marker = marker;
                nxIK.active_marker.bounce();
                var getParams = "?action=get-marker-info&" + "id=" + item.I + '&jns=' + item.BB;
                $.ajax({
                    type: 'GET',
                    url: nxIK.mymap.settings.jsonGetMarkerInfoUrl + getParams,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        if (data.Ok === "0") {
                            nxIK.log(data.EMsg);
                            return
                        }
                        var soc_buttons = '<div style="width:100px;">' +
                            '<div style="float: left" class="fb-share-button" data-href="' + HTTP_APP_PATH + '?action=marker&id=' + item.I +
                            '" data-layout="button" data-size="large" data-mobile-iframe="false">' +
                            '<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' +
                            encodeURIComponent(HTTP_APP_PATH + '?action=marker&id=' + item.I) +
                            '&amp;src=sdkpreparse"><i class="fab fa-facebook fa-2"></i></a></div>' +
                            '<a target="_blank" href="https://twitter.com/share?url=' +
                            encodeURIComponent(HTTP_APP_PATH + '?action=marker&id=' + item.I) +
                            '&text=" class="twitter-share-button" data-show-count="false"><i class="fab fa-twitter fa-2"></i></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>';
                        $("#social-buttons").html(soc_buttons);

                        $("#marker_title").html(data.Content.marker_title);
                        $("#marker-description").html(data.Content.description);
                        $("#marker-date-add").html(data.Content.date_add);
                        $('#right-sidebar').modal('show');
                        if (data.Content.img_src !== undefined) {
                            $('#marker-image').html('<img width="280" class="card-img-top" id="theImg" src="' + data.Content.img_src + '" />')
                        }
                    },
                    error: function (xhr, err) {
                        var msg = "readyState: " + xhr.readyState + "\nstatus: " + xhr.status + "\nresponseText: " + xhr.responseText + "\nerr:" + err;
                        nxIK.log(msg);
                        alert(nxIK.mymap.settings.textErrorMessage)
                    }
                });
            }
        }
    },
    getKey: function (p) {
        var s = p.X + "__" + p.Y + "__" + p.C + "__" + p.T;
        return s.replace(/\./g, "_")
    },
    getKnnKey: function (p) {
        var s = p.X + "__" + p.Y + "__" + p.Dist;
        return s.replace(/\./g, "_")
    },
    dEscape: function (d) {
        var s = d + "";
        return s.replace(/\./g, "_")
    },
    getFilterValues: function () {
        var filter_str = "";
        $('.checkbox input:checked').each(function () {
            filter_str += "&filter[]=" + this.value;
        });
        return filter_str;
    },

    chackboxSelectAll: function (obj) {
        var checkboxes = $('.checkbox input');
        if (obj.checked) {
            checkboxes.prop('checked', 'checked');
        } else {
            checkboxes.removeAttr('checked');
        }
    },
    findMarker: function (lat, lng) {
        nxIK.map.setView([lat, lng], 12);
        //var markerBounds = L.latLngBounds([lat, lng]);
        //nxIK.map.fitBounds(markerBounds);
        return false;
    },
    dynamicUpdateMarkers: function (markers, cache, keyfunction, isclusterbased) {
        var markersCacheIncome = [];
        var markersCacheOnMap = [];
        var markersDrawTodo = [];
        for (i in markers) {
            if (markers.hasOwnProperty(i)) {
                p = markers[i];
                key = keyfunction(p);
                markersCacheIncome[key] = p
            }
        }
        for (i in cache) {
            if (cache.hasOwnProperty(i)) {
                var m = cache[i];
                key = m['key'];
                if (key !== 0) {
                    markersCacheOnMap[key] = 1
                }
                if (key === undefined) {
                    nxIK.log("error in code: key")
                }
            }
        }
        for (var i in markers) {
            if (markers.hasOwnProperty(i)) {
                var p = markers[i];
                key = keyfunction(p);
                if (markersCacheOnMap[key] === undefined) {
                    if (markersCacheIncome[key] === undefined) {
                        nxIK.log("error in code: key2")
                    }
                    var newmarker = markersCacheIncome[key];
                    markersDrawTodo.push(newmarker)
                }
            }
        }
        for (i in cache) {
            if (cache.hasOwnProperty(i)) {
                var m = cache[i];
                key = m['key'];
                if (key !== 0 && markersCacheIncome[key] === undefined) {
                    if (isclusterbased === true) {
                        $(".countinfo_" + key).remove()
                    }
                    cache[i]['key'] = 0;
                    cache[i].removeFrom(nxIK.map);
                }
            }
        }
        var temp = [];
        for (i in cache) {
            if (cache.hasOwnProperty(i)) {
                var key = cache[i]['key'];
                if (key !== 0) {
                    tempItem = cache[i];
                    temp.push(tempItem)
                }
            }
        }
        cache.length = 0;
        for (i in temp) {
            if (temp.hasOwnProperty(i)) {
                var tempItem = temp[i];
                cache.push(tempItem)
            }
        }
        markersCacheIncome.length = 0;
        markersCacheOnMap.length = 0;
        temp.length = 0;
        return markersDrawTodo
    }
};

$(document).ready(function () {
    nxIK.mymap.initialize();
    $('#right-sidebar').on('hidden.bs.modal', function (e) {
        nxIK.active_marker.stopBouncing();
        $("#social-buttons").html('');
        $("#marker_title").html('');
        $("#marker-description").html('');
        $("#marker-date-add").html('');
        $('#marker-image').html('');
    });
});
