/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 *
 */
 var revGeoUrl = HTTP_APP_PATH + 'revgeocode.php';
 var nxIK = {
     map: undefined,
     marker: undefined,
     mymap: {
         settings: MAP_SETTINGS,
         initialize: function () {
             nxIK.map = L.map('map_canvas');
             nxIK.map.on('click', function addMarker(event){
                 if (nxIK.marker) {
                     nxIK.marker.setLatLng(event.latlng);
                 }
                 else {
                     nxIK.marker = new L.marker(event.latlng, {'draggable': true}).addTo(nxIK.map);
                 }
                 $.getJSON(revGeoUrl + "?lat="+ event.latlng.lat +"&lng="+ event.latlng.lng, function(data) {
                     $("#provinsi").val(data.item.provinsi);
                     $("#kota").val(data.item.kota);
                     $("#kecamatan").val(data.item.kec);
                     $("#kelurahan").val(data.item.kel);
                 });
                 $("#lat").val(event.latlng.lat);
                 $("#lng").val(event.latlng.lng);
                 nxIK.marker.on('moveend', function () {
                     var latlng = nxIK.marker.getLatLng();
                     $("#lat").val(latlng.lat);
                     $("#lng").val(latlng.lng);
                 });
             });
             $("#lng").on('keyup', function() {
                 var lat = $("#lat").val();
                 var lng = $(this).val();
                 var latlng = [lat, lng];
                 $.getJSON(revGeoUrl + "?lat="+ lat +"&lng="+ lng, function(data) {
                     $("#provinsi").val(data.item.provinsi);
                     $("#kota").val(data.item.kota);
                     $("#kecamatan").val(data.item.kec);
                     $("#kelurahan").val(data.item.kel);
                 });
                 nxIK.map.setView(latlng, 13);
                 L.marker(latlng).addTo(nxIK.map);
             });
             if(MAP_SOURCE == 1) {//mapbox
                 var osmUrl='https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + MAPBOX_API_KEY;
                 var osmAttrib='Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
             }
             else {
                 var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
                 var osmAttrib = '';
             }
 
             L.tileLayer(osmUrl, {
                 maxZoom: nxIK.mymap.settings.max_zoomLevel,
                 minZoom: nxIK.mymap.settings.min_zoomLevel,
                 attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                 '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                 osmAttrib,
                 id: nxIK.mymap.settings.map_type_id
             }).addTo(nxIK.map);
 
         }
     }
 }
 
 function CKupdate(){
     for ( instance in CKEDITOR.instances )
         CKEDITOR.instances[instance].updateElement();
 }
 function validateForm(formData, jqForm, options) {
     var form = jqForm[0];
     var err_msg = "";
     if(nxIK.marker === undefined && (form.lat.value=='' && form.lng.value=='')) {
         err_msg += "Koordinat harus diisi.\n\n";
     }
     if (!form.title.value) {
         err_msg += "Nama Objek harus diisi.\n\n";
     }
     if (err_msg != '') {
         alert(err_msg);
         return false;
     }
     $("#upload-form").hide();
     $("#upload-message").show();
     return true;
 }
 
 function checkResponse(responseText, statusText) {
     var resp = jQuery.parseJSON(responseText);
 
     if (resp.msg.code == 0) {
         $.get(HTTP_APP_PATH + "/exec.php?action=generate-cache&no_redirect=1");
         alert(resp.msg.text);
     }
     else {
         alert("Something went wrong.");
     }
     window.location.replace(resp.msg.post_url);
 
 }
 
 function saveData() {
     var options = {
         beforeSubmit: validateForm, // pre-submit callback
         success: checkResponse  // post-submit callback
     };
     if(nxIK.marker !== undefined) {
         var latlng = nxIK.marker.getLatLng();
         $("#lat").val(latlng.lat);
         $("#lng").val(latlng.lng);
     }
     CKupdate();
     $("#add-form").ajaxSubmit(options);
 }
 
 function movemarker() {
     var latlng = [$("#lat").val(), $("#lng").val()];
     // nxIK.marker.setLatLng(latlng);
     // nxIK.map.setView(latlng);
     // nxIK.mymap.marker([latlng]).addTo(nxIK.mymap);
 }
 
 $('document').ready(function() {
     CKEDITOR.replace( 'description' );
     nxIK.mymap.initialize();
     nxIK.map.setView(
         [nxIK.mymap.settings.mapCenterLat, nxIK.mymap.settings.mapCenterLon],
         nxIK.mymap.settings.zoomLevel
     );
 });