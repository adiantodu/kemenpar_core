/**
 * Created by igor on 04.06.18
 *//**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 *
 */
var iframe_content;
function calc_iframe_code() {
    iframe_content = '<iframe frameborder="0" src="' +  HTTP_APP_PATH + '?iframe" width="' + $("#input_frame_width").val() + '" height="' + $("#input_frame_height").val() + '"><\/iframe>';
    $("textarea#iframe_insert_code").val(iframe_content);
}
$(function () {
    $('[data-toggle="popover"]').popover({"trigger": "focus"});
    $('[data-toggle="popover-info"]').popover({"trigger": "hover"});
})

function checkResponse(responseText, statusText) {
    var resp = jQuery.parseJSON(responseText);
    window.location.replace(resp.msg.post_url);

}
function saveSetting() {
    var options = {// pre-submit callback
        success: checkResponse  // post-submit callback
    };
    $("#form-setting").ajaxSubmit(options);
}

$(document).ready(function () {
    calc_iframe_code();

    $(".input_frame").change(function() {
        calc_iframe_code();
    });
    $(".input_frame").keyup(function() {
        calc_iframe_code();
    });

    if ($("#input_autodetect_map_center").is(":checked")) {
        $(".ipstackkey").show();
    }
    else {
        $(".ipstackkey").hide();
    }
    $("#input_autodetect_map_center").click(function () {
        $(".ipstackkey").toggle('slow');
    });

    $( "#input_mapsourceid" ).change(function() {
        if(this.value == 1) { //mapbox
            $("#mapbox_config").show('slow');
            $("#mapbox_api_key").attr("required", true)
        }
        else {
            $("#mapbox_config").hide('slow');
            $("#mapbox_api_key").attr("required", false);
        }
    });
    if($( "#input_mapsourceid option:selected" ).val() == 1) {
        $("#mapbox_api_key").attr("required", true)
        $("#mapbox_config").show();
    }

    if ($("#input_enable_cache").is(":checked")) {
        $(".l1_l2_cache").show();
    }
    else {
        $(".l1_l2_cache").hide();
    }
    $("#input_enable_cache").click(function () {
        $(".l1_l2_cache").toggle('slow');
    });
    $("#input_show_makers_search").click(function () {
        $("#togle_search_results_per_page").toggle('slow');
    });

    if($("#input_show_makers_search").is(":checked")) {
        $("#togle_search_results_per_page").show();
    }
    else {
        $("#togle_search_results_per_page").hide();
    }
    $(".toggle-navigation").click(function() {
        if($("#input_show_makers_filter").is(":checked") || $("#input_show_makers_search").is(":checked")) {
            $("#filter-placement").show('slow');
        }
        else {
            $("#filter-placement").hide('slow');
        }
    });
    if($("#input_show_makers_filter").is(":checked") || $("#input_show_makers_search").is(":checked")) {
        $("#filter-placement").show();
    }
    else {
        $("#filter-placement").hide();
    }
});
