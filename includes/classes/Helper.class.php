<?php
/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 *
 */

class Helper {

    private static $_mapbox_types = array(
        'mapbox.streets' => 'Streets',
        'mapbox.light' => 'Light',
        'mapbox.dark' => 'Dark',
        'mapbox.satellite' => 'Satellite',
        'mapbox.streets-satellite' => 'Streets Satellite',
        'mapbox.wheatpaste' => 'Wheatpaste',
        'mapbox.streets-basic' => 'Streets Basic',
        'mapbox.comic' => 'Comic',
        'mapbox.outdoors' => 'Outdoors',
        'mapbox.run-bike-hike' => 'Run Bike Hike',
        'mapbox.pencil' => 'Pencil',
        'mapbox.pirates' => 'Pirates',
        'mapbox.emerald' => 'Emerald',
        'mapbox.high-contrast' => 'High Contrast',
    );

    private static $_map_sources = array(
        0 => 'OpenStreet',
        1 => 'MapBox',
        2 => 'Esri World_Imagery',
        3 => 'Esri  World Physical Map',
        4 => 'Esri Nat Geo',
        5 => 'Esri Grayscale',
        6 => 'OSM Water Color',
        7 => 'CartoDB.DarkMatter',
        8 => 'OpenTopoMap'
    );



    public static function prepare_config_marker_types(&$config_marker_types, $map_settings) {
        $config_marker_types["mapCenterLat"] = (float)$map_settings['config_map_center_latitude'];
        $config_marker_types["mapCenterLon"] = (float)$map_settings['config_map_center_longitude'];
        $config_marker_types["zoomLevel"] = (int)$map_settings['config_zoom'];
        $config_marker_types["min_zoomLevel"] = (int)$map_settings['config_min_zoom'];
        $config_marker_types["max_zoomLevel"] = (int)$map_settings['config_max_zoom'];
        $config_marker_types["alwaysClusteringEnabledWhenZoomLevelLess"] = (int)$map_settings['config_alwaysClusteringEnabledWhenZoomLevelLess'];
        $config_marker_types["map_type_id"] = $map_settings['config_map_type_id'];
    }

    /**
     * compare settings keys with default
     *
     * @param array $default_map_settings
     * @param array $map_settings
     * @return boolean
     */

    public static function check_settings($default_map_settings, $map_settings) {
        foreach($default_map_settings as $key => $val) {
            if(!isset($map_settings[$key])) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param integer $min
     * @param integer $max
     * @return float
     */
    public static function random_float ($min, $max) {
        return ($min + lcg_value() * (abs($max - $min)));
    }

    /**
     * check if table exists
     *
     * @param $dbh
     * @param $table_name
     * @return bool
     */

    public static function tableExists($dbh, $table_name) {
        $results = $dbh->query("SHOW TABLES LIKE '$table_name'");
        if($results->rowCount() > 0){
            return true;
        }
        else {
            return false;
        }
    }

    public static function getMapBoxTypes() {
        return self::$_mapbox_types;
    }

    public static function getMapSources() {
        return self::$_map_sources;
    }

    public static function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return self::getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    public static function isDateValid($str) {

        if (!is_string($str)) {
            return false;
        }

        $stamp = strtotime($str);

        if (!is_numeric($stamp)) {
            return false;
        }

        if ( checkdate(date('m', $stamp), date('d', $stamp), date('Y', $stamp)) ) {
            return true;
        }
        return false;
    }

    public static function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
