<?php


class Bencana {
    private $_dbh;

    /**
     * properties
     */

    private $_marker_id;
    private $_title;
    private $_description;
    private $_lat;
    private $_lng;
    private $_marker_type;
    private $_image_type;
  //  private $this_tbl = 'bencana_bnpb';

    function __construct($dbh) {
        $this->_dbh = $dbh;
    }

    public function add() {
        $stmt = $this->_dbh->prepare("INSERT INTO " . DB_BENCANA_TABLE . " (title, description, lat, lng, marker_type, image_type) VALUES (:title, :description, :lat, :lng, :marker_type, '')");
//var_dump($this);
//        if (!$stmt) {
//            echo "\nPDO::errorInfo():\n";
//            print_r($this->_dbh->errorInfo());
//        }exit;
        $stmt->bindParam(':title', $this->_title);
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':lat', $this->_lat);
        $stmt->bindParam(':lng', $this->_lng);
        $stmt->bindParam(':marker_type', $this->_marker_type);
        $stmt->execute();
        $this->_marker_id = $this->_dbh->lastInsertId();
        if(!empty($this->_marker_id)){
             $query_notif = "INSERT INTO comments(comment_subject, comment_text,lat,lng) VALUES (:title, :description,:lat,:lng) ";
             $stmt = $this->_dbh->prepare($query_notif);
            $stmt->bindParam(':title', $this->_title);
            $stmt->bindParam(':description', $this->_description);
            $stmt->bindParam(':lat', $this->_lat);
            $stmt->bindParam(':lng', $this->_lng);
            $stmt->execute();
        }
        return $this->_marker_id;
    }

    public function update() {
        $stmt = $this->_dbh->prepare("UPDATE " . DB_BENCANA_TABLE . " SET title = :title, description = :description, "
            . "lat = :lat, lng = :lng, marker_type = :marker_type WHERE id = :marker_id");
        $stmt->bindParam(':title', $this->_title);
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':lat', $this->_lat);
        $stmt->bindParam(':lng', $this->_lng);
        $stmt->bindParam(':marker_type', $this->_marker_type);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        $stmt->execute();
    }

    public function get_jenis_bencana() {
        $sql = "SELECT distinct kejadian FROM " . DB_BENCANA_TABLE . " ORDER BY kejadian ASC";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_dampak($bulan='',$tahun='',$jenis=''){
        $where  = " WHERE 1=1 ";
        if(!empty($bulan)){
            $where .= " AND month(waktu) = '$bulan'";
        }
        if(!empty($tahun)){
            $where .= " AND YEAR(waktu) = '$tahun'";
        }
        if(!empty($jenis)){
            $where .= " AND marker_type  = '$jenis'";
        }
        $sql = "SELECT sum(meninggal) as tot_meninggal, sum(hilang) as tot_hilang, sum(terluka) as tot_terluka, sum(menderita) as tot_menderita, sum(rumah_rusak_berat) as tot_rumah_rb, sum(rumah_rusak_sedang) as tot_rumah_rs, sum(rumah_rusak_ringan) as tot_rumah_rr, sum(wisman_luka) as wisman_luka, sum(wisman_hilang) as wisman_hilang, sum(wisman_meninggal) as wisman_meninggal, sum(wisnus_luka) as wisnus_luka, sum(wisnus_hilang) as wisnus_hilang, sum(wisnus_meninggal) as wisnus_meninggal , sum(atraksi_rr) as tot_atraksi_rr , sum(atraksi_rs) as tot_atraksi_rs, sum(atraksi_rb) as tot_atraksi_rb , sum(akses_rr) as tot_akses_rr , sum(akses_rs) as tot_akses_rs, sum(akses_rb) as tot_akses_rb, sum(amenitas_rr) as tot_amenitas_rr , sum(amenitas_rs) as tot_amenitas_rs, sum(amenitas_rb) as tot_amenitas_rb FROM  " . DB_BENCANA_TABLE . " $where";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        return $sth->fetch();
    }

    public function updateImageType() {
        $stmt = $this->_dbh->prepare("UPDATE " . DB_BENCANA_TABLE . " SET image_type = :image_type WHERE id = :marker_id");
        $stmt->bindParam(':image_type', $this->_image_type);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        $stmt->execute();
    }
    public function updateDescription() {
        $stmt = $this->_dbh->prepare("UPDATE " . DB_BENCANA_TABLE . " SET description = :description WHERE id = :marker_id");
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        $stmt->execute();
    }

    public function getById($id) {
        $sql = "SELECT * FROM " . DB_BENCANA_TABLE . " WHERE id_bencana = :id_bencana";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute(array(':id_bencana' => $id));
        return $sth->fetch();
    }

    //update informasi bencana
    public function getInformasiUpdate($id){
        $sql = "SELECT * FROM " . DB_BENCANA_INFO_TABLE . " WHERE id_bencana = :id_bencana ORDER by waktu_kirim DESC";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute(array(':id_bencana' => $id));
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getByTitle($title) {
        $sql = "SELECT * FROM " . DB_BENCANA_TABLE . " WHERE title = :title LIMIT 1";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute(array(':title' => $title));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function deleteById() {
        $sql = "DELETE FROM " . DB_BENCANA_TABLE . " WHERE id = :markerid";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute(array(':markerid' => $this->_marker_id));
    }

    public function deleteAll() {
        $sql = "DELETE FROM " . DB_BENCANA_TABLE;
        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute();
    }

    public function get_jumlah($source='',$bulan='',$tahun='',$jenis=''){
        $jml_data = 0;
        $where  = " WHERE 1=1 ";
        if(!empty($source)){
            $where .= " AND sumber_data = '$source'";
        }
        if(!empty($bulan)){
            $where .= " AND month(waktu) = '$bulan'";
        }
        if(!empty($tahun)){
            $where .= " AND YEAR(waktu) = '$tahun'";
        }
        if(!empty($jenis)){
            $where .= " AND marker_type  = '$jenis'";
        }
        $sql = "SELECT count(*) as jumlah FROM " . DB_BENCANA_TABLE . $where;
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        $jml_data = $sth->fetchColumn();
        return $jml_data;
    }



    public function get_list($limit = 0, $offset = 0, $search_text = "",$jns_bencana="",$sumber_data='', $order_by = 'is_penting DESC, waktu DESC') {
        $sql_where = "";
        if (strlen($search_text) > 3) {
            $search_text = "%$search_text%";
            $sql_where .= " AND (keterangan LIKE :search_text OR nprop  LIKE :search_text)";
        }
        if (!empty($jns_bencana)) {
            $sql_where .= " AND kejadian =  :jns_bencana";
        }
         if (!empty($sumber_data)) {
            $sql_where .= " AND sumber_data =  :sumber_data";
        }
        if(strlen($sql_where)) {
            $sql_where = " WHERE 1" . $sql_where;
        }
        $sql = "SELECT count(*) FROM " . DB_BENCANA_TABLE . $sql_where;

        $sth = $this->_dbh->prepare($sql);
        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        if (!empty($jns_bencana)) {
            $sth->bindParam(":jns_bencana", $jns_bencana, PDO::PARAM_STR);
        }
        if (!empty($sumber_data)) {
            $sth->bindParam(":sumber_data", $sumber_data, PDO::PARAM_STR);
        }
        $sth->execute();
        $data['count_num_rows'] = $sth->fetchColumn();

        $sql = "SELECT * FROM " . DB_BENCANA_TABLE . $sql_where . " ORDER BY " . $order_by . " LIMIT :offset_markers, :markers_per_page";
        $sth = $this->_dbh->prepare($sql);
        if($limit == 0) {
            $limit = $data['count_num_rows'];
        }
        $offset = intval($offset);
        $limit = intval($limit);
        $sth->bindParam(":offset_markers", $offset, PDO::PARAM_INT);
        $sth->bindParam(":markers_per_page", $limit, PDO::PARAM_INT);

        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        if (!empty($jns_bencana)) {
            $sth->bindParam(":jns_bencana", $jns_bencana, PDO::PARAM_STR);
        }
        if (!empty($sumber_data)) {
            $sth->bindParam(":sumber_data", $sumber_data, PDO::PARAM_STR);
        }
        $sth->execute();

        $data['rows'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function get_list_around($lat,$lng,$limit = 0, $offset = 0, $search_text = "", $marker_type = array(), $order_by = 'id DESC') {
        $sql_where = "";
        if (is_array($marker_type) && sizeof($marker_type)) {
            $sql_where .= " WHERE marker_type IN (" . implode(",", $marker_type) . ")";
        }

        $sql ="SELECT id_bencana, ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) * 1.61 AS distance FROM bencana_bnpb $sql_where HAVING  distance < 10 ORDER BY distance";
        $sth = $this->_dbh->prepare($sql);
        if($limit == 0) {
            //$limit = $data['count_num_rows'];
        }
        $offset = intval($offset);
        $limit = intval($limit);
        $sth->bindParam(":offset_markers", $offset, PDO::PARAM_INT);
        $sth->bindParam(":markers_per_page", $limit, PDO::PARAM_INT);

        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        $sth->execute();

        $datas_id = $sth->fetchAll(PDO::FETCH_ASSOC);
        $j=0;
        foreach($datas_id as $data_objek){
            $id = $data_objek['id_bencana'];
            $sql_data = "SELECT * from bencana_bnpb WHERE id_bencana='$id'";
            $sth = $this->_dbh->prepare( $sql_data); 
            $sth->execute(); 
            $row = $sth->fetch();
            $row['distance'] = $data_objek['distance'];
            $data['rows'][$j] = $row;
            $j++;
        }
        $data['count_num_rows'] = $j;
        return $data;
    }
    /**
     *
     * get markers by lon and lat
     *
     * @param float $swlat
     * @param float $nelat
     * @param float $swlon
     * @param float $nelon
     * @return array
     */

    public function get_map_markers($swlat, $nelat, $swlon, $nelon, array $filter) {
        $sql = "SELECT id, lat, lng, marker_type
                                FROM " . DB_BENCANA_TABLE . "
                                WHERE lat BETWEEN :LAT1 AND :LAT2";
                                    //AND (lon BETWEEN LON1 AND -180 OR lon BETWEEN LON2 AND 180)";
                                //where (lat between :swlat AND :nelat) and (lng between :swlon and :nelon)";
        if($nelon < 0 && $swlon > 0) {
            $sql .= " AND (lng BETWEEN -180 AND :LON1 OR lng BETWEEN :LON2 AND 180)";
        }
        else {
            $sql .= " AND lng BETWEEN :LON1 AND :LON2";
        }
        if(sizeof($filter)) {
            $sql .= " AND marker_type IN (" . implode(", ", $filter) . ")";
        }

        $LAT1 = min($nelat, $swlat);
        $LAT2 = max($nelat, $swlat);
        $LON1 = min($nelon, $swlon);
        $LON2 = max($nelon, $swlon);

        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':LAT1', $LAT1);
        $stmt->bindParam(':LAT2', $LAT2);
        $stmt->bindParam(':LON1', $LON1);
        $stmt->bindParam(':LON2', $LON2);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     *
     * count all markes in the system
     *
     * @global $dbh
     * @return integer
     *
     */

    public function count_markers() {
        $sql = "SELECT count(*) FROM " . DB_BENCANA_TABLE;
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        return $sth->fetchColumn();
    }

    public function setId($marker_id) {

        if (!is_int($marker_id)) {
            throw new \InvalidArgumentException('The Marker ID has wrong format');
        }
        $this->_marker_id = $marker_id;
    }

    public function setTitle($title) {
        if (strlen($title) == 0) {
            throw new \InvalidArgumentException('The title is too short');
        }
        $this->_title = $title;
    }

    public function setImageType($image_type) {
        $this->_image_type = $image_type;
    }

    public function setDescription($description) {
        $this->_description = $description;
    }

    public function setLat($lat) {
        if (!is_float($lat)) {
            throw new \InvalidArgumentException('The latitude has wrong format');
        }
        $this->_lat = $lat;
    }

    public function setLng($lng) {
        if (!is_float($lng)) {
            throw new \InvalidArgumentException('The longitude has wrong format');
        }
        $this->_lng = $lng;
    }

    public function setMarkerType($marker_type) {
        if (!is_int($marker_type)) {
            throw new \InvalidArgumentException('The Marker Type has wrong format');
        }
        $this->_marker_type = $marker_type;
    }

}