<?php
/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 *
 */

class Cuaca {
    private $_dbh;

    /**
     * properties
     */

    private $_marker_id;
    private $_title;
    private $_description;
    private $_lat;
    private $_lng;
    private $_marker_type;
    private $_image_type;
    private $_prov;
    private $_city;
    private $_kec;
    private $_kel;
    private $_status;
    private $_pengelola;
    private $_sumberdata;
    private $_subkategori;
    private $_created_by;
    private $_updated_by;

    function __construct($dbh) {
        $this->_dbh = $dbh;
    }

    public function get_detail_cuaca_v2($prov){

        // $now = date("Y-m-d H:i:s");
        $now = date("Y-m-d H:i:s", strtotime("+ 6 hours"));
        $now1 = date("Y-m-d H:i:s", strtotime("+ 12 hours"));
        $now2 = date("Y-m-d H:i:s", strtotime("+ 18 hours"));
               
        if($prov != null){
        $sql = "SELECT 
                 *, HOUR(datetime) as jam FROM cuaca 
                join kode_cuaca where kode_cuaca.kode_cuaca=kode
                and provinsi = '".$prov."'
                and datetime < '".$now."'
                ORDER BY datetime desc
                limit 1";

        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute();

         $sql1 = "SELECT 
                 *, HOUR(datetime) as jam FROM cuaca 
                join kode_cuaca where kode_cuaca.kode_cuaca=kode
                and provinsi = '".$prov."'
                and datetime < '".$now1."'
                and datetime > '".$now."'
                ORDER BY datetime desc
                limit 1";

        $stmt1 = $this->_dbh->prepare($sql1);
        $stmt1->execute();

         $sql2 = "SELECT 
                 *, HOUR(datetime) as jam FROM cuaca 
                join kode_cuaca where kode_cuaca.kode_cuaca=kode
                and provinsi = '".$prov."'
                and datetime < '".$now2."'
                and datetime > '".$now1."'
                ORDER BY datetime desc
                limit 1";

        $stmt2 = $this->_dbh->prepare($sql2);
        $stmt2->execute();

        }

        $data['today'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $data['today_1'] = $stmt1->fetchAll(PDO::FETCH_ASSOC);
        $data['today_2'] = $stmt2->fetchAll(PDO::FETCH_ASSOC);
        return $data;
     }

}