<?php
/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 *
 */

class Marker {
    private $_dbh;

    /**
     * properties
     */

    private $_marker_id;
    private $_title;
    private $_description;
    private $_lat;
    private $_lng;
    private $_marker_type;
    private $_image_type;
    private $_prov;
    private $_city;
    private $_kec;
    private $_kel;
    private $_status;
    private $_pengelola;
    private $_sumberdata;
    private $_subkategori;
    private $_created_by;
    private $_updated_by;

    function __construct($dbh) {
        $this->_dbh = $dbh;
    }

    public function add() {
        $stmt = $this->_dbh->prepare("INSERT INTO " . DB_MARKERS_TABLE . " (title, description, lat, lng, marker_type, image_type, provinsi, kota, kecamatan, kelurahan, status, pengelola, subkategori, sumber_data, created_by, date_created) VALUES (:title, :description, :lat, :lng, :marker_type, '', :provinsi, :kota, :kecamatan, :kelurahan, :status, :pengelola, :subkategori, :sumber_data, :created_by, :date_created)");
//var_dump($this);
//        if (!$stmt) {
//            echo "\nPDO::errorInfo():\n";
//            print_r($this->_dbh->errorInfo());
//        }exit;
        $date = date('Y-m-d H:i:s');
        $stmt->bindParam(':title', $this->_title);
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':lat', $this->_lat);
        $stmt->bindParam(':lng', $this->_lng);
        $stmt->bindParam(':marker_type', $this->_marker_type);
        $stmt->bindParam(':provinsi', $this->_prov);
        $stmt->bindParam(':kota', $this->_city);
        $stmt->bindParam(':kecamatan', $this->_kec);
        $stmt->bindParam(':kelurahan', $this->_kel);
        $stmt->bindParam(':status', $this->_status);
        $stmt->bindParam(':pengelola', $this->_pengelola);
        $stmt->bindParam(':subkategori', $this->_subkategori);
        $stmt->bindParam(':sumber_data', $this->_sumberdata);
        $stmt->bindParam(':created_by', $this->_created_by);
        $stmt->bindParam(':date_created', $date);
        $stmt->execute();
        $this->_marker_id = $this->_dbh->lastInsertId();
       /* if(!empty($this->_marker_id)){
     $query_notif = "INSERT INTO comments(comment_subject, comment_text,lat,lng) VALUES (:title, :description,:lat,:lng) ";
             $stmt = $this->_dbh->prepare($query_notif);
            $stmt->bindParam(':title', $this->_title);
            $stmt->bindParam(':description', $this->_description);
            $stmt->bindParam(':lat', $this->_lat);
            $stmt->bindParam(':lng', $this->_lng);
            $stmt->execute();
        }*/
        return $this->_marker_id;
    }

    public function update() {
        $stmt = $this->_dbh->prepare("UPDATE " . DB_MARKERS_TABLE . " SET title = :title, description = :description, "
            . "lat = :lat, lng = :lng, marker_type = :marker_type, provinsi = :provinsi, kota = :kota, kecamatan = :kecamatan, kelurahan = :kelurahan, status = :status, "
            . "pengelola = :pengelola, subkategori = :subkategori, sumber_data = :sumber_data, updated_by = :updated_by, date_updated = :date_updated WHERE id = :marker_id");
        $date = date('Y-m-d H:i:s');
        $stmt->bindParam(':title', $this->_title);
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':lat', $this->_lat);
        $stmt->bindParam(':lng', $this->_lng);
        $stmt->bindParam(':marker_type', $this->_marker_type);
        $stmt->bindParam(':provinsi', $this->_prov);
        $stmt->bindParam(':kota', $this->_city);
        $stmt->bindParam(':kecamatan', $this->_kec);
        $stmt->bindParam(':kelurahan', $this->_kel);
        $stmt->bindParam(':status', $this->_status);
        $stmt->bindParam(':pengelola', $this->_pengelola);
        $stmt->bindParam(':subkategori', $this->_subkategori);
        $stmt->bindParam(':sumber_data', $this->sumber_data);
        $stmt->bindParam(':updated_by', $this->_updated_by);
        $stmt->bindParam(':date_updated', $date);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        return $stmt->execute();
    }

    public function updateImageType() {
        $stmt = $this->_dbh->prepare("UPDATE " . DB_MARKERS_TABLE . " SET image_type = :image_type WHERE id = :marker_id");
        $stmt->bindParam(':image_type', $this->_image_type);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        $stmt->execute();
    }
    public function updateDescription() {
        $stmt = $this->_dbh->prepare("UPDATE " . DB_MARKERS_TABLE . " SET description = :description WHERE id = :marker_id");
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        $stmt->execute();
    }

    public function getById() {
        $sql = "SELECT * FROM " . DB_MARKERS_TABLE . " WHERE id = :markerid";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute(array(':markerid' => $this->_marker_id));
        return $sth->fetch();
    }

    public function getByTitle($title) {
        $sql = "SELECT * FROM " . DB_MARKERS_TABLE . " WHERE title = :title LIMIT 1";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute(array(':title' => $title));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function deleteById() {
        $sql = "DELETE FROM " . DB_MARKERS_TABLE . " WHERE id = :markerid";
        $stmt = $this->_dbh->prepare($sql);
        return ($stmt->execute(array(':markerid' => $this->_marker_id)));
    }

    public function deleteAll() {
        $sql = "DELETE FROM " . DB_MARKERS_TABLE;
        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute();
    }

    /**
     *
     * return array('count_num_rows', 'rows') by limit, offset, search string
     *
     * count_num_rows - numbr rows without limits
     * rows - array markers info
     *
     * @param integer $limit
     * @param integer $offset
     * @param string $search_text
     * @return array
     */

    public function get_markers_around($lat,$lng,$limit = 0, $offset = 0, $search_text = "", $marker_type = array(), $order_by = 'id DESC') {
        $sql_where = "";
        if (is_array($marker_type) && sizeof($marker_type)) {
            $sql_where .= " WHERE marker_type IN (" . implode(",", $marker_type) . ")";
        }
        $sql ="SELECT id, ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) * 1.61 AS distance FROM markers $sql_where HAVING  distance < 62 ORDER BY distance";


        $sth = $this->_dbh->prepare($sql);
        if($limit == 0) {
            $limit = $data['count_num_rows'];
        }
        $offset = intval($offset);
        $limit = intval($limit);
        $sth->bindParam(":offset_markers", $offset, PDO::PARAM_INT);
        $sth->bindParam(":markers_per_page", $limit, PDO::PARAM_INT);

        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        $sth->execute();

        $datas_id = $sth->fetchAll(PDO::FETCH_ASSOC);

        $j=0;
        foreach($datas_id as $data_objek){
            $id = $data_objek['id'];
            $sql_data = "SELECT * from markers WHERE id='$id'";
            $sth = $this->_dbh->prepare( $sql_data); 
            $sth->execute(); 
            $row = $sth->fetch();
            if($row['lat'] == $lat && $row['lng'] == $lng) {
                continue;
            }
            $row['distance'] = $data_objek['distance'];
            $data['rows'][$j] = $row;
            $j++;
        }
        $data['count_num_rows'] = $j;
        return $data;
    }

    public function get_markers($limit = 0, $offset = 0, $search_text = "", $marker_type = array(), $provinsi = "", $order_by = 'id DESC') {
        $sql_where = "";
        if (strlen($search_text) > 3) {
            $search_text = "%$search_text%";
            $sql_where .= " AND (title LIKE :search_text OR description LIKE :search_text)";
        }
        if (strlen($provinsi) > 3) {
            $sql_where .= " AND provinsi = :provinsi";
        }
        if (is_array($marker_type) && sizeof($marker_type)) {
            $sql_where .= " AND marker_type IN (" . implode(",", $marker_type) . ")";
        }
        if(strlen($sql_where)) {
            $sql_where = " WHERE 1" . $sql_where;
        }
        $sql = "SELECT count(*) FROM " . DB_MARKERS_TABLE . $sql_where;

        $sth = $this->_dbh->prepare($sql);
        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        if (strlen($provinsi) > 3) {
            $sth->bindParam(":provinsi", $provinsi, PDO::PARAM_STR);
        }
        $sth->execute();
        $data['count_num_rows'] = $sth->fetchColumn();

        $sql = "SELECT * FROM " . DB_MARKERS_TABLE . $sql_where . " ORDER BY " . $order_by . " LIMIT :offset_markers, :markers_per_page";
        $sth = $this->_dbh->prepare($sql);
        if($limit == 0) {
            $limit = $data['count_num_rows'];
        }
        // echo nl2br($sql);exit();
        $offset = intval($offset);
        $limit = intval($limit);
        $sth->bindParam(":offset_markers", $offset, PDO::PARAM_INT);
        $sth->bindParam(":markers_per_page", $limit, PDO::PARAM_INT);

        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        if (strlen($provinsi) > 3) {
            $sth->bindParam(":provinsi", $provinsi, PDO::PARAM_STR);
        }
        $sth->execute();

        $data['rows'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function get_markers_bencana($limit = 0, $offset = 0, $search_text = "", $marker_type = array(), $order_by = 'id DESC') {
        $sql_where = "";
        
        if (is_array($marker_type) && sizeof($marker_type)) {
            $sql_where .= " AND marker_type IN (" . implode(",", $marker_type) . ")";
        }
        if(strlen($sql_where)) {
            $sql_where = " WHERE 1" . $sql_where;
        }
        $sql = "SELECT count(*) FROM " . DB_MARKERS_TABLE . $sql_where;

        $sth = $this->_dbh->prepare($sql);
 
        $sth->execute();
        $data['count_num_rows'] = $sth->fetchColumn();

        $sql = "SELECT id, lat, lng, marker_type FROM " . DB_MARKERS_TABLE . $sql_where . " ORDER BY " . $order_by . " LIMIT :offset_markers, :markers_per_page";
        $sth = $this->_dbh->prepare($sql);
        if($limit == 0) {
            $limit = $data['count_num_rows'];
        }
        $offset = intval($offset);
        $limit = intval($limit);
        $sth->bindParam(":offset_markers", $offset, PDO::PARAM_INT);
        $sth->bindParam(":markers_per_page", $limit, PDO::PARAM_INT);

        if (strlen($search_text) > 3) {
            $sth->bindParam(":search_text", $search_text, PDO::PARAM_STR);
        }
        $sth->execute();

        $data['rows'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    /**
     *
     * get markers by lon and lat
     *
     * @param float $swlat
     * @param float $nelat
     * @param float $swlon
     * @param float $nelon
     * @return array
     */


    public function get_map_markers($swlat, $nelat, $swlon, $nelon, array $filter) {
        $sql = "SELECT id, lat, lng, marker_type,1 as jenis
                                FROM " . DB_MARKERS_TABLE ;
        $where =  " WHERE lat BETWEEN :LAT1 AND :LAT2";
                                    //AND (lon BETWEEN LON1 AND -180 OR lon BETWEEN LON2 AND 180)";
                                //where (lat between :swlat AND :nelat) and (lng between :swlon and :nelon)";
        if($nelon < 0 && $swlon > 0) {
            $where .= " AND (lng BETWEEN -180 AND :LON1 OR lng BETWEEN :LON2 AND 180)";
        }
        else {
            $where .= " AND lng BETWEEN :LON1 AND :LON2";
        }
        if(sizeof($filter)) {
            $sql .= $where. " AND marker_type IN (" . implode(", ", $filter) . ")";
            $marker_filter = " AND marker_type IN (" . implode(", ", $filter) . ")";
        }

        $LAT1 = min($nelat, $swlat);
        $LAT2 = max($nelat, $swlat);
        $LON1 = min($nelon, $swlon);
        $LON2 = max($nelon, $swlon);

        $sql .= " UNION SELECT id_bencana as id, lat, lng, marker_type,2 as jenis FROM " . DB_BENCANA_TABLE. " WHERE (waktu  <= DATE(NOW()) AND WAKTU >=  (DATE(NOW()) - INTERVAL 1 DAY)) or is_penting = 1";
        $sql .= " UNION SELECT id_ekraf as id, lat, lng, marker_type,3 as jenis FROM " . DB_EKRAF_TABLE . $where . $marker_filter;

        // echo nl2br($sql);
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':LAT1', $LAT1);
        $stmt->bindParam(':LAT2', $LAT2);
        $stmt->bindParam(':LON1', $LON1);
        $stmt->bindParam(':LON2', $LON2);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    
     public function get_markers_v2($marker_type=''){
        
        $sql_where = "";        
        if($marker_type != null){
            $sql_where =  "WHERE U.marker_type = ".$marker_type;
        }

        $sql = "SELECT
                    * 
                FROM
                    (
                    SELECT
                        id,
                        lat,
                        lng,
                        marker_type,
                        1 AS jenis 
                    FROM
                        markers UNION
                    SELECT
                        id_bencana AS id,
                        lat,
                        lng,
                        marker_type,
                        2 AS jenis 
                    FROM
                        bencana_bnpb 
                        WHERE (waktu  <= DATE(NOW()) AND WAKTU >= (DATE(NOW()) - INTERVAL 3 DAY)) or is_penting = 1
                        UNION
                    SELECT
                        id_ekraf AS id,
                        lat,
                        lng,
                        marker_type,
                        3 AS jenis 
                    FROM
                        ekraf 
                    ) AS U ". $sql_where;

        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute();
        $data['markers'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
       $data['row'] = count($data['markers']);
        return $data;
     }


      public function get_detail_notif_v2($comment_id){
        $sql = "SELECT comments.*, bencana_bnpb.marker_type,bencana_bnpb.id_bencana FROM comments LEFT JOIN bencana_bnpb ON bencana_bnpb.id_bencana = comments.bencana_id  WHERE comment_id = :commentid";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute(array(':commentid' => $comment_id));
        return $sth->fetch();
     }

    public function get_status() {
        $sql = "SELECT * FROM " . DB_STATUS_TABLE ;

        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_subkategori() {
        $sql = "SELECT * FROM " . DB_SUBKATEGORI_TABLE ;

        $stmt = $this->_dbh->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getStatusById($id_status) {
        $sql = "SELECT * FROM " . DB_STATUS_TABLE . " WHERE id_status = :idstatus";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute(array(':idstatus' => $id_status));
        return $sth->fetch();
    }

    public function get_list_provinsi() {
        $sql = "SELECT DISTINCT provinsi FROM " . DB_MARKERS_TABLE . " WHERE provinsi != '' ORDER BY provinsi ASC";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     *
     * count all markes in the system
     *
     * @global $dbh
     * @return integer
     *
     */

    public function count_markers($marker_type='') {
        $sql_where = '';
        if(!empty($marker_type)){
            $sql_where = ' WHERE marker_type= :marker_type';
        }
        $sql = "SELECT count(*) FROM " . DB_MARKERS_TABLE . $sql_where;
        $sth = $this->_dbh->prepare($sql);
        if(!empty($marker_type)){
            $sth->bindParam(':marker_type', $marker_type);
        }
        $sth->execute();
        return $sth->fetchColumn();
    }

    public function setId($marker_id) {

        if (!is_int($marker_id)) {
            throw new \InvalidArgumentException('The Marker ID has wrong format');
        }
        $this->_marker_id = $marker_id;
    }

    public function setTitle($title) {
        if (strlen($title) == 0) {
            throw new \InvalidArgumentException('The title is too short');
        }
        $this->_title = $title;
    }

    public function setImageType($image_type) {
        $this->_image_type = $image_type;
    }

    public function setDescription($description) {
        $this->_description = $description;
    }

    public function setLat($lat) {
        if (!is_float($lat)) {
            throw new \InvalidArgumentException('The latitude has wrong format');
        }
        $this->_lat = $lat;
    }

    public function setLng($lng) {
        if (!is_float($lng)) {
            throw new \InvalidArgumentException('The longitude has wrong format');
        }
        $this->_lng = $lng;
    }

    public function setMarkerType($marker_type) {
        if (!is_int($marker_type)) {
            throw new \InvalidArgumentException('The Marker Type has wrong format');
        }
        $this->_marker_type = $marker_type;
    }

    public function setProvince($prov) {
        $this->_prov = $prov;
    }

    public function setCity($city) {
        $this->_city = $city;
    }

    public function setKecamatan($kec) {
      $this->_kec = $kec;
  }

  public function setKelurahan($kel) {
      $this->_kel = $kel;
  }

    public function setStatus($status) {
        if (!is_int($status)) {
            throw new \InvalidArgumentException('The Status Field has wrong format');
        }
        $this->_status = $status;
    }

    public function setPengelola($pengelola) {
        $this->_pengelola = $pengelola;
    }

    public function setSumberdata($sumber_data) {
        $this->_sumberdata = $sumber_data;
    }
    
    public function setSubkategori($subkategori) {
        if (!is_int($subkategori)) {
            throw new \InvalidArgumentException('The Subkategori Field has wrong format');
        }
        $this->_subkategori = $subkategori;
    }

    public function setCreatedBy($created_by) {
        $this->_created_by = $created_by;
    }

    public function setUpdatedBy($updated_by) {
        $this->_updated_by = $updated_by;
    }

}