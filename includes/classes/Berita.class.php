<?php


class Berita {
    private $_dbh;
    private $tbl_name = 'berita';

    function __construct($dbh) {
        $this->_dbh = $dbh;
    }

    public function get_tbl_name(){
        return $this->tbl_name;
    }

    public function get_jumlah(){
        $jml_data = 0;
        $sql = "SELECT count(*) as jumlah FROM " . $this->tbl_name;
        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        $jml_data = $sth->fetchColumn();
        return $jml_data;
    }
    public function add() {
        $stmt = $this->_dbh->prepare("INSERT INTO " . $this->tbl_name . " (title, description, lat, lng, marker_type, image_type) VALUES (:title, :description, :lat, :lng, :marker_type, '')");

        $stmt->bindParam(':title', $this->_title);
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':lat', $this->_lat);
        $stmt->bindParam(':lng', $this->_lng);
        $stmt->bindParam(':marker_type', $this->_marker_type);
        $stmt->execute();
        $this->_marker_id = $this->_dbh->lastInsertId();
        if(!empty($this->_marker_id)){
             $query_notif = "INSERT INTO comments(comment_subject, comment_text,lat,lng) VALUES (:title, :description,:lat,:lng) ";
             $stmt = $this->_dbh->prepare($query_notif);
            $stmt->bindParam(':title', $this->_title);
            $stmt->bindParam(':description', $this->_description);
            $stmt->bindParam(':lat', $this->_lat);
            $stmt->bindParam(':lng', $this->_lng);
            $stmt->execute();
        }
        return $this->_marker_id;
    }

    public function update() {
        $stmt = $this->_dbh->prepare("UPDATE " . $this->tbl_name . " SET title = :title, description = :description, " . "lat = :lat, lng = :lng, marker_type = :marker_type WHERE id = :marker_id");
        $stmt->bindParam(':title', $this->_title);
        $stmt->bindParam(':description', $this->_description);
        $stmt->bindParam(':lat', $this->_lat);
        $stmt->bindParam(':lng', $this->_lng);
        $stmt->bindParam(':marker_type', $this->_marker_type);
        $stmt->bindParam(':marker_id', $this->_marker_id);
        $stmt->execute();
    }


    public function get_by_id($id){
        $sql = "SELECT * from " . $this->tbl_name . " WHERE id_berita = :id_berita";
        $stmt = $this->_dbh->prepare($sql);
        $stmt->bindParam(':id_berita', $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function get_data($bulan,$tahun) {
        if(!isset($bulan)){
            $bulan = date('m');
        }
        if(!isset($tahun)){
            $tahun = date('Y');
        }
        $sql = "SELECT *   FROM " . $this->tbl_name . " WHERE MONTH(waktu) = '$bulan' AND YEAR(waktu) = '$tahun' ORDER BY waktu DESC";

        $sth = $this->_dbh->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_data_limit($limit = 0, $offset = 0) {
        $limit = intval($limit);
        $sql = "SELECT *   FROM " . $this->tbl_name . " ORDER BY WAKTU DESC  LIMIT :data_start, :data_per_page";
        $sth = $this->_dbh->prepare($sql);
        $offset = intval($offset);

        $sth->bindParam(":data_start", $offset, PDO::PARAM_INT);
        $sth->bindParam(":data_per_page", $limit, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    
    public function getById($id) {
        $sql = "SELECT * FROM " . DB_BENCANA_TABLE . " WHERE id_bencana = :id_bencana";
        $sth = $this->_dbh->prepare($sql);
        $sth->execute(array(':id_bencana' => $id));
        return $sth->fetch();
    }

    
}