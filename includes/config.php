<?php
date_default_timezone_set('Asia/Jakarta');

/**
 * switch for dev and prod
 */
define('DEBUG', false);
if(DEBUG) {
    error_reporting(E_ALL & ~E_STRICT);
    ini_set('display_errors', 1);
}
else {
    error_reporting(0);
    ini_set('display_errors', 0);
}

/**
 * database settings
 */
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "wisatatangguh");
define("DB_HOST", "localhost");
define("HASH_KEY", ""); //for password
define("DB_TABLE_PREFIX", "");
define("DB_CUACA", DB_TABLE_PREFIX . "cuaca");
define("DB_MARKERS_TABLE", DB_TABLE_PREFIX . "markers");
define("DB_MARKER_TYPE_TABLE", DB_TABLE_PREFIX . "marker_type");
define("DB_BENCANA_TABLE", DB_TABLE_PREFIX . "bencana_bnpb");
define("DB_BENCANA_INFO_TABLE", DB_TABLE_PREFIX . "bencana_informasi");
define("DB_BERITA_TABLE", DB_TABLE_PREFIX . "berita");
define("DB_STORAGE_TABLE", DB_TABLE_PREFIX . "storage");
define("DB_STATUS_TABLE", DB_TABLE_PREFIX . "status");
define("DB_SUBKATEGORI_TABLE", DB_TABLE_PREFIX . "subkategori");
define("DB_SUBSEKTOR_EKRAF_TABLE", DB_TABLE_PREFIX . "subsektor_ekraf");
define("DB_EKRAF_TABLE", DB_TABLE_PREFIX . "ekraf");
define("INDEX_CLUSTER_MAP_KEY", "index_cluster_map_");

/**
 * site path settings
 */
$baseurl = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$baseurl.= "://" . $_SERVER['HTTP_HOST'];
$baseurl .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

define("APP_URL", $baseurl);
define("SITE_DOMAIN", $baseurl);
define("APP_DIR", "");
define("APP_PATH", __DIR__ . "/../");
define("HTTP_APP_PATH", SITE_DOMAIN . APP_DIR);
define("HTTP_ICO_PATH", HTTP_APP_PATH . '/static/img/ico/'); //path to marker images
define("APP_ICO_PATH", APP_PATH . 'static/img/ico/'); //path to marker images
define("HTTP_IMG_PATH", HTTP_APP_PATH . '/static/img/upload/'); //path to marker images
define("HTTP_EKRAF_PATH", HTTP_APP_PATH . 'static/img/upload/ekraf/'); //path to ekraf images
define("APP_IMG_PATH", APP_PATH . 'static/img/upload/'); //path to marker images
define("APP_EKRAF_PATH", APP_PATH . 'static/img/upload/ekraf/'); //path to ekraf images

define("NXIK_GMC_USER_NAME", "admin");
define("NXIK_GMC_USER_PASSWORD", "admin");

define("NXIK_API_IPSTACK_URL", "http://api.ipstack.com/");
define("NXIK_API_IPSTACK_KEY", "");

$endpoint_pvmbg = 'https://magma.esdm.go.id/';
$token_pvmbg = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvbWFnbWEuZXNkbS5nby5pZFwvYXBpXC9sb2dpblwvc3Rha2Vob2xkZXIiLCJpYXQiOjE1OTc0MTI2MjcsImV4cCI6MTYyODk0ODYyNywibmJmIjoxNTk3NDEyNjI3LCJqdGkiOiJVbjJySnpzd1NiTjlybnAyIiwic3ViIjo2LCJwcnYiOiI0YTlkOWEyZDI2ODAyYzMxMmU4ZTVhNWJlNjBmZjI2ZjBmYzYzZDdkIiwic291cmNlIjoiTUFHTUEgSW5kb25lc2lhIiwiYXBpX3ZlcnNpb24iOiJ2MSIsImRheXNfcmVtYWluaW5nIjozNjQsImV4cGlyZWRfYXQiOiIyMDIxLTA4LTE0IDAwOjAwOjAwIn0.XQ9JBUuHsmN1C1znH_66BQmBC61vpw8eYfoeJnAAMBU";

$xls_mime = array(
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "application/vnd.ms-excel",
    "application/octet-stream"
);
$csv_mime = array(
    "text/plain"
);

$arr_3A = array('1'=>'Atraksi','2'=>'Aksesibilitas','3'=>'Amenitas');
$arr_role = array('1'=>'Admin','2'=>'Operator','3'=>'Eksekutif');
$allowed_pages = array("index.php", "login.php", "exec.php", "ekatalog.php", "bencana-list.php","twitter.php","bencana-detil.php","objek-detil.php","ekraf-detil.php","revgeocode.php");
$arr_bencana_bnpb = array('KEKERINGAN'=>'37','KEBAKARAN'=>'33','KEBAKARAN HUTAN DAN LAHAN'=>'34','PUTING BELIUNG'=>'44','BANJIR'=>'12','TANAH LONGSOR'=>'47','LAINNYA'=>'50','KECELAKAAN TRANSPORTASI'=>'36','KONFLIK / KERUSUHAN SOSIAL'=>'40' ,'GEMPA BUMI DAN TSUNAMI'=>'30','GELOMBANG PASANG / ABRASI'=>'29','LETUSAN GUNUNG API'=>'41','AKSI TEROR / SABOTASE'=>'10','BANJIR DAN TANAH LONGSOR'=>'28','GEMPA BUMI'=>'8','TSUNAMI'=>'49','KELAPARAN'=>'38','SERANGAN HEWAN LIAR'=>'45','KLB'=>'39','KECELAKAAN INDUSTRI'=>'35','TALUD LONGSOR'=>'46','ORANG HANYUT'=>'42','TANGGUL JEBOL'=>'48','BANGUNAN ROBOH'=>'11','JEMBATAN ROBOH'=>'32');
/**
 * don't change this array
 */

$default_map_settings = array(
    'config_autodetect_map_center' => 0,
    'config_map_center_latitude' => -2.6,
    'config_map_center_longitude' => 118.01,
    'config_map_width' => "100%",
    'config_map_height' => "1000px",
    'config_min_zoom' => 3,
    'config_max_zoom' => 21,
    'config_zoom' => 5,
    'config_distance' => 50,
    'config_markers_cluster_level_2' => 10,
    'config_markers_cluster_level_3' => 50,
    'config_markers_cluster_level_4' => 100,
    'config_markers_cluster_level_5' => 300,
    'config_unclusterise_same_markers' => false,
    'config_enable_cache' => false,
    'config_cache_level' => 1,
    'config_show_makers_filter' => 1,
    'config_search_results_per_page' => 15,
    'config_show_makers_search' => true,
    'config_filter_search_place' => true,
    'config_alwaysClusteringEnabledWhenZoomLevelLess' => 2,
    'config_map_type_id' => 'ROADMAP',
    'config_map_language' => 'en',
    'config_map_source_id' => 0,
);

$_arr_bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
/**
 * manage settings
 */

$config_markers_per_page = array(10, 50, 100);


/**
 * marker types
 */

$config_marker_types = array(
    "jsonGetMarkerUrl" => HTTP_APP_PATH . '/exec.php',
    "jsonMarkerUrl" => HTTP_APP_PATH . '/exec.php',
    "jsonGetMarkerInfoUrl" => HTTP_APP_PATH . '/exec.php',
    "jsonMarkerInfoUrl" => HTTP_APP_PATH . '/exec.php',
    "clusterImage" => array(
        "src" => HTTP_APP_PATH . '/static/img/cluster2.png',
        "height" => 60,
        "width" => 60,
        "offsetH" => 30,
        "offsetW" => 30
    ),
/**
  * http://mapicons.nicolasmollet.com/
 * 
 * * marker types
 */    
    "pinImage" => array(),
    "textErrorMessage" => 'An error has occured'
);

define("DB_MARKERS_TABLE_SQL",  <<<EOD
CREATE TABLE IF NOT EXISTS `%s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` decimal(9,6) NOT NULL,
  `lng` decimal(9,6) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `marker_type` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `image_type` char(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_add` (`date_add`),
  KEY `marker_type` (`marker_type`),
  KEY `lat` (`lat`),
  KEY `lng` (`lng`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOD
);

define("DB_STORAGE_TABLE_SQL",  <<<EOD
CREATE TABLE IF NOT EXISTS `%s` (
  `data_key` varchar(64) COLLATE utf8_bin NOT NULL,
  `data_value` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`data_key`),
  KEY `last_update` (`last_update`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
EOD
);
define("DB_MARKER_TYPE_TABLE_SQL",  <<<EOD
CREATE TABLE IF NOT EXISTS `%s` (
  `id` smallint(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(1024) COLLATE utf8_bin NOT NULL,
  `image_type` varchar(5) COLLATE utf8_bin NOT NULL,
  `image_width` smallint(6) UNSIGNED NOT NULL,
  `image_height` smallint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
EOD
);
