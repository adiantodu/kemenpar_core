<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include_once("includes/config.php"); 
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <title>Kemenparekraf</title>
    <link rel="icon" href="v2/asset/favicon.ico" />

    <link href="v2/css/style.css" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <link
        rel="stylesheet"
        href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossorigin=""
    />
    <script
        src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
        integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
        crossorigin=""
    ></script>


    <script type="text/javascript"
        src="https://cdn.jsdelivr.net/gh/hosuaby/Leaflet.SmoothMarkerBouncing@v2.0.0/dist/bundle.js"
        crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
    <script src="https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js"></script>


    
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.css' rel='stylesheet' />

</head>
<body>
    <?php
    include_once(APP_PATH."v2/includes/navbar.php"); 
    ?>
    <div class="section flex-c h-center home-sec1">
        
        <?php include_once(APP_PATH."v2/includes/modalsection.php"); ?> 
        <div id="map-wrapper"></div>

        <?php include_once(APP_PATH."v2/includes/menumap.php"); ?>


    </div>

    <div class="toast flex-c">
        
    </div>

    <!-- <div class="filter-wrap">
        <button class="cursor filter-btn flex-c v-center h-center" onclick="toggleFilter()">
            <img src="asset/filter-ico.svg"/>
        </button>

        <div class="filter-menu">

        </div>
    </div> -->


    <script src="v2/js/leaflet.ajax.min.js"></script>
    <script src="v2/js/index.js"></script>
    <script type="text/javascript">
        
        var HTTP_APP_PATH =  "<?php echo APP_URL; ?>";
        var first = true
        var play = true

        function playAudio() { 
            try{
                var audio = new Audio('quarrel.mp3');
                audio.play(); 
            }catch(e){}
        } 

        function load_unseen_notification(view = '')
            {
                var params = {
                    "view2": view,
                    "notif_v2": "v2"
                 }
                 
            $.ajax({
            url:"fetch-notif.php",
            method:"POST",
            data:params,
            dataType:"json",
            success:function(data)
            {
                if(first){
                    // window.localStorage.setItem('notif_count',data.notification)
                    if(!window.localStorage.getItem('notif_list')){
                        window.localStorage.setItem('notif_list', JSON.stringify([]))
                    }
                    first = false
                }

                var rr = JSON.parse(window.localStorage.getItem('notif_list'))
                var co = 0
                
                $('#notif-wrapper').html(data.notification);
                var cc = $('.notification-container').children()
                if(!cc[0].classList.contains('no-notif')){
                    for(let y=0; y<cc.length; y++){
                        var ii = rr.indexOf(parseInt(cc[y].id))
                        if(ii > -1){
                            $('.notification-container').children()[y].style.background = 'white'
                        }else{
                            co++
                            if(play){
                                playAudio()
                                play = false
                            }
                        }
                        
                        if(y==cc.length-1){
                            play = true
                        }
                    }
                }

                if(co > 0){
                    $('.notification-count').css("display","inline-block");
                    $('.notification-count').html(co);
                    bencanaNotif()
                    $('.nav-menu-btn').addClass('notif')
                }else{
                    $('.notification-count').css("display","none");
                    bencanaNotif(true)
                    $('.nav-menu-btn').removeClass('notif')
                }

                
                

            }
            });
            }

        $(document).on('click', '#dropdown-notif', function(){
            $('.count').html('');
            load_unseen_notification('yes');
        });

        $(document).ready(()=>{
            setInterval(updateTime, 1000);
            loadMap3();
            load_unseen_notification();
            setInterval(function(){ 
                load_unseen_notification();
            }, 10000);
        })

    </script>
</body>
</html>
