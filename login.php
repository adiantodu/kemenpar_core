<?php

include_once("includes/config.php");
include_once(APP_PATH . "/exec.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Log in Aplikasi">
    <link rel="shortcut icon" href="static/img/favicon.png">
    <title>Login Aplikasi</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo HTTP_APP_PATH ?>/static/css/style.css" />

    <link type="text/css" rel="stylesheet" href="<?php echo HTTP_APP_PATH ?>/static/css/vegas.min.css" />
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo HTTP_APP_PATH ?>/static/js/vegas.min.js"></script>
    <style type="text/css">

.login-box {
width: 250px;
margin: auto;
padding: 10px;
background: linear-gradient(to bottom, rgba(136, 81, 125, 0.6) 0%, rgba(222, 222, 222, 0.4) 100%);
}

.form-signin-heading{
    color:#fff;
}
h4{
    color: #e3cd2b;
}
</style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <a class="navbar-brand" href="index.php" style="padding-top: 0px;"><img src="static/img/favicon.png" alt="" class="logo-navbar" id="brand-logo" style="height:42px;padding-top:2px;" ></a>
            <div class="navbar-header">
                   <button class="navbar-toggle" data-toggle="collapse"  data-target="#menu-utama">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <a class="navbar-brand" href="<?php echo HTTP_APP_PATH ?>">
                    <?php echo $lang["site_title"]; ?>
                </a>
            </div>
              <div class="collapse navbar-collapse" id="menu-utama">
                <div class="navbar-offcanvas offcanvas">
                    <ul class="nav navbar-nav">
                        <?php include_once("blocks/website_menu.php");?>
                    </ul>
                    <ul class="nav navbar-right">
                        <?php include_once("blocks/user_menu.php");?>
                    </ul>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>
    <div class="container-fluid">
        <div class="login-box pull-right">
        <form class="form-signin" action="<?php echo HTTP_APP_PATH ?>login.php" method="post">
            <h2 class="form-signin-heading">Login</h2>
            <?php 
            $msg ='';
            if(!empty($_SESSION['msg'])) {
                $msg = $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            echo $msg;?>
            <label for="inputEmail" class="sr-only">Username</label>
            <input type="text" name="login" id="inputEmail" class="form-control" placeholder="User name"  value="<?php echo NXIK_GMC_USER_NAME == 'admin'?NXIK_GMC_USER_NAME:''; ?>" required autofocus><br>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" value="<?php echo NXIK_GMC_USER_PASSWORD == 'admin'?NXIK_GMC_USER_PASSWORD:'';?>" required>
            <input type="hidden" name="action" value="login">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
        </form>
    </div>
    </div>
    <script>
        $(function() {
    $('body').vegas({
        delay: 8000,
        shuffle: true,
        animationDuration: 10000,
        slides: [
            { src: 'static/img/banyuwangi.jpg' },
            { src: 'static/img/laketoba.jpg' },
            { src: 'static/img/raja-ampat.jpg' },
            { src: 'static/img/bintan.jpg' },
            { src: 'static/img/solo.jpg' }           

        ]
    });
});

    </script>
    <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
</body>
</html>