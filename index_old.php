<?php
$sidebar = true;
$title = "Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
?>
        <div class="container-fluid"  style="padding-top: 50px">
            <div class="row">
                <div class="col-lg-<?php echo $map_column;?> col-md-<?php echo $map_column;?> map">
                    <div class="google-map-canvas" id="nxIK_map"></div>
                </div>
                <?php if ($map_settings['config_show_makers_search']): ?>
                    <div class="col-lg-3" id="list-column">
                        <div class="panel panel-primary  ">
                            <div class="panel-heading"><?php echo $lang["Latest_Added_Markers"];?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>manage.php">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="search" class="form-control" width="250" id="search" name="search_text" value="" placeholder="<?php echo $lang["Search_text"];?>">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li></ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="modal right fade" id="right-sidebar" role="dialog">

            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content modal-primary">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title facility" id="marker_title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="mb-1 text-muted" id="marker-date-add"></div>
                        <div id="marker-image"></div>
                        <div></div>
                        <div id="marker-description"></div>
                    </div>
                    <div class="modal-footer">
                        <div id="social-buttons"></div>
                    </div>
                </div>
            </div>
        </div>

        <script lang="javascript">
            var SITE_DOMAIN = '<?php echo SITE_DOMAIN ?>';
            var HTTP_APP_PATH = '<?php echo HTTP_APP_PATH ?>';
            var ZOOM = <?php echo $map_settings['config_zoom'] ?>;
            var AUTODETECT_MAP_CENTER = <?php echo $map_settings['config_autodetect_map_center'] ?>;
            var MAP_CENTER_LAT = <?php echo $map_settings['config_map_center_latitude'] ?>;
            var MAP_CENTER_LNG = <?php echo $map_settings['config_map_center_longitude'] ?>;
            var MARKERS_CLUSTERIZE_2 = <?php echo $map_settings['config_markers_cluster_level_2'] ?>;
            var MARKERS_CLUSTERIZE_3 = <?php echo $map_settings['config_markers_cluster_level_3'] ?>;
            var MARKERS_CLUSTERIZE_4 = <?php echo $map_settings['config_markers_cluster_level_4'] ?>;
            var MARKERS_CLUSTERIZE_5 = <?php echo $map_settings['config_markers_cluster_level_5'] ?>;
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types) ?>;
            var LIST_MARKERS_PER_PAGE = <?php echo $map_settings['config_search_results_per_page'] ?>;
            var MAP_SOURCE = <?php echo $map_settings['config_map_source_id']; ?>;
            <?php if($map_settings['config_map_source_id'] == 1): //mapbox ?>
                var MAPBOX_API_KEY = '<?php echo strlen($map_settings['config_mapbox_api_key'])?$map_settings['config_mapbox_api_key']:"";?>';
            <?php endif;?>
        </script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/js/index-normal.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-bouncing/leaflet.smoothmarkerbouncing.js"></script>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/Leaflet.TextIcon/leaflet-text-icon.js"></script>

        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/js/mapclustering-normal.js"></script>
        <div id="fb-root"></div>
      
        <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>
