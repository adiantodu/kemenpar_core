<?php 
error_reporting(0);
$sidebar = false;
$title = "User Management - Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
include_once(APP_PATH . "/includes/classes/pagination/pagination.class.php");

$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? (int) $_GET["page"] : 1;
$record_per_page = 10;
$offset = $record_per_page * ($current_page - 1);
$user_data = $user_obj->get_data($record_per_page,$offset);
$list_user = $user_data['rows'];

$pagination = (new Pagination());
$pagination->setCurrent($current_page);
$pagination->setTotal($user_data['count_num_rows']);
$pagination->setRPP($record_per_page);
$pagination_html = $pagination->parse();
?>
<div class="container-fluid">
    <?php if (isset($_GET["msg"]) && $_GET['msg'] == "user_deleted"): ?>
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        User Berhasil dihapus.
    </div>
    <?php endif; ?>
    <div class="clearfix"></div>
    <div class="text-center">
        <?php echo $pagination_html ?>
    </div>
    <div class="clearfix"></div>
    <table class="table table-striped table-bordered" id="table">
            <thead>
                <tr class="info">
                <td>Username</td>
                <td>Role</td>
                <td>Login Terakhir</td>
                <td>Aksi</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach($list_user as $user){ ?>
            <tr>
                <td><?php echo $user['username'];?></td>
                <td><?php echo $arr_role[$user['role']];?></td>
                <td><?php echo  $user['last_login'];?></td>
                <td><a href="<?php echo HTTP_APP_PATH ?>/update-user.php?user=<?php echo $user['username']; ?>">Edit</a> | <a href="<?php echo HTTP_APP_PATH ?>/exec.php?action=user-delete&user=<?php echo $user["username"] ?>">Hapus</a></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <a class="btn btn-success" id="add_information" href="user-add.php"> 
        <span class="glyphicon glyphicon-user"></span>  Tambah User
    </a>
</div>
    <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
</body>
</html>