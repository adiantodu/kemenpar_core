<?php 
error_reporting(0);

$sidebar = false;
$title = "Ubah Password - Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
$username = $_GET['user'];
$get_user =  $user_obj->get_by_id($username);
?>
<style type="text/css">
h4{
    color: green;
}
</style>
<div class="container-fluid">
<form class="form-signin" action="<?php echo HTTP_APP_PATH ?>/exec.php?action=user-update" method="post">
<div class="modalfix" id="password_modalfix" >
    <div class="modalfix-header">
        <h3>Tambah User <span class="extra-title muted"></span></h3>
        <?php 
            $msg ='';
            if(!empty($_SESSION['msg'])) {
                $msg = $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            echo $msg;?>
    </div>
    <div class="modalfix-body form-horizontal">
        <div class="control-group">
            <label for="current_password" class="control-label">Username</label>
            <div class="controls">
                <input type="text" name="newusername" value="<?php echo $get_user['username']; ?>">
            </div>
        </div>
        <div class="control-group">
            <label for="role" class="control-label">Group</label>
            <div class="controls">
               <select class="form-control" name="role" id="role">
                <option value="2" <?php echo $get_user['role'] == 2 ? 'selected=selected' : ''; ?>>Operator</option>
                <option value="3" <?php echo $get_user['role'] == 3 ? 'selected=selected' : ''; ?>>Eksekutif (View)</option>
            </select>
            </div>
        </div>

        <div class="control-group">
            <label for="password" class="control-label">Password</label>
            <div class="controls">
                <input type="password" name="password">
            </div>
        </div>
        <div class="control-group">
            <label for="confirm_password" class="control-label">Konfirmasi Password</label>
            <div class="controls">
                <input type="password" name="confirm_password">
            </div>
        </div>      
    </div>
    <div class="modalfix-footer">
        <input type="hidden" name="action" value="change-password">
        <input type='hidden' name='username' id='username' value='<?php echo $get_user['username']; ?>'>
        <button href="#" class="btn btn-primary" id="password_modalfix_save">Simpan</button>
    </div>
</div>
</form>
    </div>
    <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    <script type="text/javascript">
        loadCSS = function(href) {

  var cssLink = $("<link>");
  $("head").append(cssLink); //IE hack: append before setting href

  cssLink.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: href
  });

};
loadCSS("<?php echo HTTP_APP_PATH ?>/static/css/bootstrap-combined.min.css");
    </script>
</body>
</html>