<?php 
error_reporting(0);

$sidebar = false;
$title = "Ubah Password - Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
if($cur_role == 1){
    $list_user = $user_obj->get_data();
}
?>
<style type="text/css">
h4{
    color: green;
}
</style>
<div class="container-fluid">
<form class="form-signin" action="" method="post">
<div class="modalfix" id="password_modalfix" >
    <div class="modalfix-header">
        <h3>Ubah Password <span class="extra-title muted"></span></h3>
        <?php 
            $msg ='';
            if(!empty($_SESSION['msg'])) {
                $msg = $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
            echo $msg;?>
    </div>
    <div class="modalfix-body form-horizontal">
        <?php if($cur_role==1){ ?>
        <div class="control-group">
            <label for="current_password" class="control-label">Username</label>
            <div class="controls">
                 <select name="cmb_username">
<?php foreach ($list_user as $user) { ?>
    <option value="<?php echo $user['username'];?>"><?php echo $user['username'];?></option>
<?php }  ?>
</select> 
            </div>
        </div>
        <?php } ?>
        <div class="control-group">
            <label for="current_password" class="control-label">Password Saat ini</label>
            <div class="controls">
                <input type="password" name="current_password">
            </div>
        </div>
        <div class="control-group">
            <label for="new_password" class="control-label">Password Baru</label>
            <div class="controls">
                <input type="password" name="new_password">
            </div>
        </div>
        <div class="control-group">
            <label for="confirm_password" class="control-label">Konfirmasi Password</label>
            <div class="controls">
                <input type="password" name="confirm_password">
            </div>
        </div>      
    </div>
    <div class="modalfix-footer">
        <input type="hidden" name="action" value="change-password">
        <button href="#" class="btn btn-primary" id="password_modalfix_save">Simpan</button>
    </div>
</div>
</form>
    </div>
    <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    <script type="text/javascript">
        loadCSS = function(href) {

  var cssLink = $("<link>");
  $("head").append(cssLink); //IE hack: append before setting href

  cssLink.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: href
  });

};
loadCSS("<?php echo HTTP_APP_PATH ?>/static/css/bootstrap-combined.min.css");
    </script>
</body>
</html>