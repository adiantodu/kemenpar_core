<?php
chdir(dirname(__FILE__));
//error_reporting(E_ALL); ini_set('display_errors', TRUE); ini_set('display_startup_errors', TRUE);
include_once("../includes/config.php");
// Create connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//array jenis bencana BNPB
$arr_bencana_bnpb = array(
'KEKERINGAN' => '37',
'KEBAKARAN' => '33',
'KEBAKARAN HUTAN DAN LAHAN' => '34',
'PUTING BELIUNG' => '44',
'BANJIR' => '12',
'TANAH LONGSOR' => '4 7',
'LAINNYA' => '50',
'KECELAKAAN TRANSPORTASI' => '36',
'KONFLIK / KERUSUHAN SOSIAL' => '40',
'GEMPA BUMI DAN TSUNAMI' => '30',
'GELOMBANG PASANG / ABRASI' => '29',
'LETUSAN GUNUNG API' => '41',
'AKSI TEROR / SABOTASE' => '10',
'BANJIR DAN TANAH LONGSOR' => '28',
'GEMPA BUMI' => '8',
'TSUNAMI' => '49',
'KELAPARAN' => '38',
'SERANGAN HEWAN LIAR' => '45',
'KLB' => '39',
'KECELAKAAN INDUSTRI' => '35',
'TALUD LONGSOR' => '46',
'ORANG HANYUT' => '42',
'TANGGUL JEBOL' => '48',
'BANGUNAN ROBOH' => '11',
'JEMBATAN ROBOH' => '32'
);

$jns_bencana = 8;
$url = 'https://gis.bnpb.go.id/databencana/kabupaten/';
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$data = curl_exec($curl);
curl_close($curl);
$json_a=json_decode($data,true);
/* echo json_encode($json_a); */
$i = 1;
$dt = $json_a['result'];//$json_a['features'];
foreach ($dt as $value){
// $value = $dt_detil['properties'];
$kib = 	$value['id_prov']."-".$value['id_kab'];
// $waktu  = substr($kib,7,8);
// $waktu_format = substr($waktu,0,4) . '-' . substr($waktu,4,2) . '-' . substr($waktu,6,2);
$waktu_format = $value['tanggal_status'];
$lokasi = $value['kabkot'] .", ". $value['provinsi'];
$kejadian = $value['kejadian'];
$jns_bencana = $arr_bencana_bnpb[$kejadian];
	    $keterangan = "";
	    // $penyebab = $value['penyebab'];
	    // if(!empty($penyebab)) {
	    // if(!empty($keterangan)) {
	    //     $keterangan .= '<br />';
	    // }
	    // $keterangan .= "Penyebab: <br />" . mb_convert_encoding($penyebab, "HTML-ENTITIES", "UTF-8");
	    // }
if(empty($jns_bencana)){
	$jns_bencana = 8;
}
$sql_cek = "SELECT * from ".DB_BENCANA_TABLE." WHERE kib = '$kib'";


// tambah column || ,terendam,pendidikan_rusak,peribadatan_rusak,kesehatan_rusak,kantor_rusak,jembatan_rusak ||
/* Select queries return a resultset */
if ($result = $conn->query($sql_cek)) {
	if($result->num_rows>0){
		// echo "$i . update $kib <br />";
		$sql = "UPDATE ".DB_BENCANA_TABLE." SET meninggal = '".$value['meninggal']."' , hilang = '".$value['hilang']."', terluka = '".$value['terluka']."', menderita =  '".$value['menderita_mengungsi']."', rumah_rusak_berat = '".$value['rumah_rusak_berat']."', rumah_rusak_sedang = '".$value['rumah_rusak_sedang']."', rumah_rusak_ringan = '".$value['rumah_rusak_ringan']."', terendam = '".$value['terendam']."', pendidikan_rusak = '".$value['pendidikan_rusak']."', peribadatan_rusak = '".$value['peribadatan_rusak']."', kesehatan_rusak = '".$value['kesehatan_rusak']."', kantor_rusak = '".$value['kantor_rusak']."', jembatan_rusak = '".$value['jembatan_rusak']."', keterangan = '$keterangan' WHERE  kib = '$kib'";
	} else {

		// echo "insert $kib <br />";
		$sql = "INSERT INTO ".DB_BENCANA_TABLE." (kib, kejadian, lokasi, nkab, nprop, keterangan,meninggal,hilang,terluka,menderita,rumah_rusak_berat,rumah_rusak_sedang,rumah_rusak_ringan,lng,lat,waktu,marker_type,sumber_data,terendam,pendidikan_rusak,peribadatan_rusak,kesehatan_rusak,kantor_rusak,jembatan_rusak) VALUES ('".$kib."', '".$value['kejadian']."', '".$lokasi."', '".$value['kabkot']."', '".$value['provinsi']."', '".$keterangan."', ".$value['meninggal'].", ".$value['hilang'].", ".$value['terluka'].", ".$value['menderita_mengungsi'].", ".$value['rumah_rusak_berat'].", ".$value['rumah_rusak_sedang'].", ".$value['rumah_rusak_ringan'].", '".$value['x']."', '".$value['y']."','$waktu_format','$jns_bencana','BNPB',".$value['terendam'].",".$value['pendidikan_rusak'].",".$value['peribadatan_rusak'].",".$value['kesehatan_rusak'].",".$value['kantor_rusak'].",".$value['jembatan_rusak'].")";
		
		if($conn->query($sql)){
			 $last_id = $conn->insert_id;
			// echo 'komen';
			// masukkan ke notifikasi
			$sql_notif = "INSERT INTO comments(comment_subject, comment_text,lat,lng,comment_date,bencana_id) VALUES ('Informasi Bencana (BNPB)', '".$kejadian . " " . $value['kabkot'] . " " . $value['provinsi'] ."','".$value['y']."','".$value['x']."','".$waktu_format."','".$last_id."')";
			$conn->query($sql_notif);
			echo "inserted data";
	} else {

		echo 'gagal <br />';
		 echo("Error description: " . $conn -> error);
	}

	}



    $result->close();
}

$i++;
}

// $waktu = Date("cron bnpb: Y-m-d h:i:s");
// $path = dirname(__FILE__);
// //echo $path;
// file_put_contents($path. "\log_cron.txt","\n". $waktu, FILE_APPEND);

