<?php

include_once("../includes/config.php");
// Create connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//array jenis bencana BNPB
$arr_bencana_bnpb = array('KEKERINGAN'=>'37','KEBAKARAN'=>'33','KEBAKARAN HUTAN DAN LAHAN'=>'34','PUTING BELIUNG'=>'44','BANJIR'=>'12','TANAH LONGSOR'=>'47','LAINNYA'=>'50','KECELAKAAN TRANSPORTASI'=>'36','KONFLIK / KERUSUHAN SOSIAL'=>'40' ,'GEMPA BUMI DAN TSUNAMI'=>'30','GELOMBANG PASANG / ABRASI'=>'29','LETUSAN GUNUNG API'=>'41','AKSI TEROR / SABOTASE'=>'10','BANJIR DAN TANAH LONGSOR'=>'28','GEMPA BUMI'=>'8','TSUNAMI'=>'49','KELAPARAN'=>'38','SERANGAN HEWAN LIAR'=>'45','KLB'=>'39','KECELAKAAN INDUSTRI'=>'35','TALUD LONGSOR'=>'46','ORANG HANYUT'=>'42','TANGGUL JEBOL'=>'48','BANGUNAN ROBOH'=>'11','JEMBATAN ROBOH'=>'32');
$jns_bencana = 8;
$url = 'https://gis.bnpb.go.id/databencana/kabupaten/';
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$data = curl_exec($curl);
curl_close($curl);
$json_a=json_decode($data,true);
$i = 1;
$dt = $json_a['features'];
foreach ($dt as $dt_detil){
	$value = $dt_detil['properties'];
$kib = 	$value['kib'];
$waktu  = substr($kib,7,8);
$waktu_format = substr($waktu,0,4) . '-' . substr($waktu,4,2) . '-' . substr($waktu,6,2);
$waktu_format = $value['tanggal_status'];

$kejadian = $value['kejadian'];
$jns_bencana = $arr_bencana_bnpb[$kejadian];
if(empty($jns_bencana)){
	$jns_bencana = 8;
}
$sql_cek = "SELECT * from ".DB_BENCANA_TABLE." WHERE kib = '$kib'";

/* Select queries return a resultset */
if ($result = $conn->query($sql_cek)) {
	if($result->num_rows>0){
		echo "$i . update $kib <br />";
	} else {
		//echo "insert $kib <br />";
		$sql = "INSERT INTO ".DB_BENCANA_TABLE." (kib, kejadian, nkab,nprop,keterangan,meninggal,hilang,terluka,menderita,rumah_rusak_berat,rumah_rusak_sedang,rumah_rusak_ringan,lng,lat,waktu,marker_type,sumber_data) VALUES ('".$value['kib']."', '".$value['kejadian']."', '".$value['nkab']."', '".$value['nprop']."', '".$value['keterangan']."', '".$value['meninggal']."', '".$value['hilang']."', '".$value['terluka']."', '".$value['menderita']."', '".$value['rumah_rusak_berat']."', '".$value['rumah_rusak_sedang']."', '".$value['rumah_rusak_ringan']."', '".$value['longitude']."', '".$value['latitude']."','$waktu_format','$jns_bencana','BNPB')";

	if($conn->query($sql)){
	// masukkan ke notifikasi
	$sql_notif = "INSERT INTO comments(comment_subject, comment_text,lat,lng,comment_date,kib) VALUES ('Informasi Bencana (BNPB)', '".$kejadian . " " . $value['nkab'] . " " . $value['nprop'] ."','".$value['latitude']."','".$value['longitude']."','".$value['tanggal_status']."','".$value['kib']."')";
	$conn->query($sql_notif);
	echo "inserted <br/>";
	} else {

		echo 'gagal <br />';
	}


	}
    $result->close();
}

$i++;
}

$waktu = Date("Y-m-d h:i:s");
$path = dirname(__FILE__);
//echo $path;
file_put_contents($path. "\log_cron.txt","\n". $waktu, FILE_APPEND);

