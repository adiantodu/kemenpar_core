<?php
exit();
include_once("../includes/config.php");
// Create connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
// Get all data markers
$sql = "SELECT * from ".DB_MARKERS_TABLE." limit 1000 offset 3881";
$result = $conn->query($sql);
$data = $result->fetch_all(MYSQLI_ASSOC);
foreach($data as $key=>$value) {
    $lat = $value['lat'];
    $lng = $value['lng'];
    if(empty($lat) && empty($lng)) {
        continue;
    }
    $get_geocode = revgeocode($lat,$lng);
    $id = $value['id'];
    $provinsi = $get_geocode['item']['provinsi'];
    $kota = $get_geocode['item']['kota'];
    $sql_update = "UPDATE ". DB_MARKERS_TABLE . " SET provinsi = '$provinsi', kota = '$kota' WHERE id=$id";
    $updated = "";
    if($conn->query($sql_update) === TRUE) {
        $updated = "updated";
    }
    echo "id = ".$id."</br>";
    echo "lat = ".$lat."</br>";
    echo "lng = ".$lng."</br>";
    echo "prov = ".$provinsi."</br>";
    echo "kot = ".$kota."</br>";
    echo $updated."</br>";
    echo "<hr>";
}

// get from api
function revgeocode($lat,$lng) {
    $response = array();
    if(empty($lat) && empty($lng)) {
        return;
    }
    $url = 'https://revgeocode.search.hereapi.com/v1/revgeocode?at='.$lat.','.$lng.'&apiKey=2w7bsEbT9cVN_gJCrEMr69poSTYBeAGYFb2eXf2VKxs&q=city';
    
    try {
        $curl = curl_init();

        // Check if initialization had gone wrong*
        if ($curl === false) {
            throw new Exception('failed to initialize');
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($curl);

        // Check the return value of curl_exec(), too
        if ($data === false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }

        /* Process $data here */
        if($data) {
            $data_arr = json_decode($data, true);
            // var_dump($data_arr['items']);
            $response['status'] = 200;
            $response['msg']    = 'Ok';
            $response['item']   = array(
                'provinsi'  => $data_arr['items'][0]['address']['county'],
                'kota'      => $data_arr['items'][0]['address']['city'],
            );
            return $response;
        } else {
            return $response;
        }

        // Close curl handle
        curl_close($curl);
    } catch(Exception $e) {

        trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);
    }
}