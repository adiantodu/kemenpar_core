<?php 
set_time_limit(0);
ini_set('memory_limit', '2048M');
$url_provinsi = "https://jadesta.com/getdata/peta?idprov=0&idkota=0&filter=";
ini_set('memory_limit', '2048M');
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "2021_simkompetensi";

// Create DB connection
include_once("../includes/config.php");
// Create connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}



function get_content($url){
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Cookie: PHPSESSID=5rdqapq7jd9pk8jc4hnkf1frae'
  ),
));

$response = curl_exec($curl);
curl_close($curl);
return $response;
}

function get_gambar_url($img_address){

$arr_url = explode('/',$img_address);
$file_name = end($arr_url);

//$fp = fopen($file_name, 'wb');
//curl_setopt($ch, CURLOPT_FILE, $fp);
$ch = curl_init($img_address);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, false);
$content = curl_exec($ch);
$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
    if ($code == 200) {
    //tulis file
	    $fp = fopen($file_name, 'wb');
	    fwrite($fp, $content);
	    fclose($fp);
    } 

}

$img_jadesta_path = 'https://jadesta.com/imgpost/';
$json_prov = get_content("https://jadesta.com/getdata/peta?idprov=0");
$arr_provinsi = json_decode($json_prov)->data;
$jml_desta_total = 0;
foreach ($arr_provinsi as $dt_provinsi) {
	$id_prov = $dt_provinsi->id;
	echo '<h1>' . $dt_provinsi->nama . '</h1>';
	$jml_desta_provinsi = 0;
	$json_kabkot = get_content("https://jadesta.com/getdata/peta?idprov=$id_prov");
	$arr_kabkot = json_decode($json_kabkot)->data;

	//iterasi kabkot
	foreach ($arr_kabkot as $dt_kabkot) {
		$id_kabkot = $dt_kabkot->id;
		echo '<h2>' . $dt_kabkot->nama . '</h2>';
		$jml_desta_kabkot = 0;
		//get detil desa wisata
		$json_desta = get_content("https://jadesta.com/getdata/peta?idprov=$id_prov&idkota=$id_kabkot");
		$arr_desta = json_decode($json_desta)->data;
		//echo '<ol>';
		foreach ($arr_desta as $dt_desta) {
			echo '<li>' . $dt_desta->nama . '</li>';
			$id=$dt_desta->id;
			$lat = $dt_desta->latitude;
			$lon = $dt_desta->longitude;
			$nama_desa = addslashes($dt_desta->nama);
			$ket = addslashes(strip_tags($dt_desta->caption));
			$jml_desta_kabkot++;
			$jml_desta_provinsi++;	

			$gambar = $id . '.jpg';
			//insert database
			//echo "insert data <br />";
		$sql = "INSERT INTO ".DB_MARKERS_TABLE."  (title, provinsi,kota,description,lng,lat,marker_type,sumber_data,date_add) VALUES ('$nama_desa','".$dt_provinsi->nama."','".$dt_kabkot->nama."','$ket',  '".$lon."', '".$lat."','78', 'Jadesta',NOW())";

	if($conn->query($sql)){
		echo 'berhasil<br/>';
		$jml_desta_total++;
	} else {
		echo $sql . 'gagal <br />';
	}

			
		}
		//echo '</ol>';
		//echo '<br /> Total Desta di kabkot:' .$jml_desta_kabkot;
	}
	//echo '<br /> Total Desa Wisata di Provinsi: ' . $jml_desta_provinsi;
} 
mysqli_close($conn);
echo '<br /> Total Desa Wisata Total Masuk : ' . $jml_desta_total;

?>