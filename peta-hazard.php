<?php 
$modul = 'peta-hazard';
$sidebar= false;
$title = "Peta Hazard  - Peta Geospasial Pariwisata";
include_once('blocks/header.php');
//include_once(APP_PATH . "/includes/classes/Esri_service.class.php");
$url = "https://services7.arcgis.com/Y24oyWJVNs6VLjiH/arcgis/rest/services/KRB_GA_ID/FeatureServer/0";
//$esri = new Esri_service($url);
$base_map = 'ImageryFirefly';
//$dt_json = $esri->get_json();
//$field = $esri->get_field($dt_json);

?>

 

  <!-- Load Leaflet from CDN-->
   <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
    integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
    crossorigin=""></script>  

  <!-- Load Esri Leaflet from CDN -->
  <script src="https://unpkg.com/esri-leaflet@2.3.0/dist/esri-leaflet.js"
    integrity="sha512-1tScwpjXwwnm6tTva0l0/ZgM3rYNbdyMj5q6RSQMbNX6EUMhYDE3pMRGZaT41zHEvLoWEK7qFEJmZDOoDMU7/Q=="
    crossorigin=""></script>

  <!-- Load Esri Leaflet Renderers from CDN -->
  <script src="https://unpkg.com/esri-leaflet-renderers@2.0.6/dist/esri-leaflet-renderers.js"
    integrity="sha512-mhpdD3igvv7A/84hueuHzV0NIKFHmp2IvWnY5tIdtAHkHF36yySdstEVI11JZCmSY4TCvOkgEoW+zcV/rUfo0A=="
    crossorigin=""></script>

  <style>
    html,
    body,
    #map {
      top: 50px;
      height: 100%;
      width: 100%;
      margin: 0;
      padding: 0;
    }


     #basemaps-wrapper {
    position: absolute;
    top: 50px;
    right: 10px;
    z-index: 400;
    background: white;
    padding: 10px;
  }
  #basemaps {
    margin-bottom: 5px;
  }

  .no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(static/img/loader.gif) center no-repeat #fff;
}
  </style>
</head>
<body>
<div class="se-pre-con"></div>    
    <div id="map"></div>
    <div id="basemaps-wrapper" class="leaflet-bar">
 

 Basemap: <select name="basemaps" id="basemaps" onChange="changeBasemap(basemaps)">
<?php 
$arr_basemap = array('Topographic','Streets','Topographic','NationalGeographic','Oceans','Terrain','ImageryClarity','DarkGray','Gray','ShadedRelief','ImageryClarity','ImageryFirefly');
foreach($arr_basemap as $basemap) {
?> 
    <option value="<?php echo $basemap;?>" <?php if($basemap==$base_map){ echo 'selected'; } ?>><?php echo $basemap;?>
    </option>

<?php } ?>
  </select>
<!-- <div id='dropdown'>
   <select>
   </select>
</div>-->
</div>
    <script>
        const map = L.map('map', {
          center: [-2.6000285,118.015776],
          zoom:5,
          minZoom: 5,
          maxZoom: 17 
        });

        var peta_dasar = L.esri.basemapLayer('<?php echo $base_map;?>').addTo(map);
        const dataHazard = L.esri.featureLayer({url: '<?php echo $url;?>'});
        dataHazard.metadata(function(err, response) {
  if (response) {
        dataHazard.on('loading', iterateFeatures);
        dataHazard.addTo(map);


      dataHazard.bindPopup(function(evt) {
          return L.Util.template('<h3>{NAMOBJ} ({MAG_CODE})</h3> <p> {REMARK}<br/>Metadata: {METADATA}.', evt.feature.properties);
      });
  } else {
        alert('Layer Hazard tidak bisa diakses');
    $(".se-pre-con").fadeOut("slow");
  }
});




      function iterateFeatures () {
        $(".se-pre-con").fadeOut("slow");

        dataHazard.eachFeature(function(layer) {
         // let nama_objek = layer.feature.properties.NAMOBJ;
          //$('#dropdown select').append('<option value='+nama_objek+'>'+nama_objek+'</option>');
          console.log(layer.feature);
        });
      }


      function setBasemap(basemap) {
        if (peta_dasar) {
          map.removeLayer(peta_dasar);
        }

        peta_dasar = L.esri.basemapLayer(basemap);
        map.addLayer(peta_dasar);
      }

      function changeBasemap(basemaps){
        var basemap = basemaps.value;
        setBasemap(basemap);
      }   
    </script>    
</body>
</html>