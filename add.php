<?php

include_once("includes/config.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/exec.php");
$cur_role = $_SESSION['role'];
$arr_allowed = array(1,2);
if(!in_array($cur_role,$arr_allowed)){
    header("Location: index.php");
    exit();
}
$list_tipe = $marker_type->get(3);
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $lang["menu_add_marker"]; ?> | <?php echo $lang["site_title"]; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <script lang="javascript">
        var SITE_DOMAIN = '<?php echo SITE_DOMAIN?>';
        var HTTP_APP_PATH = '<?php echo HTTP_APP_PATH?>';
        var ZOOM = <?php echo $map_settings['config_zoom'] ?>;
        var AUTODETECT_MAP_CENTER = <?php echo $map_settings['config_autodetect_map_center'] ?>;
        var MAP_CENTER_LAT = <?php echo $map_settings['config_map_center_latitude'] ?>;
        var MAP_CENTER_LNG = <?php echo $map_settings['config_map_center_longitude'] ?>;
        var MAP_SETTINGS = <?php echo json_encode($config_marker_types)?>;
        var MAP_STYLE = <?php echo strlen($map_settings['config_map_style'])?$map_settings['config_map_style']:"''";?>;
        var lang = [];
        var MAP_SOURCE = <?php echo $map_settings['config_map_source_id']; ?>;
        <?php if($map_settings['config_map_source_id'] == 1): //mapbox ?>
        var MAPBOX_API_KEY = '<?php echo strlen($map_settings['config_mapbox_api_key'])?$map_settings['config_mapbox_api_key']:"";?>';
        <?php endif;?>
        lang["Describe_Location"] = "<?php echo $lang['Describe_Location']?>";
        lang["Describe_Description"] = "<?php echo $lang['Describe_Description']?>";
        lang["Marker_Type"] = "<?php echo $lang['Marker_Type']?>";
        lang["Latitude"] = "<?php echo $lang['Latitude']?>";
        lang["Longitude"] = "<?php echo $lang['Longitude']?>";
        lang["Move_Marker"] = "<?php echo $lang['Move_Marker']?>";
        lang["Save"] = "<?php echo $lang['Save']?>";
        lang["Saving_data"] = "<?php echo $lang["Saving_data"]?>";
        lang["Upload_Image"] = "<?php echo $lang["Image"]?>";
    </script>
    <link rel="shortcut icon" href="<?php echo HTTP_APP_PATH; ?>/static/img/favicon.ico" type="image/x-icon">
    <title><?php echo $lang["menu_add_marker"]; ?> - <?php echo $lang["site_title"]; ?></title>
    <?php include_once("blocks/scripts.php") ?>
    <link type="text/css" rel="stylesheet" href="<?php echo HTTP_APP_PATH ?>/static/plugins/select2/css/select2.min.css"/>
<style>
    html, body, .container-fluid { height: 100% }
</style>
</head>
<body>


<nav class="navbar navbar-inverse navbar-static-top mb0" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo HTTP_APP_PATH ?>">
                <?php echo $lang["site_title"]; ?>
            </a>
        </div>
        <div class="navbar-offcanvas offcanvas">
            <ul class="nav navbar-nav">
                <?php include_once("blocks/website_menu.php"); ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php include_once("blocks/user_menu.php"); ?>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<div class="container-fluid">
    <div class="row" style="height: 95%">
        <?php if(sizeof($config_marker_types['pinImage'])):?>
        <div class="col-md-8" id="primary" style='min-height: 100%; height: 100%; padding-left: 0px; padding-right: 0px;'>
            <div id="map_canvas" style="width: 100%; height: 100%; min-height: 100%;"></div>
            <div id="message"></div>
            <!-- #content -->
        </div>
        <!-- #primary -->
        <div class="col-md-4" id="list-column">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <?php echo $lang["menu_add_marker"]; ?>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-success hidden" role="alert" id="upload-message"></div>
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add-form"
                              action="<?php echo HTTP_APP_PATH; ?>/exec.php">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title" class="col-sm-3 control-label"><?php echo $lang["Describe_Location"];?>*</label>

                                    <div class="col-sm-8"><input aria-required="" name="title" required="" value="" type="text" class="form-control" id="title" maxlength="255"></div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-sm-3 control-label"><?php echo $lang["Describe_Description"];?></label>

                                    <div class="col-sm-8"><textarea name="description" class="form-control"
                                                                    id="description"/></textarea></div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-sm-3 control-label"><?php echo $lang["Upload_Image"];?></label>

                                    <div class="col-sm-8"><input type="file" name="marker_image"></div>
                                </div>
                                <div class="form-group">
                                    <label for="marker_type" class="col-sm-3 control-label"><?php echo $lang["Marker_Type"];?>*</label>
                                    <div class="col-sm-8">
                                        <select class='form-control' name='marker_type' id='marker_type'>
                                        <?php foreach($list_tipe as $marker):?>
                                            <option value='<?php echo $marker['id'];?>'><?php echo $marker['type_name'];?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group subkategori">
                                    <label for="subkategori" class="col-sm-3 control-label">Subkategori</label>
                                    <div class="col-sm-8">
                                        <select class='form-control' name='subkategori' id='subkategori'>
                                        <option value=''>- Pilih Subkategori -</option>
                                        <?php foreach($get_subkategori as $subkategori):?>
                                            <option value='<?php echo $subkategori['id_subkategori'];?>'><?php echo $subkategori['nama_subkategori'];?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lat" class="col-sm-3 control-label"><?php echo $lang["Latitude"];?></label>

                                    <div class="col-sm-8"><input name="lat" required="" value="" type="text"
                                                                 class="form-control" id="lat"></div>
                                </div>
                                <div class="form-group">
                                    <label for="lng" class="col-sm-3 control-label"><?php echo $lang["Longitude"];?></label>

                                    <div class="col-sm-8"><input name="lng" required="" value="" type="text"
                                                                 class="form-control" id="lng"></div>
                                </div>
                                <div class="form-group">
                                    <label for="provinsi" class="col-sm-3 control-label">Provinsi</label>
                                    <div class="col-sm-8"><input name="provinsi" value="" type="text" class="form-control" id="provinsi"></div>
                                </div>
                                <div class="form-group">
                                    <label for="kota" class="col-sm-3 control-label">Kota</label>
                                    <div class="col-sm-8"><input name="kota" value="" type="text" class="form-control" id="kota"></div>
                                </div>
                                <div class="form-group">
                                    <label for="kecamatan" class="col-sm-3 control-label">Kecamatan</label>
                                    <div class="col-sm-8"><input name="kecamatan" value="" type="text" class="form-control" id="kecamatan"></div>
                                </div>
                                <div class="form-group">
                                    <label for="kelurahan" class="col-sm-3 control-label">Kelurahan</label>
                                    <div class="col-sm-8"><input name="kelurahan" value="" type="text" class="form-control" id="kelurahan"></div>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-8">
                                        <select class='form-control' name='status' id='status'>
                                        <option value=''>- Pilih Status -</option>
                                        <?php foreach($get_status as $status):?>
                                            <option value='<?php echo $status['id_status'];?>'><?php echo $status['status'];?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pengelola" class="col-sm-3 control-label">Pengelola</label>
                                    <div class="col-sm-8"><input name="pengelola" value="" type="text" class="form-control" id="pengelola"></div>
                                </div>
                                <div class="form-group">
                                    <label for="pengelola" class="col-sm-3 control-label">Sumber Data</label>
                                    <div class="col-sm-8"><input name="sumber_data" value="" type="text" class="form-control" id="sumber_data"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-8">
                                       <!-- <input type="button" value="<?php echo $lang["Move_Marker"];?>" class="btn btn-default"
                                               onclick="movemarker();" id="moveMarker"/><br />-->
                                        <button type="reset" value="Reset" class="btn btn-danger">Reset</button>
                                        <input type="hidden" name="action" id="action" value="add-marker">
                                        <input type="button" class="btn btn-primary pull-right" value="<?php echo $lang["Save"];?>"
                                               onclick="saveData();" id="saveBtn"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <?php else:?>
        <h3>You don't have Maker type. Please <a href="<?php echo HTTP_APP_PATH; ?>/marker_type.php">add Marker Type.</a></h3>
        <?php endif;?>
    <!--/div-->
    <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
    <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/js/add-marker-normal.js"></script>
    <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
</body>
</html>
