<?php 
$modul = 'berita';
$sidebar= false;
$title = "RIP  - Peta Geospasial Pariwisata";
include_once('blocks/header.php');
include_once(APP_PATH . "/includes/classes/Berita.class.php");
$path_gambar = HTTP_IMG_PATH . 'berita/';
$berita = new Berita($dbh);
$jml_data = 1;

$last_id = 0;
$this_bulan = date('m');
$this_tahun = date('Y');
if(isset($_POST['bulan'])){
    $this_bulan = (int)$_POST['bulan'];
}
if(isset($_POST['tahun'])){
    $this_tahun = (int)$_POST['tahun'];
}

$list_berita = $berita->get_data($this_bulan,$this_tahun);

?>
<link rel="stylesheet" href="static/plugins/vertical-timeline/vertical-timeline.css">
<div class="container" style="background-color: #e8eff5;padding-top: 51px;">
<h1>Ringkasan Isu Pariwisata</h1>
<div class="text-right">
 <form class="form-inline" action="" method="POST">
  <div class="form-group">
    <label class="sr-only" for="bulan">Bulan:</label>
<select name="bulan" class="form-control">
    <?php foreach ($_arr_bulan as $key => $val){ ?>
    <option value="<?php echo $key;?>" <?php if($key==$this_bulan){ echo 'selected'; }?>><?php echo $val;?></option>
<?php } ?>
</select>
  </div>
  <div class="form-group">
    <label class="sr-only" for="tahun">Tahun:</label>
<select name="tahun" class="form-control">
    <?php 
    $cur_tahun = date('Y');
    for ($i=$cur_tahun;$i>$cur_tahun-10;$i--){ ?>
    <option value="<?php echo $i;?>" <?php if($i==$this_tahun){ echo 'selected'; }?>><?php echo $i;?></option>
<?php } ?>
</select>
  </div>

  <button type="submit" class="btn btn-info">Tampilkan</button>
</form> 
</div>
<?php if(($cur_role=='1') || ($cur_role=='2') ){?>
<a class="btn btn-success" id="add_information" href="berita-add.php" > 
<span class="glyphicon glyphicon-plus-sign"></span>  Input berita  </a>
<?php } 
if(count($list_berita)>0) {
 ?>


<div id="vt1">
	<?php $i=0; foreach($list_berita as $isi_berita){ 
        $pengirim = $isi_berita['pengirim'];
        ?>
    <div <?php if($i==0) { echo 'data-vtside="left"';}?> data-vtdate="<?php echo $isi_berita['waktu'] . '<br />'.$pengirim ;?>" style="padding:1.25em;background-color: #fff;">
        <?php if($_SESSION['role']==1 || $_SESSION['username'] == $pengirim ){?>
        <a href="berita-add.php?act=edit&id=<?php echo $isi_berita['id_berita'];?>" title="Edit Berita"><span class="glyphicon glyphicon-pencil"></span>
        </a>
        <a onclick="return confirm('Anda Yakin menghapus berita ini?');" 
         href="exec.php?action=delete-berita&id=<?php echo $isi_berita['id_berita'];?>" title="Hapus Berita">
          <span class="glyphicon glyphicon-remove"></span>
        </a>
    <?php } ?>
        <h3><?php 
 $gambar = $isi_berita['gambar'];
if(!empty($gambar)) {
    echo '<img src="'.$path_gambar.$gambar.'"" width="200px" class="img-responsive" />';
}
        echo $isi_berita['judul'];?></h3>
<?php echo $isi_berita['isi'];?>
    </div>
    <?php 
    $last_id = $isi_berita['id_berita'];
    $i++;} ?>
</div><!-- End vt1 -->
 <?php } else { ?>
 
<div class="panel panel-default" style="margin-top: 50px">
  <div class="panel-body text-center"><h2><span class="label label-warning">- Belum ada RIP untuk bulan ini - </span></h2></div>
</div>

 <?php } ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="static/plugins/vertical-timeline/vertical-timeline.js"></script>
<script>
    $('#vt1').verticalTimeline({
        animate: 'slide'
    });

    
</script>
 </body>
</html>