<?php 

$modul = 'berita';
$sidebar= false;
$title = "RIP  - Peta Geospasial Pariwisata";

include_once('blocks/header.php');
  if(($cur_role!='1') && ($cur_role!='2') ){
    header("location: index.php");
    exit();
    }
include_once(APP_PATH . "/includes/classes/Berita.class.php");
$obj_berita = new Berita($dbh);
$tbl_berita = $obj_berita->get_tbl_name();
$path_gambar = HTTP_IMG_PATH . 'berita/';
$act = 'tambah-berita';
$str_simpan = 'Simpan';
$class_simpan = 'btn-primary';
$judul = '';
$isi = '';
$gambar  = '';
$tgl_berita = '';
//jika edit
if($_GET['act'] == 'edit'){
    $id_berita = $_GET['id'];
    if(is_numeric($id_berita)){
           $berita = $obj_berita->get_by_id($id_berita);
           $judul = $berita['judul'];
           $isi = $berita['isi'];
           $gambar = $berita['gambar'];
           $str_simpan = 'Update';
           $class_simpan = 'btn-warning';
           $act = 'edit-berita';
           $tgl_berita =  date('d-m-Y',strtotime($berita['waktu']));
           $_SESSION['id_edit'] = $id_berita;
    } else {
        header("Location: index.php");
    }

}


if(isset($_POST['act'])){
    $act= $_POST['act'];
        $tgl_berita = $_POST['tgl_berita'];
        $waktu=strtotime($tgl_berita); 
        $waktu=date("Y-m-d",$waktu);
    if($act=='tambah-berita'){
        //kirim gambar
        include_once(APP_PATH . "/includes/classes/Upload.class.php");
        $handle = new upload($_FILES['gambar_info']);
        $gambar_upload = '';
        if ($handle->uploaded) {
            $nama_baru = time();
            $handle->file_new_name_body   = $nama_baru;
            $handle->image_convert = 'jpg';
            $handle->image_resize         = true;
            $handle->image_x              = 450;
            $handle->image_ratio_y        = true;
            $handle->process(APP_PATH.'/static/img/upload/berita/');
            if ($handle->processed) {
                $gambar_upload = $nama_baru.'.jpg';
                $handle->clean();
            } else {
                echo 'error : ' . $handle->error;
            }
        }

        $sql = "INSERT INTO ".$tbl_berita."(`waktu`,`pengirim`, `judul`, `isi`, `gambar`) VALUES (:waktu,:user,:title, :description,:gambar_upload) ";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':waktu', $waktu);
            $stmt->bindParam(':title', $_POST['judul']);
            $stmt->bindParam(':user', $_SESSION['username']);
            $stmt->bindParam(':description', $_POST['deskripsi']);
            $stmt->bindParam(':gambar_upload', $gambar_upload);
            if($stmt->execute()){
                $user_obj->insert_log($_SESSION['username'],'Tambah berita',SITE_DOMAIN . 'berita.php');
            }
            echo '<script>window.location.href = "berita.php";</script>';
            exit();
    } elseif($act=='edit-berita'){
        include_once(APP_PATH . "/includes/classes/Upload.class.php");
        $handle = new upload($_FILES['gambar_info']);
        $gambar_upload = '';
        if ($handle->uploaded) {
            $nama_baru = time();
            $handle->file_new_name_body   = $nama_baru;
            $handle->image_convert = 'jpg';
            $handle->image_resize         = true;
            $handle->image_x              = 450;
            $handle->image_ratio_y        = true;
            $handle->process(APP_PATH.'/static/img/upload/berita/');
            if ($handle->processed) {
                $gambar_upload = $nama_baru.'.jpg';
                $handle->clean();
            } else {
                echo 'error : ' . $handle->error;
            }
        }
        $str_gambar = '';
        if(!empty($gambar_upload)){
            $str_gambar = ", `gambar` = :gambar_upload";
        }
        $sql = "UPDATE ".$tbl_berita." SET  `judul` = :title , `isi` = :description, waktu=:waktu $str_gambar  WHERE id_berita = :id_berita ";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':title', $_POST['judul']);
            $stmt->bindParam(':description', $_POST['deskripsi']);
            $stmt->bindParam(':waktu', $waktu);
            if(!empty($gambar_upload)){
                $stmt->bindParam(':gambar_upload', $gambar_upload);
            }
            $stmt->bindParam(':id_berita', $_SESSION['id_edit']);
            if($stmt->execute()){
                $user_obj->insert_log($_SESSION['username'],'Edit berita '. $_POST['judul'],SITE_DOMAIN . 'berita.php');
            }
            echo '<script>window.location.href = "berita.php";</script>';
            exit();
    }
}
?>

<div class="container-fluid">
<div class="row">

<form class="form-horizontal" method="post" id="update-form" enctype="multipart/form-data" action="">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="judul" class="col-sm-3 control-label">Judul *</label>
                                    <div class="col-sm-8">
                                        <input name="judul" required="" value="<?php echo $judul;?>" type="text" class="form-control" id="judul" >
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="tgl_berita" class="col-sm-3 control-label">Tanggal RIP</label>
                                    <div class="col-sm-8">
                                    <input class="datepicker"   name="tgl_berita" data-date-format="dd-mm-yy" value="<?php echo  $tgl_berita;?>">
                                    </div>
                                </div>                              
                                <div class="form-group">
                                    <label for="deskripsi" class="col-sm-3 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea name="deskripsi" id='deskripsi' class="form-control" rows="8"><?php echo $isi;?></textarea>
                                    </div>
                                </div>
                                <?php if(!empty($gambar)){ 
                                    $str_gambar = '<img src="'.$path_gambar.$gambar.'"" width="200px" class="img-responsive" />';
                                    ?>
                                 <div class="form-group">
                                <label for="gambar_saat_ini" class="col-sm-3 control-label"></label>
                                    <div class="col-sm-8">
                                        <?php echo $str_gambar;?>
                                    </div>
                                 </div>       

                                <?php }?>
                                <div class="form-group">
                                    <label for="gambar_info" class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-8">
                                        <span id="inputMarkerImage" style="float: left"></span>
                                        <input type="file" name="gambar_info">
                                    </div>
                                </div>

                                
                            </div>
                            <div class="modal-footer">
                                <input type='hidden' name='act' id='act' value='<?php echo $act;?>'>
                                <button type="submit"  class="btn <?php echo $class_simpan;?>"><?php echo $str_simpan;?></button>
                            </div>
                        </form>
</div>
<script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
 <script>
            $(document).ready(function () {

                var editor = CKEDITOR.instances.deskripsi;
                if (editor) {
                    editor.destroy(true);
                }
                CKEDITOR.replace( 'deskripsi' );

                $('.datepicker').datepicker({
        dateFormat: 'dd-mm-yy'});
             }

             );  
</script>