<?php 
error_reporting(0);
$url_layer = '';
$pilihan  = '';
$base_map = 'OpenStreetMap';
if(isset($_POST['btn_submit'])){
$pilihan = $_POST['select_layer'];
$base_map = $_POST['basemaps'];
}

$tipe = 1;
switch ($pilihan) {
    case 1:
        $url_layer = "http://inarisk.bnpb.go.id:6080/arcgis/rest/services/inaRISK/INDEKS_BAHAYA_BANJIR_BANDANG/ImageServer";
        break;
    case 2:
        $url_layer = "http://inarisk.bnpb.go.id:6080/arcgis/rest/services/inaRISK/layer_bahaya_banjir/ImageServer";
            break;
    case 3:
        $url_layer = "http://inarisk.bnpb.go.id:6080/arcgis/rest/services/inaRISK/INDEKS_BAHAYA_GEMPABUMI/ImageServer";
              break;
    case 4:
        $url_layer = "http://inarisk.bnpb.go.id:6080/arcgis/rest/services/inaRISK/INDEKS_BAHAYA_LETUSAN_GUNUNG_API/ImageServer";
        break;
    case 5:
        $url_layer = "http://inarisk.bnpb.go.id:6080/arcgis/rest/services/inaRISK/INDEKS_BAHAYA_TSUNAMI/ImageServer";
        break;
    case 6:
        $url_layer = "http://inarisk.bnpb.go.id:6080/arcgis/rest/services/inaRISK/layer_bahaya_gempabumi/ImageServer";
        break;
    case 7: {
        $url_layer = "http://sigi.pu.go.id:6080/arcgis/rest/services/BENCANA/MapServer";
        $tipe = '2';
        break;
        
      }
}
$sidebar = false;
$title = "Peta Integrasi - Aplikasi Geospasial Pariwisata";
include_once('blocks/header.php');
?>

 <!-- Load Leaflet from CDN-->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
    integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
    crossorigin=""></script>  

  <!-- Load Esri Leaflet from CDN -->
  <script src="//unpkg.com/esri-leaflet@2.2.4/dist/esri-leaflet.js"
    integrity="sha512-tyPum7h2h36X52O2gz+Pe8z/3l+Y9S1yEUscbVs5r5aEY5dFmP1WWRY/WLLElnFHa+k1JBQZSCDGwEAnm2IxAQ=="
    crossorigin=""></script>   

    <link rel="stylesheet" href="https://unpkg.com/mapbox-gl/dist/mapbox-gl.css"/>
<script src="https://unpkg.com/mapbox-gl/dist/mapbox-gl.js"></script>
<!-- load the latest release from the cdn automatically -->
<script src="https://unpkg.com/esri-leaflet-vector/dist/esri-leaflet-vector-debug.js"></script>

 <script src="static/js/leaflet-esri/esri-leaflet-legend-compat-src.js"></script>
  <style>
    html,
    body,
    #map {
      height: 100%;
      width: 100%;
      margin: 0;
      padding: 0;
    }

     #basemaps-wrapper {
    position: absolute;
    top: 50px;
    right: 10px;
    z-index: 400;
    background: white;
    padding: 10px;
  }
  #basemaps {
    margin-bottom: 5px;
  }


.leaflet-legend-control {
    background: white;
    padding: 1em;
    max-height: 300px;
    overflow: auto;
    font: 12px/1.5 "Helvetica Neue", Arial, Helvetica, sans-serif;
}

.leaflet-legend-control ul {
    padding: 0;
    margin: 0;
    list-style-type: none;
}

.leaflet-legend-control ul li img {
    display: inline-block;
    margin-right: 5px;
}

.leaflet-legend-control ul li span {
    vertical-align: top;
    line-height: 22px;
}

.leaflet-legend-control ul ul {
    margin-left: 15px;
}
  </style>
      <div class="container-fluid">
    <div id="map" class="google-map-canvas"></div>
          
    <div id="basemaps-wrapper" class="leaflet-bar">
   <form method="post">     Layer:   <select id="select_layer" name="select_layer" >
      <option value="" <?php if($pilihan == 1){ echo 'selected' ;}?>>Pilih Layer</option>
  <option value="1" <?php if($pilihan == 1){ echo 'selected' ;}?>>INDEKS BAHAYA BANJIR BANDANG</option>
  <option value="2" <?php if($pilihan == 2){ echo 'selected' ;}?>>INDEKS BAHAYA BANJIR </option>
  <option value="3" <?php if($pilihan == 3){ echo 'selected' ;}?>>INDEKS BAHAYA GEMPABUMI</option>
  <option value="4" <?php if($pilihan == 4){ echo 'selected' ;}?>>INDEKS BAHAYA LETUSAN GUNUNG API</option>
  <option value="5" <?php if($pilihan == 5){ echo 'selected' ;}?>>INDEKS BAHAYA TSUNAMI</option>
  <option value="6" <?php if($pilihan == 6){ echo 'selected' ;}?>> BAHAYA Gempa bumi</option> 
  <option value="7" <?php if($pilihan == 7){ echo 'selected' ;}?>> Bencana PU</option> 
</select>

 Basemap: <select name="basemaps" id="basemaps" onChange="changeBasemap(basemaps)">
<?php 
$arr_basemap = array('OpenStreetMap','Newspaper','Topographic','Navigation','Streets','StreetsNight','StreetsRelief','DarkGray','Gray','ShadedRelief');
foreach($arr_basemap as $basemap) {
?> 
    <option value="<?php echo $basemap;?>" <?php if($basemap==$base_map){ echo 'selected'; } ?>><?php echo $basemap;?>
    </option>

<?php } ?>
  </select>
 <input type="submit" name="btn_submit" value="Tampilkan">
</form>
</div>
</div>
<script>
  var map = L.map('map').setView([-2.6000285,118.015776], 5);
  var layer = L.esri.Vector.basemap('<?php echo $base_map;?>').addTo(map);

  function setBasemap(basemap) {
    if (layer) {
      map.removeLayer(layer);
    }

    layer = L.esri.Vector.basemap(basemap);
    map.addLayer(layer);
  }

  function changeBasemap(basemaps){
    var basemap = basemaps.value;
    setBasemap(basemap);
  }


<?php if(!empty($url_layer) && $tipe=='2') { ?>
var bencana = L.esri.dynamicMapLayer({
    url: '<?php echo $url_layer;?>',
    useCors: false
  }).addTo(map);

L.esri.legendControl(bencana).addTo(map);
  bencana.bindPopup(function (error, featureCollection) {
    if(error || featureCollection.features.length === 0) {
      return false;
    } else {
            console.log(featureCollection.length);
      return featureCollection.features[0].properties.BENCANA + '<br />' + featureCollection.features[0].properties.KABUPATEN + '<br />'+ featureCollection.features[0].properties.TANGGAL;

    }
  });
<?php } else { ?>
      L.esri.imageMapLayer({
      url: '<?php echo $url_layer;?>',
      attribution: 'BNPB'
    }).addTo(map);
<?php }?>


/*
var marker = L.marker([-6.296849, 105.840235]).addTo(map);
marker.bindPopup('<b>Pantai Carita!</b><br><img src="img/carita.jpg" style="max-width:100px;" />','{maxWidth:300px;}');

var marker = L.marker([-8.058313, 114.243872]).addTo(map);
marker.bindPopup('<b>Kawah Ijen!</b><br><iframe  width="560" height="315" src="https://www.youtube.com/embed/3C5g7CCjUQk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>"',{maxWidth:575});

var marker = L.marker([-6.700933, 105.549025]).addTo(map);
marker.bindPopup('<b>Ciputih!</b><br><a href="http://localhost/2019/kemenpar/resort_ciputih/" target="_blank"><img src="img/360-squeeze.jpg" width="200px"/></a>"',{maxWidth:575});

var marker = L.marker([-8.354739, 116.270641]).addTo(map);
marker.bindPopup('<b>Lombok Utara!</b><p>Laporan Tourism Crisis Center (TCC)<br />Gempa Lombok<br />Minggu, 17 Maret pukul 15.07 WITA</p><p><br /><strong>3A TERDAMPAK</strong></p><p>Lombok Utara</p><p><strong>a. Atraksi</strong><br />Daerah tujuan wisata di kab. Lombok Utara Air terjun Desa Senaru Bayan yang terdampak: Air terjun Tiu Kelep dan Sendang Gile.</p><p><strong>b. Aksesibilitas</strong></p><p>Udara: bandara internasional Zainuddin Abdul Madjid Lombok<br />masih normal.</p><p>Laut:<br />- Dermaga Pelabuhan Kayangan<br />- Dermaga Gili Mas<br />- Dermaga Labuhan Haji<br />Masih berjalan normal</p><p>Darat: &nbsp;<br />Terjadi longsor di wilayah ini sehingga terdampak retakan pada jalan setapak menuju destinasi air terjun Tiu Kelep dan Sendang Gile.</p><p><strong>c. Amenitas</strong></p><p>Akomodasi<br />- Penginapan di sekitar tidak terdampak.</p><p>Note:<br />Info wisatawan yang berkunjung ke air terjun Tiu Kelep dan Sendang Gile Desa senaru Bayan, Kab. Lombok Utara sebanyak 40 orang, 12 orang menuju air terjun Tiu Kelep dan sebagian hanya sampai di air terjun Sendang Gile. Jarak tempuh dari Sendang Gile ke Tiu Kelep 1 jam melewati sungai dan hutan.</p><p></p><p><strong>Source</strong>: KaDispar Prov/Kab/kota NTB<br />As of minggu 17 maret 2019 pukul 19.12</p>"',{maxWidth:575,maxHeight:300});*/


</script>

</body>
</html>