<?php
$modul = 'objek_detil';
$sidebar= false;
$title = "Detil Bencana  - Peta Geospasial Pariwisata";
include_once('blocks/header.php');
include_once(APP_PATH . "/includes/classes/pagination/pagination.class.php");
$cur_role = 0;
if(isset($_SESSION['role'])){
$cur_role = $_SESSION['role'];
}

$pagination = (new Pagination());

$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? (int) $_GET["page"] : 1;

$input_search_text = $search_text = isset($_GET['search_text']) ? trim($_GET['search_text']) : '';

//ekraf
$jns_ekraf = isset($_GET['jns_ekraf']) ? trim($_GET['jns_ekraf']) : '';
$arr_jenis_ekraf = array();
if(!empty($jns_ekraf)){
$arr_jenis_ekraf[0] = $jns_ekraf;
}

//objek wisata
$jns_objek = isset($_GET['jns_objek']) ? trim($_GET['jns_objek']) : '';
$arr_jenis = array();
if(!empty($jns_objek)){
$arr_jenis[0] = $jns_objek;
}
//bencana
$jns_bencana = isset($_GET['jns_bencana']) ? trim($_GET['jns_bencana']) : '';
$arr_jenis_bencana = array();
if(!empty($jns_bencana)){
$arr_jenis_bencana[0] = $jns_bencana;
}

//record per Page($per_page)
$markers_per_page = isset($_GET['markers_per_page']) && in_array((int) $_GET['markers_per_page'], $config_markers_per_page) ? $_GET['markers_per_page'] : $config_markers_per_page[0];
$offset = $markers_per_page * ($current_page - 1);

//lokasi sekitar
$id_objek = $_GET['id'];
if(is_numeric($id_objek)){
    $_SESSION['id_objek'] = $id_objek;
} else {
    header("Location:index.php");
}
$ekraf->setId((int)$id_objek);
$ekraf_info = $ekraf->getById();
$jenis_ekraf = $ekraf_info['marker_type'];
$get_marker_type = $marker_type->getById($jenis_ekraf);
$nama_jenis = $get_marker_type['type_name'];
$list_ekraf = $marker_type->get(false,true);
$list_wisata = $marker_type->get(3);
$list_bencana = $marker_type->get(true);
$lat=$ekraf_info['lat'];
$lng = $ekraf_info['lng'];
$ekraf_data = $ekraf->get_ekraf_around($lat,$lng,$markers_per_page, $offset, $search_text,$arr_jenis_ekraf);
$objek_data = $marker->get_markers_around($lat,$lng,$markers_per_page, $offset, $search_text,$arr_jenis);
$bencana_data = $bencana->get_list_around($lat,$lng,$markers_per_page, $offset, $search_text, $arr_jenis_bencana);
$image_url = '';
if($ekraf_info['image_type']) {
    $image_url = HTTP_EKRAF_PATH . ($ekraf_info['id_ekraf'] % 10) . '/' . $ekraf_info['id_ekraf'] . '.' . $ekraf_info['image_type'];
}
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <h1 class="page-header"><?php echo $ekraf_info['title'];?></h1>
            <div class="row"></div><!-- /.row -->
            <div class="row">
                <div class="col-md-7">
                    <div id="map-canvas" class="" style="width: 100%"></div>
                    <?php if($image_url) { ?>
                    <img src="<?php echo $image_url; ?>" style="margin-top:1em;" class="img-responsive" alt="Responsive image">
                    <?php } ?>
                </div>
                <div class="col-md-5">
                    <div class="property-list">
                        <dl>                            
                            <dt>Jenis Ekraf </dt><dd><span class="label label-success"> <?php echo !empty($nama_jenis) ? $nama_jenis : '-';?> </span></dd>
                            <dt>Provinsi</dt><dd><span class="label label-success"> <?php echo !empty($ekraf_info['provinsi']) ? $ekraf_info['provinsi'] : '-';?> </span></dd>
                            <dt>Kota </dt><dd><span class="label label-success"> <?php echo !empty($ekraf_info['kota']) ? $ekraf_info['kota'] : '-';?></span></dd>
                            <dt>Kecamatan </dt><dd><span class="label label-success"> <?php echo !empty($ekraf_info['kecamatan']) ? $ekraf_info['kecamatan'] : '-';?></span></dd>
                            <dt>Kelurahan </dt><dd><span class="label label-success"> <?php echo !empty($ekraf_info['kelurahan']) ? $ekraf_info['kelurahan'] : '-';?></span></dd>
                            <dt>Deskripsi</dt><dd><p class="text"><?php echo nl2br($ekraf_info['description']);?></p> </dd>
                        </dl>
                    </div><!-- /.property-list --> 
                </div>
            </div><!-- /.row -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-lg-4" id="list-column">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item <?php echo !empty($jns_ekraf) || (empty($jns_bencana) && empty($jns_objek)) ? 'active' : ''; ?>">
                                <a class="nav-link" id="ekraf-tab" data-toggle="tab" href="#ekraf" role="tab" aria-controls="ekraf" aria-selected="false">Ekraf Sekitar</a>
                            </li>
                            <li class="nav-item <?php echo !empty($jns_objek) ? 'active' : ''; ?>">
                                <a class="nav-link active" id="objek-tab" data-toggle="tab" href="#objek" role="tab" aria-controls="objek" aria-selected="true">Objek Sekitar</a>
                            </li>
                            <li class="nav-item <?php echo !empty($jns_bencana) ? 'active' : ''; ?>">
                                <a class="nav-link" id="bencana-tab" data-toggle="tab" href="#bencana" role="tab" aria-controls="bencana" aria-selected="false">Bencana Sekitar</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade <?php echo !empty($jns_ekraf) || (empty($jns_bencana) && empty($jns_objek)) ? 'active in' : ''; ?>" id="ekraf" role="tabpanel" aria-labelledby="ekraf-tab">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>ekraf-detil.php">
                                        <input type="hidden" name="id" value="<?php echo $id_objek; ?>">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control" name="jns_ekraf" id="jns_ekraf" >
                                                    <option value="">- SEMUA -</option>
                                                    <?php foreach ($list_ekraf as $tipe): ?>
                                                        <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_ekraf) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>ekraf-detil.php?id=<?php echo $id_objek; ?>'" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul id="markers-list" class="list-group">
                                <?php if(isset($ekraf_data['rows'])) { foreach ($ekraf_data['rows'] as $data){?>
                                <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>ekraf-detil.php?id=<?php echo $data['id_ekraf']; ?>'" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["title"] . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                                <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li>
                            </ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                        <div class="tab-pane fade <?php echo !empty($jns_objek) ? 'active in' : ''; ?>" id="objek" role="tabpanel" aria-labelledby="objek-tab">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>ekraf-detil.php">
                                        <input type="hidden" name="id" value="<?php echo $id_objek; ?>">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control" name="jns_objek" id="jns_objek" >
                                                    <option value="">- SEMUA -</option>
                                                    <?php foreach ($list_wisata as $tipe): ?>
                                                        <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_objek) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>ekraf-detil.php?id=<?php echo $id_objek; ?>'" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul id="markers-list" class="list-group">
                                <?php if(isset($objek_data['rows'])) { foreach ($objek_data['rows'] as $data){?>
                                <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>objek-detil.php?id=<?php echo $data['id']; ?>'" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["title"] . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                                <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li>
                            </ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                        <div class="tab-pane fade <?php echo !empty($jns_bencana) ? 'active in' : ''; ?>" id="bencana" role="tabpanel" aria-labelledby="bencana-tab">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>ekraf-detil.php">
                                        <input type="hidden" name="id" value="<?php echo $id_objek; ?>">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control" name="jns_bencana" id="jns_bencana" >
                                                    <option value="">- SEMUA -</option>
                                                    <?php foreach ($list_bencana as $tipe): ?>
                                                        <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_bencana) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>ekraf-detil.php?id=<?php echo $id_objek; ?>'" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul id="markers-list" class="list-group">
                                <?php if(isset($bencana_data['rows'])) { foreach ($bencana_data['rows'] as $data){ 
                                        $waktu = new DateTime($data["waktu"]);
                                        $waktu = $waktu->format("d M Y ");
                                    ?>
                                <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>bencana-detil.php?id=<?php echo $data['id_bencana']; ?>'" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["kejadian"] . ', ' . $data['nkab'] . ', ' . $data['nprop'] . ', ' . $waktu . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                                <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li>
                            </ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.sidebar -->
    </div>
</div>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/js/update-marker-normal.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
             $("#add_information").click(function () {
                var editor = CKEDITOR.instances.deskripsi;
                if (editor) {
                    editor.destroy(true);
                }
                CKEDITOR.replace( 'deskripsi' );
                 $('#myModal').modal('show');
             }) ;  

             var mymap = L.map('map-canvas').setView([<?php echo $ekraf_info['lat'];?>, <?php echo $ekraf_info['lng'];?>], 13);
             var marker = L.marker([<?php echo $ekraf_info['lat'];?>, <?php echo $ekraf_info['lng'];?>]).addTo(mymap);
var circle = L.circle([<?php echo $ekraf_info['lat'];?>, <?php echo $ekraf_info['lng'];?>], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 15,
                minZoom: 7,
                attribution: 'attribution'
            }).addTo(mymap);
         });
        </script>
        <?php //include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>