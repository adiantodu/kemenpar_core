<?php 
$sidebar= false;
$title = "E-Katalog - Peta Geospasial Pariwisata";
include_once('blocks/header.php');
?>
<style>
 /* highlight col-* */
.row [class*='col-'] {
  text-align: center;
  background-clip: content-box;
  min-height: 130px;
  margin-bottom: 15px;
}

@media (max-width: 768px) {
   h2 { font-size: 3vw;}
}
</style>

<div class="container"  style="background-color: #e8eff5;padding-top: 100px;">
    <div class="row" >
        <div class="col-xs-6 col-md-4" style="background-color: #5133AB">
        <a href="publik/katalog/EKATALOG-SUMUT.pdf" target="=blank" style="color:#fff;">
            <img src="publik/katalog/cover-sumut.jpg" style="width:80%;" />
            <h2>Sumatera Utara</h2>
        </a>
        </div>
        <div class="col-xs-6 col-md-4" style="background-color: #AC193D" >
        <a href="publik/katalog/EKATALOG-JATENG.pdf" target="=blank" style="color:#fff;">
        <img src="publik/katalog/cover-jateng.jpg" style="width:80%;" />
            <h2>Jawa Tengah</h2>
        </a>
        </div>
        <div class="col-xs-6 col-md-4" style="background-color: #DC572E" >
        <a href="publik/katalog/EKATALOG-BALI.pdf" target="=blank" style="color:#fff;">
           <img src="publik/katalog/cover-bali.jpg" style="width:80%;" />
            <h2>Bali</h2>
        </a>
        </div>

        <div class="col-xs-6 col-md-4" style="background-color: #0394fc">
        <a href="publik/katalog/EKATALOG-NTB.pdf" target="=blank" style="color:#fff;">
            <img src="publik/katalog/cover-ntb.jpg" style="width:80%;" />
            <h2>NTB</h2>
        </a>
        </div>
        <div class="col-xs-6 col-md-4" style="background-color: #F7046D">
        <a href="publik/katalog/EKATALOG-NTT.pdf" target="=blank" style="color:#fff;">
            <img src="publik/katalog/cover-ntt.jpg" style="width:80%;" />
            <h2>NTT</h2>
        </a>
        </div>
        <div class="col-xs-6 col-md-4" style="background-color: #00A600">
        <a href="publik/katalog/EKATALOG-SULUT.pdf" target="=blank" style="color:#fff;">
            <img src="publik/katalog/cover-sulut.jpg" style="width:80%;" />
            <h2 >Sulawesi Utara</h2>
        </a>
        </div>
    </div>
</div>
        <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>