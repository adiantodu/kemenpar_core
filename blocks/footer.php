 <script>

$(document).ready(function(){


function playAudio() { 
  var audio = new Audio('quarrel.mp3');
audio.play(); 
} 

 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"fetch-notif.php",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('#content-notif').html(data.notification);
    if(data.unseen_notification > 0)
    {
        playAudio();
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
  $(document).on('click', '#dropdown-notif', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();
 }, 6000);
 
});
</script>
<hr>
<div id="footer" class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
       Kementerian Pariwisata &copy; <?php echo date('Y');?>
    </div>
</div>
