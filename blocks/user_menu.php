

<ul class="nav navbar-nav navbar-right">
<?php if(Auth::is_loggedin()):?>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-user"></i> <?php echo $_SESSION['username'];?> <span class="caret"></span></a>
        <ul class="dropdown-menu">
<?php if($cur_role<3) { ?>
            <li ><a href="<?php echo HTTP_APP_PATH ?>add.php"><i class="glyphicon glyphicon-plus"></i><?php echo $lang["menu_add_marker"]; ?></a></li>
            <li><a href="<?php echo HTTP_APP_PATH ?>manage.php"><i class="glyphicon glyphicon-map-marker"></i> <?php echo $lang["menu_manage_marker"]; ?></a></li>
            <li><a href="<?php echo HTTP_APP_PATH ?>ekraf-list.php"><i class="glyphicon glyphicon-briefcase"></i> <?php echo $lang["menu_manage_ekraf"]; ?></a></li>

<?php } if($cur_role=='1') { ?>
           <li><a href="<?php echo HTTP_APP_PATH ?>marker_type.php"><i class="glyphicon glyphicon-record"></i> <?php echo $lang["menu_marker_type"]; ?></a></li>
           <li><a href="<?php echo HTTP_APP_PATH ?>user.php"><i class="glyphicon glyphicon-user"></i> Pengguna</a></li>
           <li><a href="<?php echo HTTP_APP_PATH ?>user-log.php"><i class="glyphicon glyphicon-list-alt"></i> Log Pengguna</a></li>
          <!--   <li><a href="<?php echo HTTP_APP_PATH ?>import.php"><i class="glyphicon glyphicon-import"></i> <?php echo $lang["menu_import"]?></a></li>-->

            <li><a href="<?php echo HTTP_APP_PATH ?>settings.php"><i class="glyphicon glyphicon-cog"></i> <?php echo $lang["menu_settings"]?></a></li>
<?php } ?>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo HTTP_APP_PATH ?>user-changep.php"><i class="    glyphicon glyphicon-edit"></i> <?php echo $lang['change_password'];?></a></li>
            <li><a href="<?php echo HTTP_APP_PATH ?>exec.php?action=logout"><i class="glyphicon glyphicon-log-out"></i> <?php echo $lang['logout'];?></a></li>
        </ul>
    </li>
<?php else: ?>
    <li <?php if(basename($_SERVER["SCRIPT_FILENAME"]) == "login.php"): echo "class=active"; endif;?>>
        <a href="<?php echo HTTP_APP_PATH ?>login.php"><?php echo $lang["login"];?></a></li>
<?php endif; ?>
</ul>

