<?php if(!isset($modul)){
	$modul = '';
}?>
<li <?php if(basename($_SERVER["SCRIPT_FILENAME"]) == "bencana-list.php"): echo "class=active"; endif;?>><a href="<?php echo HTTP_APP_PATH ?>bencana-list.php"><?php echo $lang["menu_bencana_list"]; ?></a></li>
<li <?php if(basename($_SERVER["SCRIPT_FILENAME"]) == "ekatalog.php"): echo "class=active"; endif;?>><a href="<?php echo HTTP_APP_PATH ?>ekatalog.php">e-Katalog</a></li>
<li <?php if($modul == "berita"): echo "class=active"; endif;?>><a href="<?php echo HTTP_APP_PATH ?>berita.php">Informasi</a></li>
<li <?php if(basename($_SERVER["SCRIPT_FILENAME"]) == "twitter.php"): echo "class=active"; endif;?>><a href="<?php echo HTTP_APP_PATH ?>twitter.php">Tweet</a></li>
<li <?php if(basename($_SERVER["SCRIPT_FILENAME"]) == "data-stat.php"): echo "class=active"; endif;?>><a href="<?php echo HTTP_APP_PATH ?>data-stat.php">Rekap Data</a></li>

<img src="static/img/visit.png" alt="" class="logo-navbar" style="height:42px;padding-top:2px;" >


<?php
if(isset($menu_pages) && is_array($menu_pages)):
    foreach($menu_pages as $menu_page):
        ?>
        <li <?php if($menu_page["id"] == $page_id): echo "class=active"; endif;?> ><a href="<?php echo HTTP_APP_PATH ?>page.php?page_id=<?php echo $menu_page["id"];?>"><?php echo $menu_page["title_menu"]; ?></a></li>
    <?php
    endforeach;
endif;
?>