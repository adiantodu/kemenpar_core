<?php
include_once("includes/config.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/exec.php");
if(isset($_SESSION['role'])){
    $cur_role = $_SESSION['role'];
}
$check_settings = Helper::check_settings($default_map_settings, $map_settings);
if((!$map_settings && !is_array($map_settings)) || !$check_settings) {
    echo "You should specify <a href='" . HTTP_APP_PATH . "settings.php" . "'>Map settings</a>.";
    exit;
}
$map_column = 9;
$num_columns = 7;
if (!$map_settings['config_show_makers_filter']) {
    $num_columns += 2;
}
if (!$map_settings['config_show_makers_search']) {
    $num_columns += 3;
    $map_column = 12;
}


?>
<!DOCTYPE html>
<html lang="en" class="notranslate" translate="no">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Aplikasi <?php echo $lang["site_title"]; ?></title>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="static/img/favicon.png">
        <?php if(isset($marker_info)):?>
        <meta property="og:title" content="<?php echo $marker_info['title'];?>" />
        <?php if($marker_info['image_type'] != ""):?>
        <meta property="og:image" content="<?php echo HTTP_IMG_PATH . ($marker_info['id'] % 10) . '/' . $marker_info['id'] . '.' . $marker_info['image_type']?>" />
        <meta name="twitter:image" content="<?php echo HTTP_IMG_PATH . ($marker_info['id'] % 10) . '/' . $marker_info['id'] . '.' . $marker_info['image_type']?>">
        <?php endif;?>
        <meta property="og:description" content="<?php echo $marker_info['short_description'];?>" />
        <meta property="og:url" content="<?php echo HTTP_APP_PATH ?>?action=marker&id=<?php echo $marker_info['id'];?>" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="<?php echo $marker_info['title'];?>" />
        <?php endif;?>
        <?php include_once("blocks/scripts.php")?>
        <?php if(isset($_GET['iframe'])):?>
            <style>
                .container {
                    width: 100%;
                    margin-left: 0px;
                    margin-right: 0px;
                    padding-left: 0px;
                    padding-right: 0px;
                }
            </style>
        <?php endif;?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151252635-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-151252635-1');
</script>

    </head>
    <body>
 <?php if($sidebar){ ?>
        <div id="sidebar" class="sidebar collapsed">
            <?php if ($map_settings['config_show_makers_filter']){ ?>
            <!-- Nav tabs -->
            <div class="sidebar-tabs">
                <ul role="tablist">
                    <li><a href="#home" role="tab"><i class="fa fa-filter"></i></a></li>
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="sidebar-content">
                <!--        <form style="display: block;">-->
                <div class="sidebar-pane" id="home">
                    <h1 class="sidebar-header">
                        <?php echo $lang["filter"];?>
                        <span class="sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <div class="panel-body">
                      <!--  <label>
                            <input <?php echo '';//checked="checked" ?> onclick="nxIK.chackboxSelectAll(this);
                        checkboxClicked();" type="checkbox" name="check_all" id="check_all" value="-1"> <?php echo $lang["Check_All"];?>
                        </label>-->
                        <div class="checkbox" style="height: 880px; overflow-y: scroll;">

                            <?php $i=0;
                            $j=0;
                            $cur_ekraf = '';
                             foreach ($config_marker_types['pinImage'] as $key => $marker_info){
                                $str_check = '';

                                if($marker_info['is_aktif']){
                                    $str_check = 'checked="checked"';
                                }

                                if($cur_ekraf!=$marker_info['is_ekraf']){ if($j>0){ ?> </div><?php } if($j==0){?>

                                    <a href="#check-wisata" class="btn btn-success" data-toggle="collapse" style="margin-bottom: 5px;">Wisata (3A)</a><br />
                                        <div id="check-wisata" class="collapsing" > <?php } else if($j==1){?>

                                        <a href="#check-ekraf" class="btn btn-info" data-toggle="collapse"  style="margin-bottom: 5px;">Ekonomi Kreatif</a><br />
                                        <div id="check-ekraf" class="collapsing"> 
                                            <?php } else {?>

                                        <a href="#check-bencana" class="btn btn-warning" data-toggle="collapse"  style="margin-bottom: 5px;">Bencana</a>
                                        <div id="check-bencana" class="collapsing">
                                <?php } $j++;}
                                ?>

                                <label style="margin-right: 9px; margin-bottom: 10px;">
                                    <input <?php echo $str_check;?> onclick="checkboxClicked();" type="checkbox" value="<?php echo $key ?>"> <img src="<?php echo $marker_info['src'] ?>" alt="<?php echo $marker_info['type_name'] ?>" title="<?php echo $marker_info['type_name'] ?>" width="32px"> <?php echo $marker_info['type_name'] ?>
                                </label>
                                <br>
                            <?php 
                            $i++; $cur_ekraf= $marker_info['is_ekraf'];
                        } ?></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } if(!isset($_GET['iframe'])):?>

        <nav class="navbar  navbar-fixed-top navbar-inverse" >
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php" style="padding-top: 0px;" id="logo-brand"><img src="static/img/favicon.png" alt="" class="logo-navbar" style="height:42px;padding-top:2px;" ></a>
                    <a class="navbar-brand" href="<?php echo HTTP_APP_PATH ?>">
                        <?php echo $lang["site_title"]; ?>
                    </a>

                    <button class="navbar-toggle" data-toggle="collapse"  data-target="#menu-utama">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    

                </div>
              <div class="collapse navbar-collapse" id="menu-utama">
                    <div class="navbar-offcanvas offcanvas">
                        <ul class="nav navbar-nav">
                            <?php include_once("blocks/website_menu.php");?>
                        </ul>

                        <ul class="nav navbar-right">
                            <?php include_once("blocks/user_menu.php");?>
                        </ul>
     <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdown-notif"><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-bell" style="font-size:18px;"></span></a>
       <ul class="dropdown-menu" id="content-notif"></ul>
      </li>
     </ul>
                    </div>
                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container-fluid -->
        </nav>
        <?php endif;?>