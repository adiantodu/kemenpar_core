<?php
/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 * 
 */
include_once("includes/config.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/exec.php");
include_once(APP_PATH . "/includes/classes/pagination/pagination.class.php");

$sql_where = "";
$pagination = (new Pagination());

$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? (int) $_GET["page"] : 1;

$input_search_text = $search_text = isset($_GET['search_text']) ? trim($_GET['search_text']) : '';
$jns_objek = isset($_GET['jns_bencana']) ? trim($_GET['jns_bencana']) : '';
$arr_jenis = array();
if(!empty($jns_objek)){
$arr_jenis[0] = $jns_objek;
}

//record per Page($per_page)
$markers_per_page = isset($_GET['markers_per_page']) && in_array((int) $_GET['markers_per_page'], $config_markers_per_page) ? $_GET['markers_per_page'] : $config_markers_per_page[0];

$offset = $markers_per_page * ($current_page - 1);
$markers_data = $bencana->get_list($markers_per_page, $offset, $search_text,$jns_objek);
$list_tipe = $bencana->get_jenis_bencana();

$pagination->setCurrent($current_page);
$pagination->setTotal($markers_data['count_num_rows']);
$pagination->setRPP($markers_per_page);
$pagination_html = $pagination->parse();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Daftar Bencana</title>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="static/img/favicon.png">
        <script lang="javascript">
            var SITE_DOMAIN = '<?php echo SITE_DOMAIN?>';
            var HTTP_APP_PATH = '<?php echo HTTP_APP_PATH?>';
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types) ?>;
            var ZOOM = <?php echo $map_settings['config_zoom'] ?>;
            var MAP_CENTER_LAT = <?php echo $map_settings['config_map_center_latitude'] ?>;
            var MAP_CENTER_LNG = <?php echo $map_settings['config_map_center_longitude'] ?>;
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types)?>;
            var MAP_SOURCE = <?php echo $map_settings['config_map_source_id']; ?>;
            <?php if($map_settings['config_map_source_id'] == 1): //mapbox ?>
            var MAPBOX_API_KEY = '<?php echo strlen($map_settings['config_mapbox_api_key'])?$map_settings['config_mapbox_api_key']:"";?>';
            <?php endif;?>
        </script>

        <?php include_once("blocks/scripts.php")?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script>
                    $(document).ready(function() {
                        $("a.img_objek").fancybox();
                    });
        </script>
    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-static-top mb0" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo HTTP_APP_PATH ?>">
                    <?php echo $lang["site_title"]; ?>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php include_once("blocks/website_menu.php");?>
                        </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php include_once("blocks/user_menu.php");?>
                </ul>
            </div>

        </div>
    </nav>
            <!-- Modal -->
    <div class="container-fluid">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Objek #<span id="marker_it_title"></span></h4>
                        </div>
                        <form class="form-horizontal" method="post" id="update-form" enctype="multipart/form-data" action="<?php echo HTTP_APP_PATH ?>exec.php">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="inputMarkerTitle" class="col-sm-3 control-label">Judul *</label>
                                    <div class="col-sm-8">
                                        <input name="title" required="" value="" type="text" class="form-control" id="inputMarkerTitle">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMarkerTitle" class="col-sm-3 control-label">Tipe</label>
                                    <div class="col-sm-8" id="inputMarkerType">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMarkerDescription" class="col-sm-3 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea name="description" id='inputMarkerDescription' class="form-control" rows="8"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMarkerImage" class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-8">
                                        <span id="inputMarkerImage" style="float: left"></span>
                                        <input type="file" name="marker_image">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMarkerLatitude" class="col-sm-3 control-label">Latitude *</label>
                                    <div class="col-sm-4">
                                        <input name="lat" required="" value="" type="text" class="form-control" id="inputMarkerLatitude">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMarkerLongitude" class="col-sm-3 control-label">Longitude *</label>
                                    <div class="col-sm-4">
                                        <input name="lng" required="" value="" type="text" class="form-control" id="inputMarkerLongitude">
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div id="map-canvas" class=""></div>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <input type='hidden' name='action' id='action' value='update-marker'>
                                <input type='hidden' name='marker_id' id='marker_id' value=''>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" onclick='saveData();' class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php if (isset($_GET["msg"]) && $_GET['msg'] == "marker_deleted"): ?>
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    Objek Berhasil dihapus.
                </div>
            <?php endif; ?>
                <div class="text-center">

            <form class="navbar-form navbar-left" role="search" action="<?php echo HTTP_APP_PATH ?>list-bencana.php">
                    <div class="form-group">
                        <input type="search" class="form-control" id="search" name="search_text" value="<?php echo $input_search_text ?>" placeholder="Cari Bencana">
                    </div>
                <select class="form-control" name="jns_bencana" id="jns_bencana" >
                    <option value="">Semua</option>
                        <?php foreach ($list_tipe as $tipe){ ?>
                            <option value="<?php echo $tipe['kejadian']; ?>" <?php echo ($tipe['kejadian'] == $jns_objek) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['kejadian']; ?></option>
                        <?php }?>
                    </select>
                    <button type="submit" class="btn btn-default">Cari</button>
                    <a href="<?php echo HTTP_APP_PATH ?>manage.php" class="btn btn-default">Reset</a>
                    <select class="form-control" name="markers_per_page" id="markers_per_page" onchange="this.form.submit();">
                        <?php foreach ($config_markers_per_page as $per_page): ?>
                            <option value="<?php echo $per_page; ?>" <?php echo ($per_page == $markers_per_page) ? ' selected="selected" ' : ''; ?>><?php echo $per_page; ?></option>
                        <?php endforeach; ?>
                    </select>
                </form>
</div>     <div class="clearfix"></div>
            <div class="text-center">
                <?php echo $pagination_html ?>
            </div>

            <div class="clearfix"></div>
            <?php if ($markers_data['rows']): ?>
            <table class="table" id="table">
                <thead>
                 <tr>
                        <th width="50">#</th>
                        <th width="50">Kejadian</th>
                        <th width="250">Lokasi</th>
                        <th>Keterangan</th>
                        <th width="200">Waktu</th>
                        <th>Sumber</th>
                        <th width="120">Aksi</th>
                    </tr>
                   <!-- <tr>
                        <th >#</th>
                        <th >Kejadian</th>
                        <th >Lokasi</th>
                        <th></th>
                        <th>Wisma Luka</th>
                        <th>Wisman Hilang</th>
                        <th>Wisman Meninggal</th>
                        <th>Wisnus Luka</th>
                        <th>Wisnus Hilang</th>
                        <th>Wisnus Meninggal</th>
                        <th width="200">Waktu Kejadian</th>
                        <th width="120">Aksi</th>
                    </tr>-->
                </thead>
                
                <tbody>
                    <?php $i=1 ; 
                    foreach ($markers_data['rows'] as $data){ 
                        $source = $data["sumber_data"];?>
                        <tr>
                            <td id="marker-id-<?php echo $data["id_bencana"] ?>"><?php echo $i ?></td>
                            <td id="marker-title-<?php echo $data["id_bencana"] ?>"><?php echo $data["kejadian"] ?></td>
                            <td id="marker-lokasi-<?php echo $data["id_bencana"] ?>"><?php if( $source == 'BNPB'){ echo $data["nkab"] . ' ' .$data["nprop"] ; } else { echo $data['lokasi'];} ?></td>
                            <td id="marker-keterangan-<?php echo $data["id_bencana"]; ?>"><?php echo nl2br($data["keterangan"]); ?></td>

                       <!--     <td id="marker-lat-<?php echo $data["id_bencana"] ?>"><?php echo $data["wisman_luka"] ?></td>
                            <td id="marker-lng-<?php echo $data["id_bencana"] ?>"><?php echo $data["wisman_hilang"] ?></td>
                            <td id="marker-lng-<?php echo $data["id_bencana"] ?>"><?php echo $data["wisman_meninggal"] ?>
                            <td id="marker-lat-<?php echo $data["id_bencana"] ?>"><?php echo $data["wisnus_luka"] ?></td>
                            <td id="marker-lng-<?php echo $data["id_bencana"] ?>"><?php echo $data["wisnus_hilang"] ?></td>
                            <td id="marker-lng-<?php echo $data["id_bencana"] ?>"><?php echo $data["wisnus_meninggal"] ?>    
                            </td>-->


                            <td><?php
                                $data["waktu"] = new DateTime($data["waktu"]);
                                echo  $data["waktu"]->format("d M Y ") . '<br />' . $data['jam']; ?></td>
                            <td><?php echo $data["sumber_data"];?></td>
                            <td>
                                <a onclick="return confirm('Anda Yakin menghapus data ini?');" href="<?php echo HTTP_APP_PATH ?>/efek.php?action=delete-bencana&id=<?php echo $data["id_bencana"] ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                <a href="detil-bencana.php?id=<?php echo $data['id_bencana'];?>">
                                <button class="btn btn-success" data-id="<?php echo $data["id_bencana"] ?>"><i class="glyphicon  glyphicon-zoom-in"></i></button></a>
                            </td>
                        </tr>
                    <?php $i++; }?>
                </tbody>
            </table>
            <?php else: ?>
            <div>
            Results not found.
            </div>
            <?php endif; ?>
            <div class="clearfix"></div>
            <div class="text-center">
                <?php echo $pagination_html ?>
            </div>
            <?php if ($markers_data['rows']): ?>

            <a href="<?php echo HTTP_APP_PATH ?>/exec.php?action=download-csv" class="btn btn-default"><i class="glyphicon glyphicon-save-file"></i> Download CSV</a>
            <?php endif; ?>
        </div>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/js/update-marker-normal.js" type="text/javascript"></script>
        <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>