<?php
/**
 * @package Leaflet Map server side Markers clustering v1.0
 * @author Igor Karpov <mail@noxls.net>
 * 
 */
error_reporting(0);
include_once("includes/config.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/exec.php");
include_once(APP_PATH . "/includes/classes/pagination/pagination.class.php");


$sql_where = "";
$pagination = (new Pagination());

$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? (int) $_GET["page"] : 1;

$input_search_text = $search_text = isset($_GET['search_text']) ? trim($_GET['search_text']) : '';
$jns_objek = isset($_GET['jns_objek']) ? trim($_GET['jns_objek']) : '';
$arr_jenis = array();
if(!empty($jns_objek)){
$arr_jenis[0] = $jns_objek;
}

//record per Page($per_page)
$markers_per_page = isset($_GET['markers_per_page']) && in_array((int) $_GET['markers_per_page'], $config_markers_per_page) ? $_GET['markers_per_page'] : $config_markers_per_page[0];
$this_id = $_GET['id'];
$marker->setId($id);
$marker_info = $marker->getById();
$lat=$marker_info['lat'];
$lng = $marker_info['lng'];
//$lat= $_GET['lat'];
//$lng = $_GET['long'];
$offset = $markers_per_page * ($current_page - 1);
$markers_data = $marker->get_markers_around($lat,$lng,$markers_per_page, $offset, $search_text,$arr_jenis);
$list_tipe = $marker_type->get();

$pagination->setCurrent($current_page);
$pagination->setTotal($markers_data['count_num_rows']);
$pagination->setRPP($markers_per_page);
$pagination_html = $pagination->parse();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Daftar Objek</title>
        <meta charset="utf-8">
                <link rel="shortcut icon" href="static/img/favicon.png">
        <script lang="javascript">
            var SITE_DOMAIN = '<?php echo SITE_DOMAIN?>';
            var HTTP_APP_PATH = '<?php echo HTTP_APP_PATH?>';
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types) ?>;
            var ZOOM = <?php echo $map_settings['config_zoom'] ?>;
            var MAP_CENTER_LAT = <?php echo $map_settings['config_map_center_latitude'] ?>;
            var MAP_CENTER_LNG = <?php echo $map_settings['config_map_center_longitude'] ?>;
            var MAP_SETTINGS = <?php echo json_encode($config_marker_types)?>;
            var MAP_SOURCE = <?php echo $map_settings['config_map_source_id']; ?>;
            <?php if($map_settings['config_map_source_id'] == 1): //mapbox ?>
            var MAPBOX_API_KEY = '<?php echo strlen($map_settings['config_mapbox_api_key'])?$map_settings['config_mapbox_api_key']:"";?>';
            <?php endif;?>
        </script>
        <?php include_once("blocks/scripts.php")?>
    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-static-top mb0" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo HTTP_APP_PATH ?>">
                    <?php echo $lang["site_title"]; ?>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo HTTP_APP_PATH ?>add.php"><?php echo $lang["menu_add_marker"]; ?></a></li>
                </ul>
                <form class="navbar-form navbar-left" role="search" action="<?php echo HTTP_APP_PATH ?>manage.php">
                    <div class="form-group">
                        <input type="search" class="form-control" id="search" name="search_text" value="<?php echo $input_search_text ?>" placeholder="Cari Objek">
                    </div>
                <select class="form-control" name="jns_objek" id="jns_objek" >
                        <?php foreach ($list_tipe as $tipe): ?>
                            <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_objek) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <button type="submit" class="btn btn-default">Cari</button>
                    <a href="<?php echo HTTP_APP_PATH ?>manage.php" class="btn btn-default">Reset</a>
                    <select class="form-control" name="markers_per_page" id="markers_per_page" onchange="this.form.submit();">
                        <?php foreach ($config_markers_per_page as $per_page): ?>
                            <option value="<?php echo $per_page; ?>" <?php echo ($per_page == $markers_per_page) ? ' selected="selected" ' : ''; ?>><?php echo $per_page; ?></option>
                        <?php endforeach; ?>
                    </select>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <?php include_once("blocks/user_menu.php");?>
                </ul>
            </div>

        </div>
    </nav>
            <!-- Modal -->
    <div class="container-fluid">
           
            <div class="text-center">
                <?php //echo $pagination_html ?>
            </div>

            <div class="clearfix"></div>
            <?php if ($markers_data['rows']): ?>
            <table class="table" id="table">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th width="50">Tipe</th>
                        <th width="250">Nama</th>
                        <th>Keterangan</th>
                        <th>Gambar</th>
                        <th width="200">Jarak (Km)</th>

                    </tr>
                </thead>
                
                <tbody>
                    <?php $i=1; foreach ($markers_data['rows'] as $data){
                        if($data['id'] != $this_id) {
                        ?>
                        <tr>
                            <td id="marker-id-<?php echo $data["id"] ?>"><?php echo $i; ?></td>
                            <td id="marker-type-<?php echo $data["id"] ?>" data-marker-type-<?php echo $data["id"] ?>="<?php echo $data["marker_type"] ?>">
                                <?php if(isset($config_marker_types['pinImage']{$data["marker_type"]}['src'])):?>
                                    <img src="<?php echo $config_marker_types['pinImage']{$data["marker_type"]}['src'] ?>" alt="<?php echo $config_marker_types['pinImage']{$data["marker_type"]}['type_name'] ?>" title="<?php echo $config_marker_types['pinImage']{$data["marker_type"]}['type_name'] ?>" width="32px">
                                <?php else:?>
                                    Undefined
                                <?php endif;?>
                            </td>
                            <td id="marker-title-<?php echo $data["id"] ?>"><?php echo $data["title"] ?></td>
                            <td id="marker-lat-<?php echo $data["id"] ?>"><?php echo $data["description"] ?></td>
                            <td>
                                <?php if(isset($data["image_type"]) && $data["image_type"] != ""):?>

                                <a id="marker-img-<?php echo $data["id"] ?>" href="<?php echo HTTP_IMG_PATH . ($data['id'] % 10) . '/' . $data['id'] . '.' . $data['image_type'];?>" target="_blank">
                                    <i class="far fa-image fa-2"></i>
                                </a>
                                <?php endif;?>
                            </td>

                            <td><?php

                                echo  round($data["distance"],2); ?></td>
                           
                        </tr>
                    <?php $i++;}}?>
                </tbody>
            </table>
            <?php else: ?>
            <div>
            Results not found.
            </div>
            <?php endif; ?>
            <div class="clearfix"></div>
            <div class="text-center">
                <?php //echo $pagination_html ?>
            </div>
            <?php if ($markers_data['rows']): ?>

            <a href="<?php echo HTTP_APP_PATH ?>/exec.php?action=download-csv" class="btn btn-default"><i class="glyphicon glyphicon-save-file"></i> Download CSV</a>
            <?php endif; ?>
        </div>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/js/update-marker-normal.js" type="text/javascript"></script>
        <?php include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>