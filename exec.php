<?php

if ( ! session_id() ) @ session_start();
header('Content-type: text/html; charset=utf-8');
include_once("includes/config.php");
include_once(APP_PATH . "/includes/language/en_GB.php");
include_once(APP_PATH . "/includes/connect.php");
include_once(APP_PATH . "/includes/classes/Marker_type.class.php");
include_once(APP_PATH . "/includes/classes/Cuaca.class.php");
include_once(APP_PATH . "/includes/classes/Marker.class.php");
include_once(APP_PATH . "/includes/classes/Bencana.class.php");
include_once(APP_PATH . "/includes/classes/Ekraf.class.php");
include_once(APP_PATH . "/includes/classes/User.class.php");
include_once(APP_PATH . "/includes/classes/Cluster.class.php");
include_once(APP_PATH . "/includes/classes/Storage.class.php");
include_once(APP_PATH . "/includes/classes/Cache.class.php");
include_once(APP_PATH . "/includes/classes/Auth.class.php");
include_once(APP_PATH . "/includes/classes/Helper.class.php");
require_once APP_PATH . '/includes/classes/PhpSpreadsheet/src/Bootstrap.php';

Auth::check_access($allowed_pages);
if(isset($_SESSION['role'])){
$cur_role = $_SESSION['role'];
}
$bencana = new Bencana($dbh);
$marker_type = new Marker_type($dbh);
$marker = new Marker($dbh);
$cuaca = new Cuaca($dbh);
$ekraf = new Ekraf($dbh);
$storage = new Storage($dbh);
$cache = new Cache($dbh);
$cluster = new Cluster();
$user_obj = new User($dbh);
//prepare settings and config
$map_settings = array();
$map_settings = $storage->get_settings();
if(isset($map_settings['config_autodetect_map_center']) && $map_settings['config_autodetect_map_center'] == 1) {
    if(!isset($_SESSION['nxik_map_center_latitude']) && !isset($_SESSION['nxik_map_center_longitude'])) {
        include_once(APP_PATH . "vendor/autoload.php");
        $curl = new Curl\Curl();
        $curl->setOpt(CURLOPT_ENCODING , '');
        $user_ip = Helper::getRealIpAddr();
        $ipstack_url = NXIK_API_IPSTACK_URL . $user_ip;
        $curl->get($ipstack_url, array('access_key' => (NXIK_API_IPSTACK_KEY != ''?NXIK_API_IPSTACK_KEY:$map_settings['config_api_key_ipstack'])));
        if (!$curl->error) {
            if(!is_null($curl->response->latitude) && !is_null($curl->response->longitude)) {
                $_SESSION['nxik_map_center_latitude'] = $map_settings['config_map_center_latitude'] = $curl->response->latitude;
                $_SESSION['nxik_map_center_longitude'] = $map_settings['config_map_center_longitude'] = $curl->response->longitude;
            }
        }
    }
    else {
        $map_settings['config_map_center_latitude'] = $_SESSION['nxik_map_center_latitude'];
        $map_settings['config_map_center_longitude'] = $_SESSION['nxik_map_center_longitude'];
    }
}

$config_marker_types["pinImage"] = $marker_type->get_pin_image();
$config_marker_types["pinImage2"] = $marker_type->get_pin_image_v2();
Helper::prepare_config_marker_types($config_marker_types, $map_settings);
$get_status = $marker->get_status();
$get_subkategori= $marker->get_subkategori();
$action = filter_input(INPUT_GET, "action")?:filter_input(INPUT_POST, "action");

$id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);

switch ($action) {
    case "marker":
        $marker->setId((int) $id);
        $marker_info = $marker->getById();
        if(strlen($marker_info['description']) > 200) {
            $short_description = substr(strip_tags($marker_info['description']), 0, 200);
            $marker_info['short_description'] = substr($short_description, 0, strrpos($short_description, ' ')) . " ...";
        }
        else {
            $marker_info['short_description'] = strip_tags($marker_info['description']);
        }
        break;
    case "save-settings":
        $settings_data['config_autodetect_map_center'] = (int)filter_input(INPUT_POST, 'autodetect_map_center', FILTER_VALIDATE_BOOLEAN);
        $settings_data['config_map_center_latitude'] = filter_input(INPUT_POST, 'map_center_latitude', FILTER_VALIDATE_FLOAT);
        $settings_data['config_map_center_longitude'] = filter_input(INPUT_POST, 'map_center_longitude', FILTER_VALIDATE_FLOAT);
        $settings_data['config_map_width'] = filter_input(INPUT_POST, 'map_width');
        $settings_data['config_map_height'] = filter_input(INPUT_POST, 'map_height');
        $settings_data['config_min_zoom'] = filter_input(INPUT_POST, 'min_zoom', FILTER_VALIDATE_INT);
        $settings_data['config_max_zoom'] = filter_input(INPUT_POST, 'max_zoom');
        $settings_data['config_alwaysClusteringEnabledWhenZoomLevelLess'] = filter_input(INPUT_POST, 'alwaysClusteringEnabledWhenZoomLevelLess');
        $settings_data['config_zoom'] = filter_input(INPUT_POST, 'zoom');
        $settings_data['config_distance'] = filter_input(INPUT_POST, 'distance');
        $settings_data['config_markers_cluster_level_2'] = filter_input(INPUT_POST, 'markers_cluster_level_2');
        $settings_data['config_markers_cluster_level_3'] = filter_input(INPUT_POST, 'markers_cluster_level_3');
        $settings_data['config_markers_cluster_level_4'] = filter_input(INPUT_POST, 'markers_cluster_level_4');
        $settings_data['config_markers_cluster_level_5'] = filter_input(INPUT_POST, 'markers_cluster_level_5');
        $settings_data['config_unclusterise_same_markers'] = (int)filter_input(INPUT_POST, 'unclusterise_same_markers', FILTER_VALIDATE_BOOLEAN);
        $settings_data['config_enable_cache'] = (int)filter_input(INPUT_POST, 'enable_cache', FILTER_VALIDATE_BOOLEAN);
        $settings_data['config_cache_level'] = filter_input(INPUT_POST, 'cache_level');
        $settings_data['config_show_makers_filter'] = (int)filter_input(INPUT_POST, 'show_makers_filter', FILTER_VALIDATE_BOOLEAN);
        $settings_data['config_search_results_per_page'] = filter_input(INPUT_POST, 'search_results_per_page');
        $settings_data['config_show_makers_search'] = (int)filter_input(INPUT_POST, 'show_makers_search', FILTER_VALIDATE_BOOLEAN);
        $settings_data['config_filter_search_place'] = filter_input(INPUT_POST, 'filter_search_place');
        $settings_data['config_map_type_id'] = filter_input(INPUT_POST, 'map_type_id');
        $settings_data['config_map_source_id'] = filter_input(INPUT_POST, 'map_source_id');
        $settings_data['config_map_language'] = filter_input(INPUT_POST, 'map_language');
        $settings_data['config_mapbox_api_key'] = filter_input(INPUT_POST, 'mapbox_api_key');
        $settings_data['config_api_key_ipstack'] = filter_input(INPUT_POST, 'api_key_ipstack');
        $settings_data['config_map_style'] = trim(filter_input(INPUT_POST, 'map_style'));
        foreach($settings_data as $key_settings => $value_settings) {
            $storage->setKey($key_settings);
            $storage->setValue($value_settings);
            $storage->set_data_by_key();
        }
        $response_messages["msg"]["post_url"] = HTTP_APP_PATH . "/settings.php?msg=settings_updated";
        print json_encode($response_messages);
        exit;
        break;
    case "settings-reset2default":
        if(!Helper::tableExists($dbh, DB_MARKERS_TABLE)) {
            $stmt = $dbh->prepare(sprintf(DB_MARKERS_TABLE_SQL, DB_MARKERS_TABLE));
            $stmt->execute();
        }
        if(!Helper::tableExists($dbh, DB_STORAGE_TABLE)) {
            $stmt = $dbh->prepare(sprintf(DB_STORAGE_TABLE_SQL, DB_STORAGE_TABLE));
            $stmt->execute();
        }
        if(!Helper::tableExists($dbh, DB_MARKER_TYPE_TABLE)) {
            $stmt = $dbh->prepare(sprintf(DB_MARKER_TYPE_TABLE_SQL, DB_MARKER_TYPE_TABLE));
            $stmt->execute();
        }
        foreach($default_map_settings as $key_settings => $value_settings) {
            $storage->setKey($key_settings);
            $storage->setValue($value_settings);
            $storage->set_data_by_key();
        }
        header('Location: ' . HTTP_APP_PATH . "/settings.php?msg=settings_reset2default");
        break;
    case "add-marker":
        $title = filter_input(INPUT_POST, "title");
        $marker->setTitle($title);
        $marker->setDescription(filter_input(INPUT_POST, "descrFiption"));
        $marker->setLat((float)filter_input(INPUT_POST, "lat", FILTER_VALIDATE_FLOAT));
        $marker->setLng((float)filter_input(INPUT_POST, "lng", FILTER_VALIDATE_FLOAT));
        $marker->setMarkerType((int)filter_input(INPUT_POST, "marker_type", FILTER_VALIDATE_INT));
        $marker->setProvince(filter_input(INPUT_POST, "provinsi"));
        $marker->setCity(filter_input(INPUT_POST, "kota"));
        $marker->setKecamatan(filter_input(INPUT_POST, "kecamatan"));
        $marker->setKelurahan(filter_input(INPUT_POST, "kelurahan"));
        $marker->setStatus((int)filter_input(INPUT_POST, "status", FILTER_VALIDATE_INT));
        $marker->setSumberdata((int)filter_input(INPUT_POST, "sumber_data", FILTER_VALIDATE_INT));
        $marker->setPengelola(filter_input(INPUT_POST, "pengelola"));
        $marker->setSubkategori((int)filter_input(INPUT_POST, "subkategori", FILTER_VALIDATE_INT));
        $marker->setCreatedBy($_SESSION['username']);

        $marker_id = $marker->add();
        if($marker_id > 0) {
            $user_obj->insert_log($_SESSION['username'],'Tambah Objek id:' . $marker_id,SITE_DOMAIN . 'manage.php');
            if (isset($_FILES["marker_image"]) && $_FILES["marker_image"]["error"] == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["marker_image"]["tmp_name"];
                $image_info = getimagesize($tmp_name);
                $image_type = $image_info[2];
                if(in_array($image_type , array(IMAGETYPE_JPEG))) {
                    $file_extension = str_replace("image/", "", $image_info['mime']);
                    $dir = $marker_id % 10;
                    $upload_dir = APP_IMG_PATH . "/" . $dir;

                    if(!file_exists($upload_dir)) {
                        mkdir($upload_dir, 0755);
                    }
                    move_uploaded_file($tmp_name, $upload_dir . "/" . $marker_id . "." . $file_extension);
                    $marker = new Marker($dbh);
                    $marker->setImageType($file_extension);
                    $marker->setId((int)$marker_id);
                    $marker->updateImageType();
                    $response_messages = array();

                    $response_messages["msg"]["code"] = 0;
                    $response_messages["msg"]["text"] = "Data tersimpan.";
                    $response_messages["msg"]["post_url"] = HTTP_APP_PATH."/manage.php";
                    print json_encode($response_messages);
                    break;
                }
            }
        }
        else {
            $response_messages["msg"]["code"] = 1;
            $response_messages["msg"]["text"] = "Terjadi Kesalahan.";
            $response_messages["msg"]["post_url"] = HTTP_APP_PATH."/manage.php";
            print json_encode($response_messages);
            break;
        }



        $response_messages = array();
        $response_messages["msg"]["code"] = 0;
        $response_messages["msg"]["text"] = "Data Tersimpan.";
        $response_messages["msg"]["post_url"] = HTTP_APP_PATH."/manage.php";
        print json_encode($response_messages);
        break;
    case "update-marker":
        $marker_id = (int)filter_input(INPUT_POST, "marker_id", FILTER_VALIDATE_INT);
        $marker->setTitle(filter_input(INPUT_POST, "title"));
        $marker->setDescription(nl2br(filter_input(INPUT_POST, "description")));
        $marker->setLat((float)filter_input(INPUT_POST, "lat", FILTER_VALIDATE_FLOAT));
        $marker->setLng((float)filter_input(INPUT_POST, "lng", FILTER_VALIDATE_FLOAT));
        $marker->setMarkerType((int)filter_input(INPUT_POST, "marker_type", FILTER_VALIDATE_INT));
        $marker->setProvince(filter_input(INPUT_POST, "provinsi"));
        $marker->setCity(filter_input(INPUT_POST, "kota"));
        $marker->setKecamatan(filter_input(INPUT_POST, "kecamatan"));
        $marker->setKelurahan(filter_input(INPUT_POST, "kelurahan"));
        $marker->setStatus((int)filter_input(INPUT_POST, "status", FILTER_VALIDATE_INT));
        $marker->setPengelola(filter_input(INPUT_POST, "pengelola"));
        $marker->setSubkategori((int)filter_input(INPUT_POST, "subkategori", FILTER_VALIDATE_INT));
        $marker->setUpdatedBy($_SESSION['username']);
        $marker->setId($marker_id);
        $data = $marker->getById();
        if($marker->update()){
        $user_obj->insert_log($_SESSION['username'],'Edit Objek: '.$data['title']. ' - ' . $marker_id,SITE_DOMAIN . 'manage.php');
        }
        if (isset($_FILES["marker_image"]) && $_FILES["marker_image"]["error"] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["marker_image"]["tmp_name"];
            $image_info = getimagesize($tmp_name);
            $image_type = $image_info[2];
            if(in_array($image_type , array(IMAGETYPE_JPEG))) {
                $file_extension = str_replace("image/", "", $image_info['mime']);
                $dir = $marker_id % 10;
                $upload_dir = APP_IMG_PATH . "/" . $dir;

                if(!file_exists($upload_dir)) {
                    mkdir($upload_dir, 0755);
                }
                move_uploaded_file($tmp_name, $upload_dir . "/" . $marker_id . "." . $file_extension);
                $marker = new Marker($dbh);
                $marker->setImageType($file_extension);
                $marker->setId((int)$marker_id);
                $marker->updateImageType();
                $response_messages = array();

                $response_messages["msg"]["code"] = 0;
                $response_messages["msg"]["text"] = "Data updated.";
                $response_messages["msg"]["post_url"] = HTTP_APP_PATH;
                print json_encode($response_messages);
                break;
            }
        }
        $response_messages["msg"]["code"] = 0;
        $response_messages["msg"]["text"] = "Data updated.";
        print json_encode($response_messages);
        break;
    case "list-markers":
        //header('Content-Type: application/json; charset=utf-8', true, 200);
        $current_page = filter_input(INPUT_GET, "page", FILTER_VALIDATE_INT);
        $search_text = filter_input(INPUT_GET, "search_text");
        $filter = filter_input(INPUT_GET, "filter", FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY)?:array();
        if($map_settings['config_show_makers_filter'] && sizeof($filter) == 0) {
            $data = array("count_num_rows" => 0);
        }
        else {
            $order_by = 'id DESC';
            $data = $marker->get_markers(
                                $map_settings['config_search_results_per_page'],
                                $current_page * $map_settings['config_search_results_per_page'],
                                $search_text,
                                $filter,
                                $order_by
            );
        }

        print json_encode($data);
        break;
    case "get-markers":
        $sid = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
        $zoom = filter_input(INPUT_GET, "zoom", FILTER_VALIDATE_INT);
        $filter = filter_input(INPUT_GET, "filter", FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY)?:array();
        $cache_filter = "";
        if(sizeof($filter) != sizeof($config_marker_types['pinImage'])) {
            $cache_filter = implode("_", $filter);
        }
        //hide all markers if checkboxes unchecked
        if($map_settings['config_show_makers_filter'] && sizeof($filter) == 0) {
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Rid"] = $sid;
            $clustered["Count"] = 0;
            $clustered["Mia"] = 0;
            $clustered["Polylines"] = array();
            $clustered["Markers"] = array();
            print json_encode($clustered);
            break;
        }
        $json_clustered = "";
        //chech cached data
        if($map_settings['config_enable_cache']) {
            $storage->setKey(INDEX_CLUSTER_MAP_KEY . $zoom . "_filter" . $cache_filter);
            $json_clustered = $storage->get_data_by_key();
        }

        if(strlen($json_clustered)) {
            $json_clustered = json_decode($json_clustered, true);
            $json_clustered["Rid"] = $sid;
            $json_clustered = json_encode($json_clustered);
            print $json_clustered;
        }
        else {
            $data = array();
            $nelat = str_replace("_", ".", filter_input(INPUT_GET, 'nelat'));
            $swlat = str_replace("_", ".", filter_input(INPUT_GET, 'swlat'));
            $nelon = str_replace("_", ".", filter_input(INPUT_GET, 'nelon'));
            $swlon = str_replace("_", ".", filter_input(INPUT_GET, 'swlon'));

            if(1 == $map_settings["config_cache_level"] && $map_settings["config_enable_cache"] && $zoom <= $map_settings["config_zoom"]) {
                $data = $marker->get_markers(0, 0, "", $filter);
                $data = $data["rows"];
            }
            else {
                $data = $marker->get_map_markers($swlat, $nelat, $swlon, $nelon, $filter);

                if($map_settings["config_unclusterise_same_markers"] && $map_settings["config_max_zoom"] == $zoom) {
                    $hash = array();
                    array_walk($data, function(&$val, $key) use (&$hash) {
                        $new_hash = serialize(array($val["lat"], $val["lng"]));
                        if(!in_array($new_hash, $hash)) {
                            $hash[] = $new_hash;
                        }
                        else {
                            $val["lng"] +=  (($key % 2)?1:-1) * ($key * 4) / 100000;
                            $hash[] = serialize(array(sprintf('%.6f', $val["lat"]), sprintf('%.6f', $val["lng"])));
                        }
                    });
                }
            }

            if (sizeof($data)) {
                $data = $cluster->clusterer($data, $map_settings["config_distance"], $zoom);
                $clustered["EMsg"] = "";
                $clustered["Msec"] = 185;
                $clustered["Ok"] = 1;
                $clustered["Rid"] = $sid;
                $clustered["Count"] = sizeof($data);
                $clustered["Mia"] = 0;
                $clustered["Polylines"] = array();
                $clustered["Markers"] = array();
                foreach ($data as $key => $marker) {
                    if (isset($marker[0]) && is_array($marker[0])) {
                        $clustered["Markers"][$key] = array(
                            "I" => (sizeof($marker) == 1 ? $marker[0]['id'] : 0),
                            "T" => (int)$marker[0]['marker_type'],
                            "X" => $marker[0]['lng'],
                            "Y" => $marker[0]['lat'],
                            "BB" =>$marker[0]['jenis'],
                            "C" => sizeof($marker)
                        );
                    } else {
                        $clustered["Markers"][$key] = array(
                            "I" => $marker['id'],
                            "T" => (int)$marker['marker_type'],
                            "X" => $marker['lng'],
                            "Y" => $marker['lat'],
                            "BB" =>$marker['jenis'],
                            "C" => 1
                        );
                    }
                }
                $json_clustered = json_encode($clustered);

            }
            else {
                $clustered["EMsg"] = "";
                $clustered["Msec"] = 0;
                $clustered["Ok"] = 1;
                $clustered["Rid"] = $sid;
                $clustered["Count"] = 0;
                $clustered["Mia"] = 0;
                $clustered["Polylines"] = array();
                $clustered["Markers"] = array();
                print json_encode($clustered);
                break;
            }
            if(1 == $map_settings["config_cache_level"] && $map_settings["config_enable_cache"] && $zoom <= $map_settings["config_zoom"]) {
                $storage->setKey(INDEX_CLUSTER_MAP_KEY . $zoom . "_filter" . $cache_filter);
                $storage->setValue($json_clustered);
                $storage->set_data_by_key();
            }
            print $json_clustered;
        }
        break;
    case "get-marker-v2":
        $marker_type = filter_input(INPUT_GET, "marker_type");
        $data = $marker->get_markers_v2($marker_type);
                
            $clustered['row'] = count($data['markers']);
            foreach ($data['markers'] as $key => $marker) {
                    if (isset($marker[0]) && is_array($marker[0])) {
                        $clustered["Markers"][$key] = array(
                            "I" => (sizeof($marker) == 1 ? $marker[0]['id'] : 0),
                            "T" => (int)$marker[0]['marker_type'],
                            "X" => $marker[0]['lng'],
                            "Y" => $marker[0]['lat'],
                            "BB" =>$marker[0]['jenis'],
                            "C" => sizeof($marker)
                        );
                    } else {
                        $clustered["Markers"][$key] = array(
                            "I" => $marker['id'],
                            "T" => (int)$marker['marker_type'],
                            "X" => $marker['lng'],
                            "Y" => $marker['lat'],
                            "BB" =>$marker['jenis'],
                            "C" => 1
                        );
                    }
                }

        $json_clustered = json_encode($clustered);
        print $json_clustered;
        break;

    case "get-detail-notif-v2":
        $id_comment=$_GET['id'];

        $data = $marker->get_detail_notif_v2($id_comment);

        print json_encode($data);

        break;
    
    case "get-marker-info-v2":
        if ($id) {
            $jns=$_GET['jns'];

            if($jns==1){
            $marker->setId((int) filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT));
            $row = $marker->getById();
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Id"] = $id;
            $clustered["Rid"] = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
            $clustered["Lat"] = $row['lat'];
            $clustered["Lon"] = $row['lng'];
            $date = new DateTime($row['date_add']);
            $clustered["Type"] = 0;
            $clustered["Content"]['marker_title'] = $row["title"];
            if(isset($row['image_type']) && $row['image_type'] != '') {
                $clustered["Content"]['img_src'] = HTTP_IMG_PATH . ($row['id'] % 10) . '/' . $row['id'] . '.' . $row['image_type'];
            }

            $clustered["Content"]["description"] = $row["description"];
            $clustered["Content"]["date_add"] = $date->format("d F Y");
            } elseif($jns==2) {

            $row= $bencana->getById($id);
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Id"] = $id;
            $clustered["Rid"] = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
            $clustered["Lat"] = $row['lat'];
            $clustered["Lon"] = $row['lng'];
            $date = new DateTime($row['waktu']);
            $clustered["Type"] = 0;
            $clustered["Content"]['marker_title'] = $row["kejadian"];
            if(isset($row['image_type']) && $row['image_type'] != '') {
                $clustered["Content"]['img_src'] = '';
            }
            $lokasi = $row['lokasi'];
            if(!empty($lokasi)){
                $lokasi = '<br />' . nl2br($lokasi);
            }
            $clustered["Content"]["description"] =  $row["nkab"]. ' ' . $row['nprop'] . '<br/ >'. nl2br($row['keterangan']). $lokasi;
            $clustered["Content"]["date_add"] = $date->format("d F Y");
            } else {
                $ekraf->setId((int) filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT));
                $row= $ekraf->getById($id);
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Id"] = $id;
            $clustered["Rid"] = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
            $clustered["Lat"] = $row['lat'];
            $clustered["Lon"] = $row['lng'];
            $date = new DateTime($row['date_created']);
            $clustered["Type"] = 0;
            $clustered["Content"]['marker_title'] = $row["title"];
            if(isset($row['image_type']) && $row['image_type'] != '') {
                $clustered["Content"]['img_src'] = HTTP_EKRAF_PATH . ($row['id_ekraf'] % 10) . '/' . $row['id_ekraf'] . '.' . $row['image_type'];
            }

            $clustered["Content"]["description"] = $row["description"];
            $clustered["Content"]["date_add"] = $date->format("d F Y");
            }

            print json_encode($clustered);

        }
        break;
    case "get-cuaca-detail-v2":
        $prov = filter_input(INPUT_GET, "prov");  
        $data = $cuaca->get_detail_cuaca_v2($prov);
        $json_clustered = json_encode($data);
        print $json_clustered;
        break;
    case "get-provinsi":
    
        $strJsonFileContents = file_get_contents(APP_PATH . "/v2/json/geojson-provinsi.json");
        print $strJsonFileContents;
        break;
    
    case "get-gelombang":

        $loc = filter_input(INPUT_GET, "loc");
        $strJsonFileContents = file_get_contents(APP_PATH . "/v2/json/geo_gelombang/gel" . $loc . ".json");
        print $strJsonFileContents;
        break;

    case "get-detail-gelombang-v2":
        $kode = filter_input(INPUT_GET, "kode");
        $url = 'https://peta-maritim.bmkg.go.id/public_api/perairan/'.$kode.'.json';
        $clean_url =  str_replace(" ","%20",$url);   
        $json = file_get_contents($clean_url);

        print $json;
        break;

    case "cron-test-bnpb":
        include_once(APP_PATH . "/cron/get_data_bnpb.php");
        break;

    case "cron-test-bmkg":
        include_once(APP_PATH . "/cron/get_gempa_bmkg.php");
        break;


    case "get-marker-info":
        if ($id) {
            $jns=$_GET['jns'];

            if($jns==1){
            $marker->setId((int) filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT));
            $row = $marker->getById();
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Id"] = $id;
            $clustered["Rid"] = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
            $clustered["Lat"] = $row['lat'];
            $clustered["Lon"] = $row['lng'];
            $date = new DateTime($row['date_add']);
            $clustered["Type"] = 0;
            $clustered["Content"]['marker_title'] = $row["title"];
            if(isset($row['image_type']) && $row['image_type'] != '') {
                $clustered["Content"]['img_src'] = HTTP_IMG_PATH . ($row['id'] % 10) . '/' . $row['id'] . '.' . $row['image_type'];
            }

            $clustered["Content"]["description"] = $row["description"]. '<br/ ><a href="objek-detil.php?id='.$id.'"><button type="button" class="btn btn-info">Detil Objek</button></a>';
            $clustered["Content"]["date_add"] = $date->format("d F Y");
            } elseif($jns==2) {

            $row= $bencana->getById($id);
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Id"] = $id;
            $clustered["Rid"] = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
            $clustered["Lat"] = $row['lat'];
            $clustered["Lon"] = $row['lng'];
            $date = new DateTime($row['waktu']);
            $clustered["Type"] = 0;
            $clustered["Content"]['marker_title'] = $row["kejadian"];
            if(isset($row['image_type']) && $row['image_type'] != '') {
                $clustered["Content"]['img_src'] = '';
            }
            $lokasi = $row['lokasi'];
            if(!empty($lokasi)){
                $lokasi = '<br />' . nl2br($lokasi);
            }
            $clustered["Content"]["description"] =  $row["nkab"]. ' ' . $row['nprop'] . '<br/ >'. nl2br($row['keterangan']). $lokasi. '<br /><a href="bencana-detil.php?id='.$id.'"><button type="button" class="btn btn-info">Detil Bencana</button></a>';
            $clustered["Content"]["date_add"] = $date->format("d F Y");
            } else {
                $ekraf->setId((int) filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT));
                $row= $ekraf->getById($id);
            $clustered["EMsg"] = "";
            $clustered["Msec"] = 0;
            $clustered["Ok"] = 1;
            $clustered["Id"] = $id;
            $clustered["Rid"] = filter_input(INPUT_GET, "sid", FILTER_VALIDATE_INT);
            $clustered["Lat"] = $row['lat'];
            $clustered["Lon"] = $row['lng'];
            $date = new DateTime($row['date_created']);
            $clustered["Type"] = 0;
            $clustered["Content"]['marker_title'] = $row["title"];
            if(isset($row['image_type']) && $row['image_type'] != '') {
                $clustered["Content"]['img_src'] = HTTP_EKRAF_PATH . ($row['id_ekraf'] % 10) . '/' . $row['id_ekraf'] . '.' . $row['image_type'];
            }

            $clustered["Content"]["description"] = $row["description"]. '<br/ ><a href="ekraf-detil.php?id='.$id.'"><button type="button" class="btn btn-info">Detil Ekraf</button></a>';
            $clustered["Content"]["date_add"] = $date->format("d F Y");
            }

            print json_encode($clustered);

        }
        break;
    case "get-marker":
        if ($id) {
            $marker->setId((int)$id);
            $row = $marker->getById();
            print json_encode($row);
        }
        break;
    case "delete-marker":
        if ($id) {
            $marker->setId($id);
            $data = $marker->getById();
            if($marker->deleteById()){
            $user_obj->insert_log($_SESSION['username'],'Hapus Objek: ' . $data['title']);
            }
            if($map_settings["config_enable_cache"] && 1 == $map_settings["config_cache_level"]) {
                $cache->generate($marker->get_markers(), true);
            }
        }
        header('Location: ' . HTTP_APP_PATH . "/manage.php?msg=marker_deleted");
        exit;
        break;
    case "delete-marker-type":
        if ($id) {
            $marker_data = $marker_type->getById($id);
            if($marker_type->deleteById($id)) {
                unlink($marker_type->get_image($id, "", "dir"));
            }
            if($map_settings["config_enable_cache"] && 1 == $map_settings["config_cache_level"]) {
                $cache->generate($marker->get_markers(), true);
            }
        }
        header('Location: ' . HTTP_APP_PATH . "/marker_type.php?msg=marker_deleted");
        exit;
        break;
    case "add-marker-type":
        $marker_type_data = array();
        $marker_type_data['type_name'] = filter_input(INPUT_POST, "marker_type_name");
        $marker_type_data['is_aktif'] = filter_input(INPUT_POST, "is_aktif");
        $marker_type_data['3a_tipe'] = filter_input(INPUT_POST, "3a_tipe");
        $marker_type_data['is_ekraf'] = 0;
        $marker_type_data['image_type'] = '';
        $marker_type_data['image_width'] = 0;
        $marker_type_data['image_height'] = 0;
        $marker_type_id = $marker_type->save($marker_type_data);
        if($marker_type_id > 0) {
            if ($_FILES["marker_icon"]["error"] == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["marker_icon"]["tmp_name"];
                $image_info = getimagesize($tmp_name);
                $image_type = $image_info[2];
                if(in_array($image_type , array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG))) {
                    $file_extension = str_replace("image/", "", $image_info['mime']);
                    move_uploaded_file($tmp_name, APP_ICO_PATH . "/" . $marker_type_id . "." . $file_extension);
                    $marker_type_data['image_type'] = $file_extension;
                    $marker_type_data['image_width'] = 32;
                    $marker_type_data['image_height'] = 32;
                    $marker_type_data['id'] = $marker_type_id;
                    $marker_type_id = $marker_type->save($marker_type_data);
                    $response_messages = array();

                    $response_messages["msg"]["code"] = 0;
                    $response_messages["msg"]["text"] = "Data Tersimpan.";
                    $response_messages["msg"]["post_url"] = HTTP_APP_PATH;
                    print json_encode($response_messages);
                }
            }
        }
        else {
            $response_messages["msg"]["code"] = 1;
            $response_messages["msg"]["text"] = "Gagal Menambah Jenis Objek.";
            $response_messages["msg"]["post_url"] = HTTP_APP_PATH;
            print json_encode($response_messages);
        }
        break;
    case "update-marker-type":
        $marker_type_data = array();
        $marker_type_data['id'] = filter_input(INPUT_POST, "marker_id");
        $marker_type_data['type_name'] = filter_input(INPUT_POST, "marker_type_name");
        $marker_type_data['is_aktif'] = filter_input(INPUT_POST, "is_aktif");
        $marker_type_data['3a_tipe'] = filter_input(INPUT_POST, "3a_tipe");
        $marker_type_data['is_ekraf'] = filter_input(INPUT_POST, "is_ekraf");
        if (isset($_FILES["marker_icon"]) && $_FILES["marker_icon"]["error"] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["marker_icon"]["tmp_name"];
            $image_info = getimagesize($tmp_name);
            $image_type = $image_info[2];
            if(in_array($image_type , array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG))) {
                $file_extension = str_replace("image/", "", $image_info['mime']);
                move_uploaded_file($tmp_name, APP_ICO_PATH . "/" . $marker_type_data['id'] . "." . $file_extension);
                $marker_type_data['image_type'] = $file_extension;
                $marker_type_data['image_width'] = 32;
                $marker_type_data['image_height'] = 32;
                $marker_type->save($marker_type_data);

            }
        }
        else {
            $marker_type->update_name($marker_type_data['type_name'], $marker_type_data['is_aktif'],$marker_type_data['id'], $marker_type_data['is_ekraf']);
        }
        $response_messages["msg"]["code"] = 0;
        $response_messages["msg"]["text"] = "Data updated.";
        print json_encode($response_messages);
        break;
    case "add-ekraf":
        $title = filter_input(INPUT_POST, "title");
        $ekraf->setTitle($title);
        $ekraf->setDescription(filter_input(INPUT_POST, "description"));
        $ekraf->setLat((float)filter_input(INPUT_POST, "lat", FILTER_VALIDATE_FLOAT));
        $ekraf->setLng((float)filter_input(INPUT_POST, "lng", FILTER_VALIDATE_FLOAT));
        $ekraf->setMarkerType((int)filter_input(INPUT_POST, "marker_type", FILTER_VALIDATE_INT));
        $ekraf->setProvince(filter_input(INPUT_POST, "provinsi"));
        $ekraf->setCity(filter_input(INPUT_POST, "kota"));
        $ekraf->setKecamatan(filter_input(INPUT_POST, "kecamatan"));
        $ekraf->setKelurahan(filter_input(INPUT_POST, "kelurahan"));
        // $ekraf->setSubkategori((int)filter_input(INPUT_POST, "subkategori", FILTER_VALIDATE_INT));
        $ekraf->setCreatedBy($_SESSION['username']);

        $ekraf_id = $ekraf->add();
        if($ekraf_id > 0) {
            $user_obj->insert_log($_SESSION['username'],'Tambah Ekraf id:' . $ekraf_id,SITE_DOMAIN . 'ekraf-list.php');
            if (isset($_FILES["ekraf_image"]) && $_FILES["ekraf_image"]["error"] == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["ekraf_image"]["tmp_name"];
                $image_info = getimagesize($tmp_name);
                $image_type = $image_info[2];
                if(in_array($image_type , array(IMAGETYPE_JPEG))) {
                    $file_extension = str_replace("image/", "", $image_info['mime']);
                    $dir = $ekraf_id % 10;
                    $upload_dir = APP_EKRAF_PATH . "/" . $dir;

                    if(!file_exists($upload_dir)) {
                        mkdir($upload_dir, 0755);
                    }
                    move_uploaded_file($tmp_name, $upload_dir . "/" . $ekraf_id . "." . $file_extension);
                    $ekraf = new Ekraf($dbh);
                    $ekraf->setImageType($file_extension);
                    $ekraf->setId((int)$ekraf_id);
                    $ekraf->updateImageType();
                    $response_messages = array();

                    $response_messages["msg"]["code"] = 0;
                    $response_messages["msg"]["text"] = "Data tersimpan.";
                    $response_messages["msg"]["post_url"] = HTTP_APP_PATH."/ekraf-list.php";
                    print json_encode($response_messages);
                    break;
                }
            }
        }
        else {
            $response_messages["msg"]["code"] = 1;
            $response_messages["msg"]["text"] = "Terjadi Kesalahan.";
            $response_messages["msg"]["post_url"] = HTTP_APP_PATH."/ekraf-list.php";
            print json_encode($response_messages);
            break;
        }



        $response_messages = array();
        $response_messages["msg"]["code"] = 0;
        $response_messages["msg"]["text"] = "Data Tersimpan.";
        $response_messages["msg"]["post_url"] = HTTP_APP_PATH."/ekraf-list.php";
        print json_encode($response_messages);
        break;
    case "update-ekraf":
        $ekraf_id = (int)filter_input(INPUT_POST, "ekraf_id", FILTER_VALIDATE_INT);
        $ekraf->setTitle(filter_input(INPUT_POST, "title"));
        $ekraf->setDescription(nl2br(filter_input(INPUT_POST, "description")));
        $ekraf->setLat((float)filter_input(INPUT_POST, "lat", FILTER_VALIDATE_FLOAT));
        $ekraf->setLng((float)filter_input(INPUT_POST, "lng", FILTER_VALIDATE_FLOAT));
        $ekraf->setMarkerType((int)filter_input(INPUT_POST, "marker_type", FILTER_VALIDATE_INT));
        $ekraf->setProvince(filter_input(INPUT_POST, "provinsi"));
        $ekraf->setCity(filter_input(INPUT_POST, "kota"));
        $ekraf->setKecamatan(filter_input(INPUT_POST, "kecamatan"));
        $ekraf->setKelurahan(filter_input(INPUT_POST, "kelurahan"));
        // $ekraf->setSubkategori((int)filter_input(INPUT_POST, "subkategori", FILTER_VALIDATE_INT));
        $ekraf->setUpdatedBy($_SESSION['username']);
        $ekraf->setId($ekraf_id);
        $data = $ekraf->getById();
        if($ekraf->update()){
        $user_obj->insert_log($_SESSION['username'],'Edit Ekraf: '.$data['title']. ' - ' . $ekraf_id,SITE_DOMAIN . 'ekraf-list.php');
        }
        if (isset($_FILES["ekraf_image"]) && $_FILES["ekraf_image"]["error"] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["ekraf_image"]["tmp_name"];
            $image_info = getimagesize($tmp_name);
            $image_type = $image_info[2];
            if(in_array($image_type , array(IMAGETYPE_JPEG))) {
                $file_extension = str_replace("image/", "", $image_info['mime']);
                $dir = $ekraf_id % 10;
                $upload_dir = APP_EKRAF_PATH . "/" . $dir;

                if(!file_exists($upload_dir)) {
                    mkdir($upload_dir, 0755);
                }
                move_uploaded_file($tmp_name, $upload_dir . "/" . $ekraf_id . "." . $file_extension);
                $ekraf = new Ekraf($dbh);
                $ekraf->setImageType($file_extension);
                $ekraf->setId((int)$ekraf_id);
                $ekraf->updateImageType();
                $response_messages = array();

                $response_messages["msg"]["code"] = 0;
                $response_messages["msg"]["text"] = "Data updated.";
                $response_messages["msg"]["post_url"] = HTTP_APP_PATH;
                print json_encode($response_messages);
                break;
            }
        }
        $response_messages["msg"]["code"] = 0;
        $response_messages["msg"]["text"] = "Data updated.";
        print json_encode($response_messages);
        break;
    case "get-ekraf":
        if ($id) {
            $ekraf->setId((int)$id);
            $row = $ekraf->getById();
            print json_encode($row);
        }
        break;
    case "delete-ekraf":
        if ($id) {
            $ekraf->setId($id);
            $data = $ekraf->getById();
            if($ekraf->deleteById()){
            $user_obj->insert_log($_SESSION['username'],'Hapus Ekraf: ' . $data['title']);
            }
            if($map_settings["config_enable_cache"] && 1 == $map_settings["config_cache_level"]) {
                $cache->generate($ekraf->get_ekraf(), true);
            }
        }
        header('Location: ' . HTTP_APP_PATH . "/ekraf-list.php?msg=ekraf_deleted");
        exit;
        break;
    case "delete-ekraf-image":
        if ($id) {
            $ekraf->setId($id);
            $ekraf_info = $ekraf->getById();
            $dir = $id % 10;
            $upload_dir = APP_IMG_PATH . "/" . $dir;
            $delete_file = $upload_dir . "/" . $ekraf_info['id'] . "." . $ekraf_info['image_type'];
            if(file_exists($delete_file)) {
                unlink($delete_file);
            }
            $ekraf->setImageType('');
            $ekraf->updateImageType();
        }
        break;
    case "delete-berita":
        if ($id) {
        $sql = "DELETE FROM  " . DB_BERITA_TABLE . " WHERE id_berita = :id_berita";
        $stmt = $dbh->prepare($sql);
                $stmt->execute(array(':id_berita' => $id));
               $user_obj->insert_log($_SESSION['username'],'Hapus berita(RIP)');
        }
        header('Location: ' . HTTP_APP_PATH . "/berita.php?msg=berita_dihapus");
        exit;
        break;
    case "set-star":
        if ($id) {
            $set_star = 1;
            if(isset($_GET['to'])){
                $set_star = $_GET['to'];
            }
        $sql = "UPDATE  " . DB_BENCANA_TABLE . " SET is_penting = :set_star WHERE id_bencana = :id_bencana";
        $stmt = $dbh->prepare($sql);
                $stmt->execute(array(':id_bencana' => $id,'set_star'=>$set_star));
        }
        header('Location: ' . HTTP_APP_PATH . "/bencana-list.php?msg=bencana_di_set");
        exit;
        break;
    case "clear-cache":
        $sql = "DELETE FROM " . DB_STORAGE_TABLE . " WHERE data_key like :data_key";
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(':data_key' => INDEX_CLUSTER_MAP_KEY . '%'));
        header('Location: ' . HTTP_APP_PATH . "/settings.php?msg=cache_clear");
        exit;
        break;
    case "generate-cache":
        if($map_settings["config_enable_cache"]) {
            $cache->generate($marker->get_markers(), true);
            if(!filter_input(INPUT_GET, "no_redirect")) {
                header('Location: ' . HTTP_APP_PATH . "/settings.php?msg=cache_generated");
                exit;
            }
        }
        break;
    case "login":
    include_once(APP_PATH . "/includes/classes/User.class.php");
    $obj_user = new User($dbh);

        $login = filter_input(INPUT_POST, "login");
        $password = filter_input(INPUT_POST, "password");
        $res_login = $obj_user->login($login,$password);
        if($res_login ) {  
            $_SESSION['nxik_gmc_allowed_access'] = true;
            $_SESSION['username'] = $login;
            $_SESSION['role'] = $res_login['role'];
            $obj_user->set_last_login($login);
            if(isset($_SESSION['redirect_url'])) {
                header('Location: ' . SITE_DOMAIN . $_SESSION['redirect_url']);
                exit;
            }
            else {
                header('Location: ' . HTTP_APP_PATH);
                exit;
            }
        } else {
            $_SESSION['msg'] = '<h4>Username / Password tidak sesuai</h4>';
        }
        break;
    case "user-update":
    include_once(APP_PATH . "/includes/classes/User.class.php");
    $obj_user = new User($dbh);

        $username = filter_input(INPUT_POST, "username");
        $new_username = filter_input(INPUT_POST, "newusername");
        $password = $_POST['password'];
        $new_role = filter_input(INPUT_POST, "role");
        $obj_user->update_user($username,$new_username,$password,$new_role);
        $_SESSION['msg'] = '<h4>Username berhasil diupdate</h4>';
        header('Location: ' . HTTP_APP_PATH . '/user.php');
        exit();
        break;
    case "user-add":
            include_once(APP_PATH . "/includes/classes/User.class.php");
            $obj_user = new User($dbh);
        
                $username = filter_input(INPUT_POST, "username");
                $password = $_POST['password'];
                $role = filter_input(INPUT_POST, "role");
                $obj_user->add($username,$password,$role);
                $_SESSION['msg'] = '<h4>Username berhasil ditambahkan</h4>';
                header('Location: ' . HTTP_APP_PATH . '/user.php');
                exit();
                break;
    case "user-delete":
        if (isset($_GET['user'])) {
            $username = $_GET['user'];
            include_once(APP_PATH . "/includes/classes/User.class.php");
            $obj_user = new User($dbh);
            $obj_user->delete_user($username);
        }
        header('Location: ' . HTTP_APP_PATH . "/user.php?msg=user_deleted");
        exit;
        break;
    case "logout":
        unset($_SESSION['nxik_gmc_allowed_access']);
        unset($_SESSION['redirect_url']);
        unset( $_SESSION['role']);
        unset( $_SESSION['username']);
        header('Location: ' . HTTP_APP_PATH);
        exit;
        break;
    case "change-password":
    include_once(APP_PATH . "/includes/classes/User.class.php");
    $obj_user = new User($dbh);

        $username = $_SESSION['username'];

        $cur_password = filter_input(INPUT_POST, "current_password");
        $password = filter_input(INPUT_POST, "new_password");
        $confirm_pass  = filter_input(INPUT_POST, "confirm_password");
        if($password!=$confirm_pass){
           $_SESSION['msg'] = '<h4>Konfirmasi Password tidak sama</h4>'; 
           break;
        }
        $res_login = $obj_user->login($username,$cur_password);
        if($cur_role==1){
           if($username != $_POST['cmb_username']){
            $username = $_POST['cmb_username'];
            $res_login = true;
           }
        }        
        if($res_login ) { 
            $obj_user->update_pass($username,$password);
            $user_obj->insert_log($_SESSION['username'],'Ubah password user: ' . $username);
            $_SESSION['msg'] = '<h4>Password berhasil diubah</h4>';

        } else {
            $_SESSION['msg'] = '<h4>Password lama salah</h4>';
        }
        break;
    case "download-csv":
        $sql = "SELECT * FROM " . DB_MARKERS_TABLE . " as m left join " . DB_MARKER_TYPE_TABLE . " as mt on m.marker_type = mt.id";
        $nama = 'objek';
        if($_GET['is_ekraf'] == 1) {
          $sql = "SELECT * FROM " . DB_EKRAF_TABLE . " as e left join " . DB_MARKER_TYPE_TABLE . " as mt on e.marker_type = mt.id";
          $nama = 'ekraf';
        }
        $stmt = $dbh->prepare($sql);
        $stmt->execute();

        $filename =  $nama .'-'.date('Y-m-d_H-m-i').'.csv';


        $fp = fopen('php://output', 'w');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            fputcsv($fp, $row);
        }
        exit;
    case "import_step_2":
        $current_step = 2;
        if (isset($_FILES["import_file"]) && $_FILES["import_file"]["error"] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["import_file"]["tmp_name"];
            $file_type = mime_content_type($tmp_name);

            if(in_array($file_type , $xls_mime)) {
                $_SESSION['file_type'] = "xls";
                $upload_dir = sys_get_temp_dir();
                $import_file_path = $upload_dir . "/import_file_" . uniqid() . ".xls";
                move_uploaded_file($tmp_name, $import_file_path);
                $_SESSION['import_file_path'] = $import_file_path;
                $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($import_file_path);
                $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                $xls_sheet_names = $objReader->listWorksheetNames($import_file_path);
            }
            elseif(in_array($file_type, $csv_mime)) {
                $current_step = 3;
                $_SESSION['file_type'] = "csv";
                $upload_dir = sys_get_temp_dir();
                $import_file_path = $upload_dir . "/import_file_" . uniqid() . ".csv";
                move_uploaded_file($tmp_name, $import_file_path);
                $_SESSION['import_file_path'] = $import_file_path;
                ini_set('auto_detect_line_endings',TRUE);
                $file_data = array_map('str_getcsv', file($import_file_path, FILE_SKIP_EMPTY_LINES));
                $row_and_column_range['totalRows'] = sizeof($file_data);
                $drop_down_columns = "";
                $num_columns = sizeof($file_data[0]);
                for($i = 1; $i <= $num_columns; $i++) {
                    $drop_down_columns .= '<option value="' . ($i - 1). '">' . $i . '</option>';
                }
            }
        }
        else {
            print "Something went wrong.";
            exit;
        }

        break;
    case "import_step_3":
        $import_file_path = $_SESSION['import_file_path'];
        $sheet_number = $_SESSION['sheet_number'] = filter_input(INPUT_POST, 'sheet_number', FILTER_VALIDATE_INT);

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($import_file_path);
        $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $xls_sheet_names = $objReader->listWorksheetNames($import_file_path);
        if($sheet_number > sizeof($xls_sheet_names)) {
            print "'Sheet number' is out of range [0.." . (sizeof($xls_sheet_names) - 1) . "]";
            exit;
        }
        $current_step = 3;
        $worksheetData = $objReader->listWorksheetInfo($import_file_path);
        $row_and_column_range = $worksheetData[$sheet_number];

        $column_name_array = array();
        $column_index = 0;
        $column_name = "";
        do {
            $column_name = Helper::getNameFromNumber($column_index);
            $column_name_array[$column_index] = $column_name;
            $column_index++;
        }
        while($row_and_column_range['lastColumnLetter'] != $column_name);
        $drop_down_columns = "";

        foreach($column_name_array as $column_index => $column_name) {
            $drop_down_columns .= '<option value="' . $column_index . '">' . $column_name . '</option>';
        }
        break;
    case "import_step_4":
        $current_step = 4;
        $import_file_path = $_SESSION['import_file_path'];
        if($_SESSION['file_type'] == "csv") {
            $sheet_data = array_map('str_getcsv', file($import_file_path, FILE_SKIP_EMPTY_LINES));
        }
        else {
            $sheet_number = $_SESSION['sheet_number'];
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($import_file_path);
            $worksheet = $spreadsheet->getSheet($sheet_number);
            $sheet_data = $worksheet->toArray();
        }
        $marker_title_column = filter_input(INPUT_POST, "marker_title");
        $marker_description_column = filter_input(INPUT_POST, "marker_description");
        $marker_type_column = filter_input(INPUT_POST, "marker_type");
        $marker_latitude_column = filter_input(INPUT_POST, "marker_latitude");
        $marker_longitude_column = filter_input(INPUT_POST, "marker_longitude");
        $start_import_from = filter_input(INPUT_POST, "start_import_from");
        $start_import_to = filter_input(INPUT_POST, "start_import_to");
        $overwrite_all_data = filter_input(INPUT_POST, 'overwrite_all_data', FILTER_VALIDATE_INT);
        if($overwrite_all_data == 2) {
            $marker->deleteAll();
        }

        for($i = ($start_import_from - 1); $i < $start_import_to; $i++) {
            $marker->setTitle($sheet_data[$i][$marker_title_column]);
            $marker->setDescription(nl2br($sheet_data[$i][$marker_description_column]));
            $marker->setLat(floatval(str_replace(',', '.', $sheet_data[$i][$marker_latitude_column])));
            $marker->setLng(floatval(str_replace(',', '.',$sheet_data[$i][$marker_longitude_column])));
            $marker->setMarkerType((int)$sheet_data[$i][$marker_type_column]);
            $marker_id = $marker->add();
        }
        break;
    case "delete-marker-image":
        if ($id) {
            $marker->setId($id);
            $marker_info = $marker->getById();
            $dir = $id % 10;
            $upload_dir = APP_IMG_PATH . "/" . $dir;
            $delete_file = $upload_dir . "/" . $marker_info['id'] . "." . $marker_info['image_type'];
            if(file_exists($delete_file)) {
                unlink($delete_file);
            }
            $marker->setImageType('');
            $marker->updateImageType();
        }
        break;
}
