<?php

if(empty($_GET['lat']) && empty($_GET['lng'])) {
    $response = array('status'=>'Error','msg'=>'Bad Request.');
    echo json_encode($response);
    exit();
}
$lat = $_GET['lat'];
$lng = $_GET['lng'];
$response = array();
$url = 'https://revgeocode.search.hereapi.com/v1/revgeocode?at='.$lat.','.$lng.'&apiKey=2w7bsEbT9cVN_gJCrEMr69poSTYBeAGYFb2eXf2VKxs&q=city';
// $curl = curl_init();
// curl_setopt($curl, CURLOPT_URL, $url);
// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// $data = curl_exec($curl);
// curl_close($curl);
// echo $data;
try {
    $curl = curl_init();

    // Check if initialization had gone wrong*
    if ($curl === false) {
        throw new Exception('failed to initialize');
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $data = curl_exec($curl);

    // Check the return value of curl_exec(), too
    if ($data === false) {
        throw new Exception(curl_error($curl), curl_errno($curl));
    }

    /* Process $data here */
    if($data) {
        $data_arr = json_decode($data, true);
        // var_dump($data_arr['items']);
        $response['status'] = 200;
        $response['msg']    = 'Ok';
        $response['item']   = array(
            'provinsi'  => $data_arr['items'][0]['address']['county'],
            'kota'      => $data_arr['items'][0]['address']['city'],
            'kec'       => $data_arr['items'][0]['address']['district'],
            'kel'       => $data_arr['items'][0]['address']['subdistrict'],
        );
        header('Content-type: application/json');
        echo json_encode($response);
    }

    // Close curl handle
    curl_close($curl);
} catch(Exception $e) {

    trigger_error(sprintf(
        'Curl failed with error #%d: %s',
        $e->getCode(), $e->getMessage()),
        E_USER_ERROR);
}
?>