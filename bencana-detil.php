<?php
$id_bencana = $_GET['id'];
if(!is_numeric($id_bencana)){
    header("Location: https://wisatatangguh.kemenparekraf.go.id/bencana-list.php");
}

$modul = 'bencana_detil';
$sidebar= false;
$title = "Detil Bencana  - Peta Geospasial Pariwisata";
include_once('blocks/header.php');
include_once(APP_PATH . "/includes/classes/pagination/pagination.class.php");
$cur_role = 0;
if(isset($_SESSION['role'])){
$cur_role = $_SESSION['role'];
}

$form_act = '';
if(isset($_GET['act']) && $_GET['act'] == 'edit'){
  $form_act = 'edit';

}
if(isset($_POST['act'])){
    $act= $_POST['act'];
    if($act=='update-info'){
        //kirim gambar
        include_once(APP_PATH . "/includes/classes/Upload.class.php");
        $handle = new upload($_FILES['gambar_info']);
        $gambar_upload = '';
if ($handle->uploaded) {
    $nama_baru = time();
  $handle->file_new_name_body   = $nama_baru;
  $handle->image_convert = 'jpg';
  $handle->image_resize         = true;
  $handle->image_x              = 300;
  $handle->image_ratio_y        = true;
  $handle->process(APP_PATH.'/static/img/upload/update_info/');
  if ($handle->processed) {
    $gambar_upload = $nama_baru.'.jpg';
    $handle->clean();
  } else {
    echo 'error : ' . $handle->error;
  }
}

        $sql = "INSERT INTO bencana_informasi(id_bencana, waktu_kirim,user,judul,informasi,gambar) VALUES (:id_bencana,NOW(),:user,:title, :description,:gambar_upload) ";
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':id_bencana', $_SESSION['id_bencana']);
            $stmt->bindParam(':title', $_POST['judul']);
            $stmt->bindParam(':user', $_SESSION['username']);
            $stmt->bindParam(':description', $_POST['deskripsi']);
            $stmt->bindParam(':gambar_upload', $gambar_upload);
            if($stmt->execute()){
            $user_obj->insert_log($_SESSION['username'],'Tambah informasi bencana :' . $marker_id,SITE_DOMAIN . 'bencana-detil.php?id=' . $_SESSION['id_bencana']);
           }

    } else if($act=='update_bencana'){
        $wisman_luka = (int)$_POST['wisman_luka'];
        $wisman_hilang = (int)$_POST['wisman_hilang'];
        $wisman_meninggal = $_POST['wisman_meninggal'];
        $wisnus_luka = $_POST['wisnus_luka'];
        $wisnus_hilang = (int)$_POST['wisnus_hilang'];
        $wisnus_meninggal = $_POST['wisnus_meninggal'];
        $atraksi_rr = $_POST['atraksi_rr'];
        $atraksi_rs = $_POST['atraksi_rs'];
        $atraksi_rb = $_POST['atraksi_rb'];
        $akses_rr = $_POST['akses_rr'];
        $akses_rs = $_POST['akses_rs'];
        $akses_rb = $_POST['akses_rb'];
        $amenitas_rr = $_POST['amenitas_rr'];
        $amenitas_rs = $_POST['amenitas_rs'];
        $amenitas_rb = $_POST['amenitas_rb']; 
        $deskripsi = $_POST['deskripsi_bencana']; 
        $aksi = $_POST['aksi_bencana']; 
        $catatan = $_POST['catatan_bencana'];     
        $id_bencana = $_SESSION['id_bencana'];
            $sql = "UPDATE bencana_bnpb SET wisman_luka = '$wisman_luka', wisman_hilang ='$wisman_hilang', wisman_meninggal ='$wisman_meninggal', wisnus_luka ='$wisnus_luka',wisnus_hilang='$wisnus_hilang',wisnus_meninggal='$wisnus_meninggal',atraksi_rr ='$atraksi_rr', atraksi_rs='$atraksi_rs', atraksi_rb='$atraksi_rb',akses_rr='$akses_rr',akses_rs='$akses_rs',akses_rb='$akses_rb',amenitas_rr='$amenitas_rr',amenitas_rs='$amenitas_rs',amenitas_rb='$amenitas_rb',deskripsi=:deskripsi,aksi=:aksi,catatan=:catatan WHERE id_bencana = '$id_bencana'";
           // echo $sql;
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':deskripsi', $deskripsi);
            $stmt->bindParam(':aksi', $aksi);
            $stmt->bindParam(':catatan', $catatan);
            if($stmt->execute()){
            $user_obj->insert_log($_SESSION['username'],'Update data bencana',SITE_DOMAIN . 'bencana-detil.php?id=' . $id_bencana);
           }

    }
}
$pagination = (new Pagination());

$current_page = (isset($_GET['page']) && $_GET['page'] > 0) ? (int) $_GET["page"] : 1;

$input_search_text = $search_text = isset($_GET['search_text']) ? trim($_GET['search_text']) : '';

//ekraf
$jns_ekraf = isset($_GET['jns_ekraf']) ? trim($_GET['jns_ekraf']) : '';
$arr_jenis_ekraf = array();
if(!empty($jns_ekraf)){
$arr_jenis_ekraf[0] = $jns_ekraf;
}

//objek wisata
$jns_objek = isset($_GET['jns_objek']) ? trim($_GET['jns_objek']) : '';
$arr_jenis = array();
if(!empty($jns_objek)){
$arr_jenis[0] = $jns_objek;
}
//bencana
$jns_bencana = isset($_GET['jns_bencana']) ? trim($_GET['jns_bencana']) : '';
$arr_jenis_bencana = array();
if(!empty($jns_bencana)){
$arr_jenis_bencana[0] = $jns_bencana;
}

//record per Page($per_page)
$markers_per_page = isset($_GET['markers_per_page']) && in_array((int) $_GET['markers_per_page'], $config_markers_per_page) ? $_GET['markers_per_page'] : $config_markers_per_page[0];
$offset = $markers_per_page * ($current_page - 1);

//lokasi sekitar

if(is_numeric($id_bencana)){
    $_SESSION['id_bencana'] = $id_bencana;
} else {
    header("Location:index.php");
}
$list_ekraf = $marker_type->get(false,true);
$list_wisata = $marker_type->get(3);
$list_bencana = $marker_type->get(true);
$bencana_info = $bencana->getById($id_bencana);
$lat=$bencana_info['lat'];
$lng = $bencana_info['lng'];
$sumber = $bencana_info['sumber_data'];
$ekraf_data = $ekraf->get_ekraf_around($lat,$lng,$markers_per_page, $offset, $search_text,$arr_jenis_ekraf);
$objek_data = $marker->get_markers_around($lat,$lng,$markers_per_page, $offset, $search_text,$arr_jenis);
$bencana_data = $bencana->get_list_around($lat,$lng,$markers_per_page, $offset, $search_text, $arr_jenis_bencana);

//$history_bencana = $bencana->get_list_around($lat,$lng);
// data update informasi
$list_info = $bencana->getInformasiUpdate($id_bencana);

?>

<div class="container-fluid">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Informasi#<span id="marker_it_title"></span></h4>
                        </div>
                        <form class="form-horizontal" method="post" id="update-form" enctype="multipart/form-data" action="">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="judul" class="col-sm-3 control-label">Judul *</label>
                                    <div class="col-sm-8">
                                        <input name="judul" required="" value="" type="text" class="form-control" id="judul">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label for="deskripsi" class="col-sm-3 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea name="deskripsi" id='deskripsi' class="form-control" rows="8"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="gambar_info" class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-8">
                                        <span id="inputMarkerImage" style="float: left"></span>
                                        <input type="file" name="gambar_info">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <input type='hidden' name='act' id='act' value='update-info'>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                <button type="submit"  class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8">
                        <h1 class="page-header"><?php echo $bencana_info['kejadian'];?> (<?php echo $sumber;?>)</h1>
                        <h4><?php
                                $waktu = new DateTime($bencana_info["waktu"]);
                                echo  $waktu->format("d M Y ") . '<br />' . $bencana_info['jam']; ?></h4>

    <div class="row">
            </div><!-- /.row -->
    <div class="row">
    <div class="col-md-7">
      <div id="map-canvas" class="" style="width: 100%"></div>
    </div>

    <div class="col-md-5">
        <div class="property-list">
            <dl>
                
 <?php 

 if($sumber== 'BNPB'){ ?>                
                 
                  <dt>Provinsi</dt><dd><span class="label label-success"> <?php echo $bencana_info['nprop'];?> </span></dd>
                 <dt>Kabupaten </dt><dd><span class="label label-success"> <?php echo $bencana_info['nkab'] . ' - ' . $bencana_info['lokasi'];?> </span></dd>
                   <dt>Meninggal </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['meninggal'],0,'','.');?> </span><!--<span class="label label-warning"> <?php echo number_format($bencana_info['wisman_luka'],0,'','.');?> </span>--></dd>
                 <dt>Hilang </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['hilang'],0,'','.');?> </span></dt>
                <dt>Terluka </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['terluka'],0,'','.');?> </span></dd>
                <dt>Menderita </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['menderita'],0,'','.');?> </span></dd>
                <dt>Rusak Ringan </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['rumah_rusak_ringan'],0,'','.');?> </span></dd>
                <dt>Rusak Sedang </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['rumah_rusak_sedang'],0,'','.');?> </span></dd>
                <dt>Rusak Berat </dt><dd><span class="label label-success"> <?php echo number_format($bencana_info['rumah_rusak_berat'],0,'','.');?> </span></dd>
                 <dt>Keterangan</dt><dd><p class="text"><?php echo nl2br(html_entity_decode($bencana_info['keterangan']));?></p> </dd>
 <?php } else if($sumber == 'BMKG')  { ?>
                  <dt>Magnitude</dt><dd><span class="label label-success"> <?php echo $bencana_info['magnitude'];?> </span></dd>
                 <dt>Kedalaman </dt><dd><span class="label label-success"> <?php echo $bencana_info['kedalaman'];?></span></dd>
                 <dt>Lokasi </dt><dd><span class="label label-success"><?php echo $bencana_info['lokasi'];?></span></dd>
                 <dt>Keterangan</dt><dd><p class="text"><?php echo nl2br($bencana_info['keterangan']);?></p> </dd>

 <?php }?>              
                 
                
            </dl>
        </div><!-- /.property-list -->
 

 
   </div>
</div><!-- /.row -->

<div  >
    <h3 class="page-header">Data Kepariwisataan 

      <?php if($form_act == 'edit') { ?> 
            <a href="bencana-detil.php?id=<?php echo $id_bencana;?>"> 
     <button class="btn btn-danger" id="btn_close_edit" ><i class="glyphicon glyphicon-remove"></i></button></a>

      <?php } else {;?>
<button class="btn btn-success" id="btn_data" ><i class="glyphicon  glyphicon glyphicon-list"></i></button>
<?php if(($cur_role=='1') || ($cur_role=='2') ){?>
            <a href="bencana-detil.php?id=<?php echo $id_bencana;?>&act=edit"> 
     <button class="btn btn-warning" id="btn_edit_data" title="edit data"><i class="glyphicon glyphicon-pencil"></i></button></a><?php }}?></h3>
 </div>
 <div id="div_data" >
  <?php if($form_act == 'edit') { ?>

 <form action="" method="post">
    <input type="hidden" id="act" name="act" value="update_bencana">
<div class="form-row">
 <div class="form-group col-md-12">
    <label>Korban Terdampak</label>
    </div>
</div>
 <div class="form-row">
 <div class="form-group col-md-3">
    </div>
    <div class="form-group col-md-3">
      <label for="inputCity">Luka</label>
    </div>
    <div class="form-group col-md-3">
      <label for="inputState">Hilang</label>
    </div>
    <div class="form-group col-md-3">
      <label>Meninggal</label>
     
    </div>
  </div>
    <div class="form-row">
    <div class="form-group col-md-3">
      <label>Wisman</label>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="wisman_luka" name="wisman_luka" value="<?php echo $bencana_info['wisman_luka'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="wisman_hilang" name="wisman_hilang" value="<?php echo $bencana_info['wisman_hilang'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="wisman_meninggal" name="wisman_meninggal" value="<?php echo $bencana_info['wisman_meninggal'];?> ">
    </div>
  </div>
      <div class="form-row">
    <div class="form-group col-md-3">
      <label >Wisnus</label>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="wisnus_luka" name="wisnus_luka" value="<?php echo $bencana_info['wisnus_luka'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="wisnus_hilang" name="wisnus_hilang" value="<?php echo $bencana_info['wisnus_hilang'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="wisnus_meninggal" name="wisnus_meninggal" value="<?php echo $bencana_info['wisnus_meninggal'];?> ">
    </div>
  </div>
  <div class="form-row">
 <div class="form-group col-md-12">
    <label>Infrastruktur Terdampak</label>
    </div>
</div>
 <div class="form-row">
 <div class="form-group col-md-3">
    Kerusakan
    </div>
    <div class="form-group col-md-3">
      <label for="inputCity">Ringan</label>
    </div>
    <div class="form-group col-md-3">
      <label for="inputState">Sedang</label>
    </div>
    <div class="form-group col-md-3">
      <label>Berat</label>
     
    </div>
  </div>
    <div class="form-row">
    <div class="form-group col-md-3">
      <label>Atraksi</label>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="atraksi_rr" name="atraksi_rr" value="<?php echo $bencana_info['atraksi_rr'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="atraksi_rs" name="atraksi_rs" value="<?php echo $bencana_info['atraksi_rs'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="atraksi_rb" name="atraksi_rb" value="<?php echo $bencana_info['atraksi_rb'];?> ">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label >Aksesibilitas</label>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="akses_rr" name="akses_rr" value="<?php echo $bencana_info['akses_rr'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="akses_rs" name="akses_rs" value="<?php echo $bencana_info['akses_rs'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="akses_rb" name="akses_rb" value="<?php echo $bencana_info['akses_rb'];?> ">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label >Amenitas</label>
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="amenitas_rr" name="amenitas_rr" value="<?php echo $bencana_info['amenitas_rr'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="amenitas_rs" name="amenitas_rs" value="<?php echo $bencana_info['amenitas_rs'];?> ">
    </div>
    <div class="form-group col-md-3">
      <input type="text" class="form-control" id="amenitas_rb" name="amenitas_rb" value="<?php echo $bencana_info['amenitas_rb'];?> ">
    </div>
  </div>
 <div class="form-group">
<div class="form-group col-md-6">
  <label for="comment">Deskripsi:</label>
  <textarea class="form-control" rows="3" id="deskripsi_bencana" name="deskripsi_bencana"><?php echo $bencana_info['deskripsi'];?></textarea>
</div>
<div class="form-group col-md-6">
  <label for="comment">Aksi:</label>
  <textarea class="form-control" rows="3" id="aksi_bencana" name="aksi_bencana"><?php echo $bencana_info['aksi'];?></textarea>
</div> 
</div>
 <div class="form-group">
    <div class="form-group col-md-12">
  <label for="comment">Catatan/Rekomendasi:</label>
  <textarea class="form-control" rows="3" id="catatan_bencana" name="catatan_bencana"><?php echo $bencana_info['catatan'];?></textarea>
</div>
</div> 
<?php if(($cur_role=='1') || ($cur_role=='2') ){?>
<input type="submit" class="btn btn-primary" value="Simpan Informasi"></button>
<?php }?>
</form>
<?php } else { ?>
<div class="form-row">
 <div class="form-group col-md-12">
    <label>Korban Terdampak</label>
    </div>
</div>
 <div class="form-row">
 <div class="form-group col-md-3">
    </div>
    <div class="form-group col-md-3 text-right">
      <label for="inputCity">Luka</label>
    </div>
    <div class="form-group col-md-3 text-right">
      <label for="inputState">Hilang</label>
    </div>
    <div class="form-group col-md-3 text-right">
      <label>Meninggal</label>
     
    </div>
  </div>
    <div class="row">
    <div class="form-group col-md-3 text-right">
      <label>Wisman</label>
    </div>
    <div class="form-group col-md-3 text-right">
<?php echo $bencana_info['wisman_luka'];?>
    </div>
    <div class="form-group col-md-3 text-right">
<?php echo $bencana_info['wisman_hilang'];?>
    </div>
    <div class="form-group col-md-3 text-right">
<?php echo $bencana_info['wisman_meninggal'];?>
    </div>
  </div>
      <div class="row">
    <div class="form-group col-md-3 text-right">
      <label >Wisnus</label>
    </div>
    <div class="form-group col-md-3 text-right">
<?php echo $bencana_info['wisnus_luka'];?>
    </div>
    <div class="form-group col-md-3 text-right">
<?php echo $bencana_info['wisnus_hilang'];?>
    </div>
    <div class="form-group col-md-3 text-right">
<?php echo $bencana_info['wisnus_meninggal'];?>
    </div>
  </div>
  <div class="form-row">
 <div class="form-group col-md-12">
    <label>Infrastruktur Terdampak</label>
    </div>
</div>
 <div class="form-row">
 <div class="form-group col-md-3">
    Kerusakan
    </div>
    <div class="form-group col-md-3 text-right">
      <label for="inputCity">Ringan</label>
    </div>
    <div class="form-group col-md-3 text-right">
      <label for="inputState">Sedang</label>
    </div>
    <div class="form-group col-md-3 text-right">
      <label>Berat</label>
     
    </div>
  </div>
    <div class="row">
    <div class="form-group col-md-3 text-right">
      <label>Atraksi</label>
    </div>
    <div class=" col-md-3 text-right">
<?php echo $bencana_info['atraksi_rr'];?>
    </div>
    <div class=" col-md-3 text-right">
<?php echo $bencana_info['atraksi_rs'];?>
    </div>
    <div class=" col-md-3 text-right">
<?php echo $bencana_info['atraksi_rb'];?>
    </div>
  </div>
  <div class="row">
    <div class="form-group  col-md-3 text-right">
      <label >Aksesibilitas</label>
    </div>
    <div class="col-md-3 text-right">
<?php echo $bencana_info['akses_rr'];?>
    </div>
    <div class="col-md-3 text-right">
<?php echo $bencana_info['akses_rs'];?>
    </div>
    <div class="col-md-3 text-right">
<?php echo $bencana_info['akses_rb'];?>
    </div>
  </div>
  <div class="row">
    <div class="form-group col-md-3 text-right">
      <label >Amenitas</label>
    </div>
    <div class="col-md-3  text-right">
<?php echo $bencana_info['amenitas_rr'];?>
    </div>
    <div class="col-md-3 text-right">
<?php echo $bencana_info['amenitas_rs'];?>
    </div>
    <div class=" col-md-3 text-right">
<?php echo $bencana_info['amenitas_rb'];?>
    </div>
  </div>
 <div class="form-group">
<div class="form-group col-md-12">
  <label >Deskripsi:</label><br/>
  <?php echo nl2br($bencana_info['deskripsi']);?>
</div>
<div class="form-group col-md-12">
  <label for="comment">Aksi:</label><br/>
<?php echo nl2br($bencana_info['aksi']);?>
</div> 
</div>
 <div class="form-group">
    <div class="form-group col-md-12">
  <label >Catatan/Rekomendasi:</label>
<?php echo nl2br($bencana_info['catatan']);?>
</div>
</div> 
<?php } ?>
</div>
<div id="div_informasi"></div>
<h3 class="page-header">Update Informasi  
    <button class="btn btn-success" id="btn_informasi" ><i class="glyphicon  glyphicon glyphicon-list"></i></button>
<?php if(($cur_role=='1') || ($cur_role=='2') ){?>
<a class="btn btn-warning" id="add_information" href="#" style=""><i class="icon-star icon-white"></i> Tambah informasi <i class="load-indicator"></i></a>
<?php }?>
</h3>
<div id="update_informasi" style="max-height: 450px;overflow: auto;">
 <table class="table table-striped">
    <thead>
      <tr>
        <th>Judul</th>
        <th>Isi Berita</th>
        <th>Gambar</th>
        <th>Waktu</th>
        <th>Pengirim</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($list_info as $info){ 
$str_gambar = '';
$gambar = $info['gambar'];
if(!empty($gambar)){
    $str_gambar = '<img src="./static/img/upload/update_info/'.$gambar.'" width="300px" />';
}
            ?>
      <tr>
        <td><?php echo $info['judul']; ?> </td>
        <td><?php echo $info['informasi']; ?></td>
        <td><?php echo $str_gambar; ?></td>
        <td><?php echo $info['waktu_kirim']; ?></td>
        <td><?php echo $info['user']; ?></td>
      </tr>
    <?php }?>
    </tbody>
  </table>
</div>
 <br style="clear:both;">

                
</div><!-- /.content -->

              <div class="col-lg-4" id="list-column">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item <?php echo !empty($jns_bencana) || (empty($jns_objek) && empty($jns_ekraf)) ? 'active' : ''; ?>">
                                <a class="nav-link" id="bencana-tab" data-toggle="tab" href="#bencana" role="tab" aria-controls="bencana" aria-selected="false">Bencana Sekitar</a>
                            </li>
                            <li class="nav-item <?php echo !empty($jns_objek) ? 'active' : ''; ?>">
                                <a class="nav-link active" id="objek-tab" data-toggle="tab" href="#objek" role="tab" aria-controls="objek" aria-selected="true">Objek Sekitar</a>
                            </li>
                            <li class="nav-item <?php echo !empty($jns_ekraf) ? 'active' : ''; ?>">
                                <a class="nav-link" id="ekraf-tab" data-toggle="tab" href="#ekraf" role="tab" aria-controls="ekraf" aria-selected="false">Ekraf Sekitar</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade <?php echo !empty($jns_bencana) || (empty($jns_objek) && empty($jns_ekraf)) ? 'active in' : ''; ?>" id="bencana" role="tabpanel" aria-labelledby="bencana-tab">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>bencana-detil.php">
                                        <input type="hidden" name="id" value="<?php echo $id_bencana; ?>">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control" name="jns_bencana" id="jns_bencana" >
                                                    <option value="">- SEMUA -</option>
                                                    <?php foreach ($list_bencana as $tipe): ?>
                                                        <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_bencana) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>bencana-detil.php?id=<?php echo $id_bencana; ?>'" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul id="markers-list" class="list-group">
                                <?php if(isset($bencana_data['rows'])) { foreach ($bencana_data['rows'] as $data){ 
                                        $waktu = new DateTime($data["waktu"]);
                                        $waktu = $waktu->format("d M Y ");
                                    ?>
                                <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>bencana-detil.php?id=<?php echo $data['id_bencana']; ?>'" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["kejadian"] . ', ' . $data['nkab'] . ', ' . $data['nprop'] . ', ' . $waktu . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                                <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li>
                            </ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                        <div class="tab-pane fade <?php echo !empty($jns_objek) ? 'active in' : ''; ?>" id="objek" role="tabpanel" aria-labelledby="objek-tab">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>bencana-detil.php">
                                        <input type="hidden" name="id" value="<?php echo $id_bencana; ?>">
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%">
                                                <select class="form-control" name="jns_objek" id="jns_objek" >
                                                    <option value="">- SEMUA -</option>
                                                    <?php foreach ($list_wisata as $tipe): ?>
                                                        <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_objek) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>bencana-detil.php?id=<?php echo $id_bencana; ?>'" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul id="markers-list" class="list-group">
                                <?php if(isset($objek_data['rows'])) { foreach ($objek_data['rows'] as $data){?>
                                <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>objek-detil.php?id=<?php echo $data['id']; ?>'" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["title"] . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                                <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li>
                            </ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                        <div class="tab-pane fade <?php echo !empty($jns_ekraf) ? 'active in' : ''; ?>" id="ekraf" role="tabpanel" aria-labelledby="ekraf-tab">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="navbar-form navbar-left form-inline"  role="search" action="<?php echo HTTP_APP_PATH ?>bencana-detil.php">
                                        <input type="hidden" name="id" value="<?php echo $id_bencana; ?>">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control" name="jns_ekraf" id="jns_ekraf" >
                                                    <option value="">- SEMUA -</option>
                                                    <?php foreach ($list_ekraf as $tipe): ?>
                                                        <option value="<?php echo $tipe['id']; ?>" <?php echo ($tipe['id'] == $jns_ekraf) ? ' selected="selected" ' : ''; ?>><?php echo $tipe['type_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>ekraf-detil.php?id=<?php echo $id_bencana; ?>'" id="clear-search-btn" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <ul id="markers-list" class="list-group">
                                <?php if(isset($ekraf_data['rows'])) { foreach ($ekraf_data['rows'] as $data){?>
                                <div style="display: table; width:100%;"><a style="display: table; width:100%;" href="#" class="list-group-item" onclick="window.location.href = '<?php echo HTTP_APP_PATH ?>ekraf-detil.php?id=<?php echo $data['id_ekraf']; ?>'" id="0"><div class="pull-right text-right col-md-9 col-xs-8" style="margin-bottom:5px;"><h4><?php echo $data["title"] . ' (' . round($data["distance"],2) . ' km)';?></h4></div></a></div>
                                <?php }}?>
                            </ul>
                            <ul class="list-group">
                                <li class='list-group-item'>
                                    <a href="#" class="btn btn-default" id="list-markers-prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                    <a href="#" class="btn btn-default" id="list-markers-next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                    <span id="pager-info"></span>
                                </li>
                            </ul>
                            <ul id="markers-list" class="list-group"></ul>
                        </div>
                    </div>
                </div>
              </div>


                </div><!-- /.sidebar -->
            </div><!-- /.row -->
        </div>
        <script type="text/javascript" src="<?php echo HTTP_APP_PATH ?>/static/plugins/leaflet-map/leaflet.js"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_APP_PATH ?>/static/js/update-marker-normal.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
             $("#add_information").click(function () {
                var editor = CKEDITOR.instances.deskripsi;
                if (editor) {
                    editor.destroy(true);
                }
                CKEDITOR.replace( 'deskripsi' );
                 $('#myModal').modal('show');
             }) ;  

             var mymap = L.map('map-canvas').setView([<?php echo $bencana_info['lat'];?>, <?php echo $bencana_info['lng'];?>], 13);
             var marker = L.marker([<?php echo $bencana_info['lat'];?>, <?php echo $bencana_info['lng'];?>]).addTo(mymap);
var circle = L.circle([<?php echo $bencana_info['lat'];?>, <?php echo $bencana_info['lng'];?>], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 15,
                minZoom: 7,
                attribution: 'attribution'
            }).addTo(mymap);


            //hide show data wisata
              <?php if($form_act != 'edit') { ?>
 $("#div_data").hide();
<?php } else { ?>
location.href = "#div_data";
<?php } ?>
  $("#btn_data").click(function(){
    $("#div_data").toggle();
    window.location.href = "#div_data";
  });

  //hide show informasi update
  update_informasi
  $("#update_informasi").hide();
  $("#btn_informasi").click(function(){
    $("#update_informasi").toggle();
    window.location.href = "#div_informasi";
  });
         });
        </script>
        <?php //include_once(APP_PATH . "/blocks/footer.php"); ?>
    </body>
</html>